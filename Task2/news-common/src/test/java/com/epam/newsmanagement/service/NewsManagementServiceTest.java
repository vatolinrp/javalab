package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.service.impl.AuthorService;
import com.epam.newsmanagement.service.impl.NewsManagementService;
import com.epam.newsmanagement.service.impl.NewsService;
import com.epam.newsmanagement.service.impl.TagService;

public class NewsManagementServiceTest {
	@InjectMocks
	private NewsManagementService newsManService;
	@Mock
	private AuthorService authorServ;
	@Mock
	private TagService tagServ;
	@Mock
	private NewsService newsServ;
	@Mock
	private IUserService userServ;
	@Mock
	private ICommentService commentServ;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void complexCreateNewsTest() throws Exception {
		NewsTO news = new NewsTO();
		TagTO tag = new TagTO();
		tag.setTagName("testName4");
		tag.setTagId(1l);
		AuthorTO author = new AuthorTO();
		author.setName("testName4");
		author.setAuthorId(1l);
		List<TagTO> tags = new ArrayList<TagTO>();
		tags.add(tag);
		newsManService.complexCreateNews(news, author, tags);
		Mockito.verify(newsServ).create(news);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1l);
		Long authorId = 1l;
		Mockito.verify(tagServ).attachTags(tagIds, news.getNewsId());
		Mockito.verify(authorServ).attachAuthor(authorId, news.getNewsId());
	}
	@Test
	public void isNextNewsExistTest() throws Exception {
		Long id =1l;
		newsManService.isNextNewsExist(id);
		Mockito.verify(newsServ).isNextExist(id);
	}
	@Test
	public void isPrevNewsExistTest() throws Exception {
		Long id =1l;
		newsManService.isPrevNewsExist(id);
		Mockito.verify(newsServ).isPrevExist(id);
	}
	@Test
	public void getNextNewsTest() throws Exception {
		Long id =1l;
		newsManService.getNextNews(id);
		Mockito.verify(newsServ).getNextNews(id);
	}
	@Test
	public void getPrevNewsTest() throws Exception {
		Long id =1l;
		newsManService.getPrevNews(id);
		Mockito.verify(newsServ).getPreviousNews(id);
	}
	@Test
	public void deleteCommentTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		newsManService.deleteComment(ids);
		Mockito.verify(commentServ).delete(ids);
	}
}