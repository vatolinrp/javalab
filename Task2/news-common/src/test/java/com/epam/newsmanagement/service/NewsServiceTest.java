package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.service.impl.NewsService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class NewsServiceTest {
	@InjectMocks
	private NewsService newsService;
	@Mock
	private IAuthorDAO authorDAO;
	@Mock
	private INewsDAO newsDAO;
	@Mock
	private ITagDAO tagDAO;
	@Mock
	private ICommentDAO commentDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getNewsTest() throws Exception {
		Mockito.verify(newsDAO).getList();

	}

	@Test
	public void createNewsTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_test4");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		newsService.create(news);
		Mockito.verify(newsDAO).create(news);
	}

	@Test
	public void deleteNewsTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		ids.add(2L);
		newsService.delete(ids);
		Mockito.verify(newsDAO).delete(ids);
	}

	@Test
	public void viewNewsTest() throws Exception {
		newsService.getById(1L);
		Mockito.verify(newsDAO).getById(1L);

	}
}
