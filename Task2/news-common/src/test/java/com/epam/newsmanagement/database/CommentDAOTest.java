package com.epam.newsmanagement.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;

/**
 *
 * @author Rostislav_Vatolin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class CommentDAOTest {
	private static IDataSet newsDataSet;
	private static IDataSet commentsDataSet;
	@Autowired
	private ICommentDAO commentDAO;
	@Autowired
	private DataSource dataSource;

	@BeforeClass
	public static void beforeClass() {
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		newsDataSet = loader.load("/news.xml");
		commentsDataSet = loader.load("/comments.xml");
	}

	@Before
	public void setUp() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect,
					commentsDataSet);

		}
	}

	@After
	public void tearDown() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL
					.execute(dbUnitConnect, commentsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsDataSet);
		}
	}

	@Test
	public void getByIdTest() throws Exception {
		CommentTO commentId1 = commentDAO.getById(1L);
		assertNotNull(commentId1);
		assertEquals(commentId1.getCommentText(), "testName1");
	}

	@Test
	public void readListTest() throws Exception {
		List<CommentTO> newsList = commentDAO.getList();
		assertEquals(3, newsList.size());
	}

	@Test
	public void getCommentsByNewsIdTest() throws Exception {
		List<CommentTO> comments = commentDAO.getCommentsByNewsId(1L);
		assertEquals(2, comments.size());
	}

	@Test
	public void getMapCommentsTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1l);
		Map<Long, ArrayList<CommentTO>> map = commentDAO.getMapComments(ids);
		assertNotNull(map);
	}

	@Test
	public void deleteByNewsIdTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1l);
		commentDAO.deleteByNewsId(ids);
		List<CommentTO> newsList = commentDAO.getList();
		assertEquals(1, newsList.size());
	}

	@Test
	public void createTest() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setCommentText("testName4");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		comment.setCreationDate(new Date(cal.getTime().getTime()));
		comment.setNewsId(1L);
		Long generatedId = commentDAO.create(comment);
		assertNotNull(generatedId);
		comment = commentDAO.getById(generatedId);
		assertNotNull(comment);
	}

	@Test
	public void deleteTest() throws Exception {
		List<Long> listCommentsId = new ArrayList<Long>();
		listCommentsId.add(3L);
		commentDAO.delete(listCommentsId);
		CommentTO comment;
		comment = commentDAO.getById(3L);
		assertEquals(comment, null);
	}

	@Test
	public void updateTest() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setCommentText("testNameX");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		comment.setCreationDate(new Date(cal.getTime().getTime()));
		comment.setNewsId(1L);
		comment.setCommentId(1L);
		commentDAO.update(comment);
		comment = commentDAO.getById(1L);
		assertNotNull(comment);
		assertEquals(comment.getCommentText(), "testNameX");
	}

}