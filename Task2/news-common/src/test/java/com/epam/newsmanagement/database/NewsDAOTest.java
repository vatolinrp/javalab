package com.epam.newsmanagement.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.sql.DataSource;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.NewsTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })

public class NewsDAOTest {
	private static IDataSet newsDataSet;
	private static IDataSet commentsDataSet;
	private static IDataSet ntDataSet;
	private static IDataSet naDataSet;
	private static IDataSet authorsDataSet;
	private static IDataSet tagsDataSet;
	@Autowired
	private INewsDAO newsDAO;
	@Autowired
	private ICommentDAO commentDAO;
	@Autowired
	private DataSource dataSource;

	@BeforeClass
	public static void beforeClass() {
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		newsDataSet = loader.load("/news.xml");
		commentsDataSet = loader.load("/comments.xml");
		ntDataSet = loader.load("/news-tags.xml");
		naDataSet = loader.load("/news-authors.xml");
		authorsDataSet = loader.load("/authors.xml");
		tagsDataSet = loader.load("/tags.xml");
	}

	@Before
	public void setUp() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect,
					authorsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, tagsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect,
					commentsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, ntDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, naDataSet);
		}
	}

	@After
	public void tearDown() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, ntDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, naDataSet);
			DatabaseOperation.DELETE_ALL
					.execute(dbUnitConnect, commentsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, authorsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, tagsDataSet);
		}
	}

	@Test
	public void getListTest() throws Exception {
		List<NewsTO> newsList = newsDAO.getList();
		assertNotNull(newsList);
	}

	@Test
	public void getNewsByIdsTest() throws Exception {
		List<Long> newsIdList = new ArrayList<Long>();
		newsIdList.add(1L);
		newsIdList.add(2L);
		List<NewsTO> newsList = newsDAO.getNewsByIds(newsIdList);
		assertNotNull(newsList);
		assertEquals(2, newsList.size());
	}

	@Test
	public void createTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_test4");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		Long generatedId = newsDAO.create(news);
		assertNotNull(generatedId);
		news = newsDAO.getById(generatedId);
		assertNotNull(news);
	}

	@Test
	public void updateTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_text4");
		news.setNewsId(1L);
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		newsDAO.update(news);
		news = newsDAO.getById(1L);
		assertNotNull(news);
		assertEquals(news.getTitle(), "title_4");
	}

	@Test
	public void getByIdTest() throws Exception {
		NewsTO newsId1 = newsDAO.getById(1L);
		assertNotNull(newsId1);
		assertEquals(newsId1.getShortText(), "s_text1");
	}

	@Test
	public void deleteTest() throws Exception {
		List<Long> idList = new ArrayList<Long>();
		idList.add(3L);
		commentDAO.deleteByNewsId(idList);
		newsDAO.deleteNA(idList);
		newsDAO.deleteNT(idList);
		newsDAO.delete(idList);
		NewsTO news;
		news = newsDAO.getById(3L);
		assertEquals(news, null);
	}
	@Test
	public void getNewsNumTest() throws Exception {
		Integer totalCount = newsDAO.getNewsNum();
		assertEquals(totalCount, new Integer(3));
	}

	@Test
	public void getNewsTOsByPageTest() throws Exception {
		List<NewsTO> news = newsDAO.getNewsTOsByPage(1, 2);
		assertEquals(news.size(), 2);
	}

	@Test
	public void getNextNewsTest() throws Exception {
		NewsTO news = newsDAO.getNews(3l, true);
		assertEquals(news.getNewsId(), new Long(3));
	}
	@Test
	public void getAfterNextNewsTest() throws Exception {
		NewsTO news = newsDAO.getNews(1l, true);
		assertEquals(news.getNewsId(), new Long(2));
	}

	@Test
	public void getPreviousNews() throws Exception {
		NewsTO news = newsDAO.getNews(2l, false);
		assertEquals(news.getNewsId(), new Long(1));
	}

	@Test
	public void deleteNTTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1l);
		newsDAO.deleteNT(ids);

	}

	@Test
	public void deleteNATest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1l);
		newsDAO.deleteNA(ids);
	}

	@Test
	public void getCustomNews1Test() throws Exception {
		FilterVO filter = new FilterVO();
		Long[] tags = {1l};
		filter.setTagIds(tags);
		List<NewsTO> news = newsDAO.getPageOfFilteredNews(filter, 1, 1);
		NewsTO newsTO = news.get(0);
		assertEquals(newsTO.getNewsId(), new Long(1));
	}

	@Test
	public void getCustomNews2Test() throws Exception {
		FilterVO filter = new FilterVO();
		filter.setAuthorId(1l);
		List<NewsTO> news = newsDAO.getPageOfFilteredNews(filter, 1, 1);
		NewsTO newsTO = news.get(0);
		assertEquals(newsTO.getNewsId(), new Long(1));
	}

	@Test
	public void getCustomNewsTest() throws Exception {
		FilterVO filter = new FilterVO();
		Long[] tags = {1l};
		filter.setTagIds(tags);
		filter.setAuthorId(1l);
		List<NewsTO> news = newsDAO.getPageOfFilteredNews(filter, 1, 1);
		NewsTO newsTO = news.get(0);
		assertEquals(newsTO.getNewsId(), new Long(1));
	}

	@Test
	public void getCustomNewsNum1Test() throws Exception {
		FilterVO filter = new FilterVO();
		Long[] tags = {1l};
		filter.setTagIds(tags);
		filter.setAuthorId(1l);
		Integer total = newsDAO.getFilteredNewsNum(filter);
		assertEquals(total, new Integer(2));
	}

	@Test
	public void getCustomNewsNum2Test() throws Exception {
		FilterVO filter = new FilterVO();
		filter.setAuthorId(1l);
		Integer total = newsDAO.getFilteredNewsNum(filter);
		assertEquals(total, new Integer(3));
	}

	@Test
	public void getCustomNewsNum3Test() throws Exception {
		FilterVO filter = new FilterVO();
		Long[] tags = {1l};
		filter.setTagIds(tags);
		Integer total = newsDAO.getFilteredNewsNum(filter);
		assertEquals(total, new Integer(2));
	}
}