package com.epam.newsmanagement.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class Close {
	/**
	 * Closes statement
	 * 
	 * @param statement
	 *            - used statement
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	public static void close(Statement statement) throws DAOException {
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	public static void close(Connection con) throws DAOException {
		try {
			if (con != null)
				con.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes resultSet
	 * 
	 * @param rs
	 *            - used resultSet
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	public static void close(ResultSet rs) throws DAOException {
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}
}
