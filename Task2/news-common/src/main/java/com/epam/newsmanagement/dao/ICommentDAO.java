package com.epam.newsmanagement.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;



/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ICommentDAO extends ICommonDAO<CommentTO>{
	/**
	 * @param newsId - selected news id
	 * @return List<CommentTO> - list of comment which belong to the news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException;
	/**
	 * Gets a map of comments by news ids
	 * @param newsIds selected ids
	 * @return Map<Long, ArrayList<CommentTO>> wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds) throws DAOException;
	/**
	 * Deletes custom comments
	 * @param newsIds - the ids of news, to which comments are attached
	 * @throws DAOException if any exceptions occur in DAO
	 */
	void deleteByNewsId(List<Long> newsIds) throws DAOException;
}
