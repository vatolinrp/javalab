package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.Close;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class NewsDAO implements INewsDAO {
	// SQL
	private final String SQL_GET_NEWS_NUM = "SELECT COUNT(news_id) FROM news";
	private final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private final String SQL_DELETE_NA = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private final String SQL_DELETE_NT = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private final String SQL_GET_NEWS_BY_IDS = "SELECT NEWS_ID,SHORT_TEXT,FULL_TEXT,"
			+ " TITLE,CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID IN #";
	private final String SQL_UPDATE_NEWS = "UPDATE NEWS SET SHORT_TEXT=?, FULL_TEXT=?,"
			+ " TITLE=?, CREATION_DATE=?, MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private final String SQL_CREATE_NEWS = "INSERT INTO NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,"
			+ "CREATION_DATE,MODIFICATION_DATE) values (NEWS_SEQ.nextVal,?,?,?,?,?) ";
	private final String SQL_FIND_NEWS = "SELECT NEWS_ID,SHORT_TEXT,FULL_TEXT, TITLE,"
			+ "CREATION_DATE, MODIFICATION_DATE  FROM NEWS WHERE NEWS_ID=?";
	private final String SQL_GET_A_PART_OF_NEWS = "Select news_id from (SELECT news.NEWS_ID, Row_number()"
			+ " over (ORDER BY NVL(com.comment_count,0) DESC,news.modification_date DESC) R FROM news "
			+ "LEFT JOIN (SELECT comments.news_id, COUNT(comments.news_id) as comment_count "
			+ "FROM COMMENTS GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where R between ? and ?";
	private final String SQL_GET_NEWS = "Select news_id from (SELECT news.NEWS_ID, Row_number() "
			+ "over(ORDER BY NVL(com.comment_count,0) DESC,news.modification_date DESC) R FROM news "
			+ "LEFT JOIN(SELECT comments.news_id, COUNT(comments.news_id) as comment_count FROM COMMENTS "
			+ "GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where"
			+ " R=((Select R from (SELECT news.NEWS_ID, Row_number() over (ORDER BY NVL(com.comment_count,0) DESC,"
			+ "news.modification_date DESC) R FROM news "
			+ "LEFT JOIN (SELECT comments.news_id, COUNT(comments.news_id) as comment_count"
			+ " FROM COMMENTS GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where news_id=?)#)";
	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(NewsTO news) throws DAOException {
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_CREATE_NEWS,
					new String[] { "NEWS_ID" });
			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setTimestamp(4, new Timestamp(news.getCreationDate()
					.getTime()));
			statement.setDate(5, new java.sql.Date(news.getModificationDate()
					.getTime()));
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating a news!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating a news!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public NewsTO getById(Long newsId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		NewsTO news = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_FIND_NEWS);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news = buildNewsTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return news;
	}

	@Override
	public void update(NewsTO news) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_UPDATE_NEWS);
			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setTimestamp(4, new Timestamp(news.getCreationDate()
					.getTime()));
			statement.setDate(5, new java.sql.Date(news.getModificationDate()
					.getTime()));
			statement.setLong(6, news.getNewsId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE_NEWS);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
			con.commit();
		} catch (SQLException e) {
			throw new DAOException("sql error in delete method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public List<NewsTO> getList() throws DAOException {
		return null;
	}

	public static NewsTO buildNewsTO(ResultSet resultSet) throws SQLException {
		NewsTO news = new NewsTO();
		news.setNewsId(resultSet.getLong("NEWS_ID"));
		news.setTitle(resultSet.getString("TITLE"));
		news.setShortText(resultSet.getString("SHORT_TEXT"));
		news.setFullText(resultSet.getString("FULL_TEXT"));
		Date date1 = new Date(resultSet.getTimestamp("CREATION_DATE").getTime());
		news.setCreationDate(date1);
		Date date2 = resultSet.getDate("MODIFICATION_DATE");
		news.setModificationDate(date2);
		return news;
	}

	@Override
	public List<NewsTO> getNewsByIds(List<Long> listOfNewsId)
			throws DAOException {
		List<NewsTO> newslist = new ArrayList<NewsTO>();
		List<NewsTO> resultList = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		String sql = SQLBuilder.getSQLList(listOfNewsId, SQL_GET_NEWS_BY_IDS);
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = buildNewsTO(resultSet);
				newslist.add(news);
			}
			for (Long newsId : listOfNewsId) {
				for (NewsTO newsTO : newslist) {
					if (newsTO.getNewsId().equals(newsId)) {
						resultList.add(newsTO);
						break;
					}
				}
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsByIds method", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return resultList;
	}

	@Override
	public Integer getNewsNum() throws DAOException {
		Integer totalCount = 0;
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_NEWS_NUM);
			if (resultSet.next()) {
				totalCount = resultSet.getInt("COUNT(NEWS_ID)");
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getNewsNum method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return totalCount;
	}

	@Override
	public List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		List<Long> newsIds = new LinkedList<Long>();
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_A_PART_OF_NEWS);
			statement.setInt(1, page * perPage - (perPage - 1));
			statement.setInt(2, page * perPage);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				newsIds.add(resultSet.getLong("NEWS_ID"));
			}
			return getNewsByIds(newsIds);
		} catch (SQLException e) {
			throw new DAOException("sql exception in getNewsVOsByPage method!",
					e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public NewsTO getNews(Long newsId, boolean isNext) throws DAOException {
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		Long nextNewsId = null;
		String sql = SQL_GET_NEWS;
		if (isNext) {
			sql = sql.replaceFirst("#", "+1");
		} else {
			sql = sql.replaceFirst("#", "-1");
		}
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(sql);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				nextNewsId = resultSet.getLong("NEWS_ID");
			} else {
				nextNewsId = newsId;
			}
			return getById(nextNewsId);
		} catch (SQLException e) {
			throw new DAOException("sql error in getPreviousNews method", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public void deleteNT(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE_NT);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in deleteNT method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public void deleteNA(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE_NA);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in deleteNA method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}

	}

	@Override
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId, boolean isNext)
			throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			
			String sql = SQLBuilder.getSQLForOneFilteredNews(filter, newsId, isNext);
			if(sql!=null){
				resultSet = statement.executeQuery(sql);
			}
			else{
				return getById(newsId);
			}
			if (resultSet.next()) {
				return buildNewsTO(resultSet);
			} else {
				return getById(newsId);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql exception in getNextFilteredNewsByAuthor method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<NewsTO> listOfNews = new LinkedList<NewsTO>();
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			String sql = SQLBuilder.getSQLForPageOfFilteredNews(filter, page, perPage);
			if(sql!=null){
				resultSet = statement.executeQuery(sql);
			}
			else{
				return listOfNews;
			}
			while (resultSet.next()) {
				listOfNews.add(buildNewsTO(resultSet));
			}
			return listOfNews;
		} catch (SQLException e) {
			throw new DAOException(
					"sql exception in getNextFilteredNewsByAuthor method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public Integer getFilteredNewsNum(FilterVO filter) throws DAOException {
		Integer totalCount = null;
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			String sql = SQLBuilder.getSQLForNumOfFilteredNews(filter);
			if(sql!=null){
				resultSet = statement.executeQuery(sql);
			}
			else{
				return 0;
			}
			if (resultSet.next()) {
				totalCount = resultSet.getInt("COUNT(NEWS_ID)");
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getCustomNewsNum method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return totalCount;
	}

	

	
}