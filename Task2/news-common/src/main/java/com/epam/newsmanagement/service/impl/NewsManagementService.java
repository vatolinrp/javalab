package com.epam.newsmanagement.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.IUserService;
import com.epam.newsmanagement.service.entity.NewsVO;
/**
*
* @author Rostislav_Vatolin
*
*/
@Service
public class NewsManagementService implements INewsManagementService{
	@Autowired
	private ITagService tagServ;
	@Autowired
	private IUserService userServ;
	@Autowired
	private INewsService newsServ;
	@Autowired
	private ICommentService commentServ;
	@Autowired
	private IAuthorService authorServ;
	@Override
	public List<NewsVO> getNewsVOsByPage(Integer page, Integer perPage)
			throws ServiceException {
		List<NewsTO> newsList;
		List<NewsVO> newsVOList = new LinkedList<NewsVO>();
		Map<Long, AuthorTO> mapNewsAuthors;
		Map<Long, ArrayList<TagTO>> mapNewsTags;
		Map<Long, ArrayList<CommentTO>> mapNewsComments;
		newsList = (newsServ.getNewsTOsByPage(page,perPage));
		NewsVO newsVO;
		List<Long> newsIds = new ArrayList<Long>();
		if(newsList.size()==0){
			return newsVOList;
		}
		for (NewsTO news : newsList) {
			newsIds.add(news.getNewsId());
		}
		
		mapNewsAuthors = authorServ.getMapAuthors(newsIds);
		mapNewsTags = tagServ.getMapTags(newsIds);
		mapNewsComments = commentServ.getMapComments(newsIds);
		for (NewsTO news : newsList) {
			newsVO = new NewsVO();
			newsVO.setAuthorTO(mapNewsAuthors.get(news.getNewsId()));
			newsVO.setNewsTO(news);
			newsVO.setTagTOs(mapNewsTags.get(news.getNewsId()));
			newsVO.setComments(mapNewsComments.get(news.getNewsId()));
			newsVOList.add(newsVO);
		}
		return newsVOList;
	}
	@Override
	public NewsTO getNewsById(Long id) throws ServiceException {
		return (newsServ.getById(id));
	}
	@Override
	public AuthorTO getAuthorByNewsId(Long id) throws ServiceException {
		return authorServ.getAuthorByNewsId(id);
	}
	@Override
	public List<CommentTO> getCommentsByNewsId(Long id) throws ServiceException {
		List<CommentTO> comments = commentServ.getCommentsByNewsId(id);
		Collections.sort(comments, new Comparator<CommentTO>() {
			  public int compare(CommentTO o1, CommentTO o2) {
			      return o1.getCreationDate().compareTo(o2.getCreationDate());
			  }
			});
		return comments;
	}
	@Override
	public Boolean isNextNewsExist(Long id) throws ServiceException {
		return newsServ.isNextExist(id);
		
	}
	@Override
	public Boolean isPrevNewsExist(Long id) throws ServiceException {
		return newsServ.isPrevExist(id);
	}
	@Override
	public NewsTO getNextNews(Long id) throws ServiceException {
		return newsServ.getNextNews(id);
	}
	@Override
	public NewsTO getPrevNews(Long id) throws ServiceException {
		return newsServ.getPreviousNews(id);
	}
	@Override
	@Transactional
	public void deleteComment(List<Long> ids) throws ServiceException {
		commentServ.delete(ids);
	}
	@Override
	@Transactional
	public Long createComment(CommentTO newComment) throws ServiceException {
		return commentServ.create(newComment);
	}
	@Override
	@Transactional
	public void deleteNews(List<Long> newsIds) throws ServiceException {
		newsServ.deleteNA(newsIds);
		newsServ.deleteNT(newsIds);
		commentServ.deleteByNewsId(newsIds);
		newsServ.delete(newsIds);
	}
	@Override
	public List<TagTO> getListOfTags() throws ServiceException {
		return tagServ.getList();
	}
	@Override
	public List<AuthorTO> getListOfAuthors() throws ServiceException {
		return authorServ.getList();
	}
	@Override
	public Integer getNumberOfCustomPages(FilterVO filter,
			Integer perPage) throws ServiceException {
		return newsServ.getNumberOfCustomPages(filter, perPage);
	}
	@Override
	public List<NewsVO> getFilteredNewsVOs(FilterVO filter,
			Integer page, Integer perPage) throws ServiceException {
		Map<Long,AuthorTO> mapNewsAuthors;
		Map<Long,ArrayList<CommentTO>> mapNewsComments;
		Map<Long,ArrayList<TagTO>> mapNewsTags;
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		List<NewsVO> newsVOList = new LinkedList<NewsVO>();
			newsList=(getPageOfFilteredNews(filter,page,perPage));
			NewsVO newsVO;
			List<Long> newsIds = new ArrayList<Long>();
			if(newsList.size()==0){
				return newsVOList;
			}
			for(NewsTO news:newsList){
				newsIds.add(news.getNewsId());
			}
			mapNewsAuthors = authorServ.getMapAuthors(newsIds);
			mapNewsTags = tagServ.getMapTags(newsIds);
			mapNewsComments = commentServ.getMapComments(newsIds);
			for(NewsTO news:newsList){
				newsVO = new NewsVO();
				newsVO.setAuthorTO(mapNewsAuthors.get(news.getNewsId()));
				newsVO.setNewsTO(news);
				newsVO.setTagTOs(mapNewsTags.get(news.getNewsId()));
				newsVO.setComments(mapNewsComments.get(news.getNewsId()));
				newsVOList.add(newsVO);
			}
			return newsVOList;
	}
	@Override
	public Integer getNumberOfPages(Integer perPage) throws ServiceException {
		return newsServ.getNumberOfPages(perPage);
	}
	@Override
	public List<AuthorTO> getAuthorsForEdit(Long newsId) throws ServiceException {
		List<AuthorTO> authors = new ArrayList<AuthorTO>(authorServ.getList());
		AuthorTO author = authorServ.getAuthorByNewsId(Long.valueOf(newsId));
		if(author.getExpireDate()==null){
			int index = authors.indexOf(author);
			authors.remove(index);
			authors.add(0,author);
		}
		else{
			authors.add(0,author);
		}
		return authors;
	}
	@Override
	public Date getDate(NewsVO newsVO) throws ServiceException, ParseException {
		return newsServ.getDate(newsVO);
	}
	@Override
	@Transactional
	public NewsVO setNewsTO(NewsVO newsVO, Date date) throws ServiceException {
		return newsServ.setNewsTO(newsVO, date);
	}
	@Override
	@Transactional
	public NewsVO setAuthor(NewsVO newsVO) throws ServiceException {
		return authorServ.setAuthor(newsVO);
	}
	@Override
	@Transactional
	public NewsVO setTags(NewsVO newsVO) throws ServiceException {
		return tagServ.setTags(newsVO);
	}
	@Override
	@Transactional
	public Long complexCreateNews(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException {
		List<Long> tagIds = new ArrayList<Long>();
		news.setNewsId(newsServ.create(news));
		authorServ.attachAuthor(author.getAuthorId(), news.getNewsId());
		for (TagTO tag : tags) {
			tagIds.add(tag.getTagId());
		}
		if (!tagIds.isEmpty()) {
			tagServ.attachTags(tagIds, news.getNewsId());
		}
		return news.getNewsId();
	}
	@Override
	@Transactional
	public void updateNews(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException {
		List<Long> tagIds = new ArrayList<Long>();
		List<Long> newsID = new ArrayList<Long>();
		newsID.add(news.getNewsId());
		newsServ.deleteNA(newsID);
		newsServ.deleteNT(newsID);
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(news);
		newsServ.update(newsList);
		authorServ.attachAuthor(author.getAuthorId(),
				news.getNewsId());
		for (TagTO tag : tags) {
			tagIds.add(tag.getTagId());
		}
		tagServ.attachTags(tagIds, news.getNewsId());
	}
	@Override
	@Transactional
	public Long createAuthor(AuthorTO author) throws ServiceException {
		return authorServ.create(author);
	}
	@Override
	@Transactional
	public Long createTag(TagTO tag) throws ServiceException {
		return tagServ.create(tag);
	}
	@Override
	@Transactional
	public void updateAuthors(List<AuthorTO> authors) throws ServiceException {
		authorServ.update(authors);
	}
	@Override
	@Transactional
	public void updateTags(List<TagTO> tags) throws ServiceException {
		tagServ.update(tags);
	}
	@Override
	@Transactional
	public void setAuthorAsExpired(Long authorId) throws ServiceException {
		authorServ.setExpired(authorId);
	}
	@Override
	@Transactional
	public void deleteTags(List<Long> ids) throws ServiceException {
		tagServ.deleteNT(ids);
		tagServ.delete(ids);
	}
	@Override
	public NewsVO getNewsVO(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		NewsTO newsTO;
		newsTO = newsServ.getById(Long.valueOf(newsId));
		newsVO.setNewsTO(newsTO);
		newsVO.setTags(tagServ.getTagsByNewsId(newsId));
		return newsVO;
	}
	@Override
	public Boolean isNextNewsExist(FilterVO filter, Long id)
			throws ServiceException {
		return newsServ.isNextExist(filter,id);
	}
	@Override
	public Boolean isPrevNewsExist(FilterVO filter, Long id)
			throws ServiceException {
		return newsServ.isPrevExist(filter,id);
	}
	@Override
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId,
			boolean isNextNeeded) throws ServiceException {
		return newsServ.getOneFilteredNews(filter, newsId, isNextNeeded);
	}
	@Override
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws ServiceException {
		return newsServ.getPageOfFilteredNews(filter, page, perPage);
	}
	@Override
	public UserTO getUser() throws ServiceException {
		String userLogin=((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
		return userServ.getUserByLogin(userLogin);
	}
}