package com.epam.newsmanagement.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;
/**
 *
 * @author Rostislav_Vatolin
 *
 */
public interface INewsManagementService {
	/**
	 * Returns news by id
	 * @param newsId - selected id
	 * @return NewsTO - expected element
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	NewsTO getNewsById(Long id) throws ServiceException;
	/**
	 * Gets author by news id
	 * @return AuthorTO - author of the news
	 * @param id - selected news id
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	AuthorTO getAuthorByNewsId(Long id) throws ServiceException;
	/**
	 * Gets a list of comments by news id
	 * @param id - selected news id
	 * @return List<CommentTO> wanted list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<CommentTO> getCommentsByNewsId(Long id) throws ServiceException;
	/**
	 * Checks if next news exists
	 * @param id - the id of the current news
	 * @return - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isNextNewsExist(Long id) throws ServiceException;
	/**
	 * Checks if previous news exists
	 * @param id - the id of the current news
	 * @return boolean - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isPrevNewsExist(Long id) throws ServiceException;
	/**
	 * Gets filtered news
	 * @param filter - filter param
	 * @param newsId - current news id
	 * @param isNextNeeded - if next needed
	 * @return NewsTO - expected news
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	NewsTO getOneFilteredNews(FilterVO filter,Long newsId,boolean isNextNeeded) throws ServiceException;
	/**
	 * Checks if next news exists
	 * @param id - the id of the current news
	 * @param filter - filter
	 * @return - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isNextNewsExist(FilterVO filter, Long id) throws ServiceException;
	/**
	 * Checks if previous news exists
	 * @param id - the id of the current news
	 * @param filter - filter
	 * @return boolean - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isPrevNewsExist(FilterVO filter,Long id) throws ServiceException;
	/**
	 * Gets the next news
	 * @param id - the id of the current news
	 * @return NewsTO - result news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsTO getNextNews(Long id) throws ServiceException;
	/**
	 * Gets previous news
	 * @param id - the id of the current news
	 * @return NewsTO - result news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsTO getPrevNews(Long id) throws ServiceException;
	/**
	 * Deletes a list of comments from the DB
	 * 
	 * @param ids
	 *            - selected list of comment's ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteComment(List<Long> ids) throws ServiceException;
	/**
	 * creates comment in the DB
	 * 
	 * @param comment
	 *            - comment instance to be created
	 * @return Long - generated comment's id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createComment(CommentTO newComment) throws ServiceException;
	/**
	 * Deletes a list of news from the DB
	 * 
	 * @param ids
	 *            - selected list of comment's ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteNews(List<Long> ids) throws ServiceException;
	/**
	 * Gets a list of all tags from DB
	 * @return List<TagTO> - expected list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<TagTO> getListOfTags() throws ServiceException;
	/**
	 * Gets a list of all authors from DB
	 * @return List<AuthorTO> - expected list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<AuthorTO> getListOfAuthors() throws ServiceException;
	/**
	 * Gets the number of custom pages
	 * @param authorId - author's id
	 * @param tagIds - tag's ids
	 * @param perPage - number of news per page
	 * @return Integer - number of pages
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Integer getNumberOfCustomPages(FilterVO filter, Integer perPage) throws ServiceException;
	/**
	 * Gets filtered NewsVOs
	 * @param filter - our filter
	 * @param page - showing page
	 * @param perPage - number of news per page
	 * @return List<NewsVO> - expected list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<NewsVO> getFilteredNewsVOs(FilterVO filter,Integer page,Integer perPage) throws ServiceException;
	/**
	 * Gets NewsVO by page
	 * @param page - the page wanted
	 * @param perPage - number of news per page
	 * @return List<NewsVO> - expected list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<NewsVO> getNewsVOsByPage(Integer page,Integer perPage) throws ServiceException;
	/**
	 * Gets number of showing pages of news
	 * @param perPage - number of news per page
	 * @return Integer - number of pages
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Integer getNumberOfPages(Integer perPage) throws ServiceException;
	/**
	 * Gets newsVO by news id
	 * @param newsId - id of the news
	 * @return NewsVO - expected newsVO
	 * @throws ServiceException
	 */
	NewsVO getNewsVO(Long newsId) throws ServiceException;
	/**
	 * Gets authors in special order
	 * @param newsId - id of the news, which was written by that author
	 * @return List<AuthorTO> - expected list
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<AuthorTO> getAuthorsForEdit(Long newsId) throws ServiceException;
	/**
	 * Gets date from form
	 * 
	 * @param newsVO
	 *            - class object with string date parameter
	 * @return Date expected Date
	 * @throws ParseException
	 *             if date format is bad
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	Date getDate(NewsVO newsVO) throws ServiceException, ParseException;
	/**
	 * Sets Dates of the newsTO and adds it to the newsVO
	 * 
	 * @param newsVO
	 *            - for getting newsTO
	 * @param date
	 *            - date from the news
	 * @return NewsVO - for setting newsTO
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	NewsVO setNewsTO(NewsVO newsVO,Date date) throws ServiceException;
	/**
	 * Sets author
	 * @param newsVO - where to set
	 * @return newsVO - result object
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	NewsVO setAuthor(NewsVO newsVO) throws ServiceException;
	/**
	 * Sets tags
	 * 
	 * @param newsVO
	 *            - where to set
	 * @return newsVO - result object
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setTags(NewsVO newsVO) throws ServiceException;
	/**
	 * Creates news with tags and author
	 * @param news - new to be created
	 * @param author - author to be attached
	 * @param tags - tags to be attached
	 * @return Long - the generated news id
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	Long complexCreateNews(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException;
	/**
	 * Updates news with tags and ids
	 * @param news - news to updated
	 * @param author - author to be updated
	 * @param tags - tags to be updated
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	void updateNews(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException;
	/**
	 * creates author in the DB
	 * 
	 * @param author
	 *            - author instance to be created
	 * @return Long - generated author's id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createAuthor(AuthorTO author) throws ServiceException;
	/**
	 * creates tag in the DB
	 * 
	 * @param tag
	 *            - tag instance to be created
	 * @return Long - generated tag's id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createTag(TagTO tag) throws ServiceException;
	/**
	 * Updates authors
	 * @param authors - authors to update
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	void updateAuthors(List<AuthorTO> tags) throws ServiceException;
	/**
	 * Updates tags
	 * @param tags - tags to update
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	void updateTags(List<TagTO> tags) throws ServiceException;
	/**
	 * Sets author as expired
	 * @param authorId the id of author to set as expired
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	void setAuthorAsExpired(Long authorId) throws ServiceException;
	/**
	 * Deletes a list of tags from the DB
	 * 
	 * @param ids
	 *            - selected list of tag's ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteTags(List<Long> ids) throws ServiceException;
	/**
	 * Gets list of filtered news for page
	 * @param filter - filter param
	 * @param page - page param
	 * @param perPage - number of news per page
	 * @return List<NewsTO> - list of expected news
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page, Integer perPage) throws ServiceException;
	/**
	 * Gets current user
	 * @return UserTO current user
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	UserTO getUser() throws ServiceException;
	
}
