package com.epam.newsmanagement.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface INewsService extends ICommonService<NewsTO> {
	/**
	 * Gets number of showing pages of news
	 * @param perPage - number of news per page
	 * @return Integer - number of pages
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Integer getNumberOfPages(Integer perPage) throws ServiceException;

	/**
	 * Gets the number of custom pages
	 * @param filter - filter
	 * @param perPage - number of news per page
	 * @return Integer - number of pages
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Integer getNumberOfCustomPages(FilterVO filter,
			Integer perPage) throws ServiceException;

	/**
	 * Gets the next news
	 * @param newsId - the id of the current news
	 * @return NewsTO - result news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsTO getNextNews(Long newsId) throws ServiceException;

	/**
	 * Gets previous news
	 * @param newsId - the id of the current news
	 * @return NewsTO - result news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsTO getPreviousNews(Long newsId) throws ServiceException;
	/**
	 * Gets one filtered news
	 * @param filter - filter param
	 * @param newsId - the id of the news
	 * @param isNextNeeded - true, if next needed
	 * @return NewsTO - expected news
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	NewsTO getOneFilteredNews(FilterVO filter,Long newsId,boolean isNextNeeded) throws ServiceException;
	/**
	 * Sets Dates of the newsTO and adds it to the newsVO
	 * 
	 * @param newsVO
	 *            - for getting newsTO
	 * @param date
	 *            - date from the news
	 * @return NewsVO - for setting newsTO
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setNewsTO(NewsVO newsVO, Date date) throws ServiceException;

	/**
	 * Gets date from form
	 * 
	 * @param newsVO
	 *            - class object with string date parameter
	 * @return Date expected Date
	 * @throws ParseException
	 *             if date format is bad
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	Date getDate(NewsVO newsVO) throws ServiceException, ParseException;

	/**
	 * Checks if previous news exists
	 * @param newsId - the id of the current news
	 * @return Boolean - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isPrevExist(Long newsId) throws ServiceException;

	/**
	 * Checks if next news exists
	 * @param newsId - the id of the current news
	 * @return Boolean- the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isNextExist(Long newsId) throws ServiceException;
	/**
	 * Checks if previous news exists
	 * @param newsId - the id of the current news
	 * @param filter - filter
	 * @return Boolean - the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isPrevExist(FilterVO filter, Long newsId) throws ServiceException;

	/**
	 * Checks if next news exists
	 * @param newsId - the id of the current news
	 * @param filter - filter
	 * @return Boolean- the result
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Boolean isNextExist(FilterVO filter, Long newsId) throws ServiceException;

	/**
	 * Gets news by page
	 * 
	 * @param page
	 *            - the page number
	 * @param perPage
	 *            - number of news per page
	 * @return List<NewsTO> - list of
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws ServiceException;

	/**
	 * Deletes tags from news_tag table
	 * 
	 * @param ids
	 *            - list of tag ids
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteNT(List<Long> ids) throws ServiceException;

	/**
	 * Deletes authors from news_author table
	 * 
	 * @param ids
	 *            - list of tag ids
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteNA(List<Long> ids) throws ServiceException;
	/**
	 * Gets list of filtered news for page
	 * @param filter - filter param
	 * @param page - wanted page
	 * @param perPage - news per page
	 * @return List<NewsTO> - list of filtered news for page
	 * @throws ServiceException if any exceptions occur in Service layer
	 */
	List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page, Integer perPage) throws ServiceException;
}