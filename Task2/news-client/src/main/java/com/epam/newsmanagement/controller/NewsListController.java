package com.epam.newsmanagement.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
@SessionAttributes("filterVO")
public class NewsListController {
	private final Logger logger = Logger.getLogger(NewsListController.class);

	@Autowired
	private INewsManagementService newsManServ;
	private final Integer perPage = 5;

	/**
	 * Displays the list of news
	 * 
	 * @param model
	 *            map of objects
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = { "/news-client", "/" }, method = RequestMethod.GET)
	public String displayList(Map<String, Object> model)
			throws ControllerException {
		return "redirect:" + "/news-list/1";
	}

	/**
	 * Filters the news
	 * 
	 * @param filterVO
	 *            the class with filter parameters
	 * @param model
	 *            map of objects
	 * @param page
	 *            - the page to be shown
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/filter/{page}", method = RequestMethod.GET)
	public String filterNews(@ModelAttribute("filterVO") FilterVO filterVO,
			@PathVariable String page, HttpSession httpSession,
			Map<String, Object> model, @ModelAttribute FilterVO filterVOb)
			throws ControllerException {
		if (filterVO == null) {
			filterVO = filterVOb;
		}
		model.put("filterVO", filterVO);
		httpSession.setAttribute("filterVO", filterVO);
		try {
			model.put("tags", newsManServ.getListOfTags());
			model.put("authors", newsManServ.getListOfAuthors());
			model.put(
					"newsVOList",
					newsManServ.getFilteredNewsVOs(filterVO,
							Integer.parseInt(page), perPage));
			model.put("cpages", newsManServ.getNumberOfCustomPages(filterVO,
					perPage));
			return "news-list";
		} catch (ServiceException e) {
			return "redirect:" + "/news-list/1";
		}
	}

	/**
	 * Displays part of all news from DB
	 * 
	 * @param page
	 *            - the page to be shown
	 * @param model
	 *            - map of objects
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
	public String displayListPage(@PathVariable String page,
			Map<String, Object> model, HttpSession httpSession)
			throws ControllerException {
		try {
			httpSession.setAttribute("filterVO", null);
			model.put("pages", newsManServ.getNumberOfPages(perPage));
			model.put("tags", newsManServ.getListOfTags());
			model.put("authors", newsManServ.getListOfAuthors());
			model.put("newsVOList", newsManServ.getNewsVOsByPage(
					Integer.parseInt(page), perPage));
			model.put("filterVO", new FilterVO());
			return "news-list";
		} catch (ServiceException e) {
			logger.error("service exception in displayList method", e);
			throw new ControllerException(
					"service error in displayList method!", e);
		}
	}
}