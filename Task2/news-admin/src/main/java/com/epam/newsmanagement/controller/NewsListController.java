package com.epam.newsmanagement.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.entity.ListNewsVOs;
import com.epam.newsmanagement.service.util.ServiceUtil;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
@SessionAttributes({"filterVO","greeting"})
public class NewsListController {
	@Autowired
	private INewsManagementService newsManServ;
	private final Integer perPage = 5;
	private Logger logger = Logger.getLogger(NewsListController.class);

	/**
	 * This method deletes selected news and displays all news
	 * 
	 * @param newsList
	 *            - class with all news
	 * @param model
	 *            map of objects
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteNews(
			@ModelAttribute("deleteNewsVO") ListNewsVOs newsList,
			Map<String, Object> model) throws ControllerException {
		try {
			if ((newsList.getForDelete()) != null) {
				newsManServ.deleteNews(newsList.getForDelete());
			}
			return "redirect:" + "/news-list/1";
		} catch (ServiceException e) {
			logger.error("service exception in deleteNews method", e);
			throw new ControllerException(
					"service error in putAuthors method!", e);
		}

	}

	/**
	 * Filters the news
	 * 
	 * @param filterVO
	 *            the class with filter parameters
	 * @param model
	 *            - map of objects
	 * @param page
	 *            - the page to be shown
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/filter/{page}", method = RequestMethod.GET)
	public String filterNews(@ModelAttribute("filterVO") FilterVO filterVO,
			@PathVariable String page, HttpSession httpSession,
			Map<String, Object> model, @ModelAttribute FilterVO filterVOb)
			throws ControllerException {
		ListNewsVOs deleteNews = new ListNewsVOs();
		if (ServiceUtil.isFilterNull(filterVOb)) {
			filterVOb = filterVO;
		}

		model.put("filterVO", filterVOb);
		httpSession.setAttribute("filterVO", filterVOb);
		try {
			model.put("tags", newsManServ.getListOfTags());
			model.put("authors", newsManServ.getListOfAuthors());
			deleteNews.setNewsVOList(newsManServ.getFilteredNewsVOs(filterVOb,
					Integer.parseInt(page), perPage));
			model.put("cpages", newsManServ.getNumberOfCustomPages(filterVOb, perPage));
			model.put("deleteNewsVO", deleteNews);
			return "news-list";
		} catch (ServiceException e) {
			return "redirect:" + "/news-list/1";
		}
	}

	/**
	 * Displays part of all news from DB
	 * 
	 * @param page
	 *            - the page to be shown
	 * @param model
	 *            - map of objects
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
	public String displayListPage(@PathVariable String page,
			Map<String, Object> model, HttpSession httpSession)
			throws ControllerException {
		ListNewsVOs deleteNewsVO = new ListNewsVOs();
		try {
			httpSession.setAttribute("filterVO", null);
			httpSession.setAttribute("user", newsManServ.getUser());
			model.put("pages", newsManServ.getNumberOfPages(perPage));
			model.put("tags", newsManServ.getListOfTags());
			model.put("authors", newsManServ.getListOfAuthors());
			deleteNewsVO.setNewsVOList(newsManServ.getNewsVOsByPage(
					Integer.parseInt(page), perPage));
			model.put("filterVO", new FilterVO());
			model.put("deleteNewsVO", deleteNewsVO);
			return "news-list";
		} catch (ServiceException e) {
			logger.error("service exception in displayList method", e);
			throw new ControllerException(
					"service error in displayList method!", e);
		}
	}
}