package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
public class AuthorAddEditController {
	private Logger logger = Logger.getLogger(NewsListController.class);
	@Autowired
	private INewsManagementService newsManServ;
	/**
	 * Displays all authors
	 * 
	 * @param model
	 *            which contains needed objects
	 * @return String which points to page
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/authors-add-edit", method = RequestMethod.GET)
	public String displayAuthors(Map<String, Object> model)
			throws ControllerException {
		try {
			model.put("authorTO", new AuthorTO());
			model.put("newAuthorTO", new AuthorTO());
			model.put("authorTOList", newsManServ.getListOfAuthors());
			return "authors-add-edit";
		} catch (ServiceException e) {
			logger.error("service exception in displayAuthors method", e);
			throw new ControllerException(
					"service error in displayAuthors method!", e);
		}
		
	}

	/**
	 * Saves author
	 * 
	 * @param model
	 *            which contains needed objects
	 * @param newAuthorTO
	 *            - new author to be saved
	 * @return String which points to page
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/save-author", method = RequestMethod.POST)
	public String saveAuthor(Map<String, Object> model,
			@ModelAttribute("newAuthorTO") AuthorTO newAuthorTO)
			throws ControllerException {
		try {
			newsManServ.createAuthor(newAuthorTO);
				return "redirect:" + "/authors-add-edit";
		} catch (ServiceException e) {
			logger.error("service exception in saveAuthor method", e);
			throw new ControllerException(
					"service error in saveAuthor method!", e);
		}
	}

	/**
	 * Updates the author
	 * 
	 * @param model
	 *            which contains needed objects
	 * @param authorTO
	 *            - author to be updated
	 * @return String which points to page
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/update-author", method = RequestMethod.POST)
	public String updateAuthor(Map<String, Object> model,
			@ModelAttribute("authorTO") AuthorTO authorTO)
			throws ControllerException {
		try {
			List<AuthorTO> authors = new ArrayList<AuthorTO>();
			authors.add(authorTO);
			newsManServ.updateAuthors(authors);
		} catch (ServiceException e) {
			logger.error("service exception in updateAuthor method", e);
			throw new ControllerException(
					"service error in updateAuthor method!", e);
		}
		String link = "redirect:" + "/authors-add-edit";
		return link;
	}

	/**
	 * Expires chosen author
	 * 
	 * @param model
	 *            which contains needed objects
	 * @param authorId
	 *            - the id of the author to be expired
	 * @return String which points to page
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/expire-author/{authorId}", method = RequestMethod.GET)
	public String expireAuthor(Map<String, Object> model,
			@PathVariable String authorId) throws ControllerException {
		try {
			newsManServ.setAuthorAsExpired(Long.parseLong(authorId));
			return "redirect:" + "/authors-add-edit";
		} catch (ServiceException e) {
			logger.error("service exception in deleteTag method", e);
			throw new ControllerException("service error in deleteTag method!",	e);
		}
		
	}
}
