package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
public class TagAddEditController {
	private Logger logger = Logger.getLogger(NewsListController.class);
	@Autowired
	private INewsManagementService newsManServ;
	/**
	 * Displays all tags
	 * @param model which contains needed objects
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/tags-add-edit", method = RequestMethod.GET)
	public String displayTags(Map<String, Object> model) throws ControllerException {
		try{
			model.put("TagTO",new TagTO());
			model.put("newTagTO",new TagTO());
			model.put("tagTOList", newsManServ.getListOfTags());
			return "tags-add-edit";
		}
		catch(ServiceException e){
			logger.error("service exception in displayAuthors method",e);
			throw new ControllerException("service error in displayAuthors method!", e);
		}
		
	}
	/**
	 * Creates tag
	 * @param model which contains needed objects
	 * @param newTagTO - the TagTO to be created
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/save-tag", method = RequestMethod.POST)
	public String saveTag(Map<String, Object> model, @ModelAttribute("newTagTO") TagTO newTagTO) throws ControllerException {
		try{
			newsManServ.createTag(newTagTO);
			return "redirect:"+"/tags-add-edit";
		}
		catch(ServiceException e){
			logger.error("service exception in displayAuthors method",e);
			throw new ControllerException("service error in displayAuthors method!", e);
		}
	}
	/**
	 * Updates tag
	 * @param model which contains needed objects
	 * @param tagTO - the TagTO to be created
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/update-tag", method = RequestMethod.POST)
	public String updateTag(Map<String, Object> model, @ModelAttribute("TagTO") TagTO tagTO) throws ControllerException {
		try{
			List<TagTO> tags = new ArrayList<TagTO>();
			tags.add(tagTO);
			newsManServ.updateTags(tags);
			return "redirect:"+"/tags-add-edit";
		}
		catch(ServiceException e){
			logger.error("service exception in updateTag method",e);
			throw new ControllerException("service error in updateTag method!", e);
		}
		
	}
	/**
	 * Deletes tag
	 * @param model which contains needed objects
	 * @param tagId - the TagTO to be created
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/delete-tag/{tagId}", method = RequestMethod.GET)
	public String deleteTag(Map<String, Object> model,@PathVariable String tagId) throws ControllerException {
		try{
			List<Long> ids = new ArrayList<Long>();
			ids.add(Long.parseLong(tagId));
			newsManServ.deleteTags(ids);
			return "redirect:"+"/tags-add-edit";
		}
		catch(ServiceException e){
			logger.error("service exception in deleteTag method",e);
			throw new ControllerException("service error in deleteTag method!", e);
		}
	}
}
