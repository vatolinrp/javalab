<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="header">
	<div id="logo">
	<spring:message code="title" />
	</div>
	<div id="lang">
	<a href="?lang=en"><spring:message code="lang.en" /></a>
	<a href="?lang=ru"><spring:message code="lang.ru" /></a>
	</div>
</div>