<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
				<c:if test="${not empty tagTOList}">
					<div class="list-of-tags">
						<c:forEach var="listValue" items="${tagTOList}"	varStatus="status">
							<form:form action="/news-admin/update-tag" method="post" commandName="TagTO">
									<div class="tag-select">
										<div class="tag-title"><spring:message code="tag.title" /></div>
										<div class="tag-name">
											<form:input type="text" class="tag-update-input" path="tagName" value="${listValue.tagName}" readonly="true" />
											<form:hidden path="tagId" value="${listValue.tagId}"/>
										</div>
										<div style="display: block;" class="select-link"><spring:message code="edit" /></div>
										
										<div class="options" style="display: none;">
											<fmt:message key="delete.confirm.tag" var="confirm" />
											<a href="delete-tag/${listValue.tagId}" onclick="return confirmComment('${confirm}');"><spring:message code="delete" /></a>
											<input id="save-tag" type="submit" value="<spring:message code="update" />">
											<a class="cancel"><spring:message code="cancel" /></a>
										</div>
									</div>
							</form:form>
							<script src="<c:url value="/resources/js/confirm.js" />"></script>
							<script src="<c:url value="/resources/js/edit.js" />"></script>
						</c:forEach>
					</div>
				</c:if>
			<form:form method="post" action="save-tag" commandName="newTagTO"
			 onsubmit="return validateNewTagForm('${pageContext.response.locale.language}')">
				<div class="author-component">
					<div class="tag-title"><spring:message code="tag.add" /></div>
					<div class="tag-name">
						<form:input type="text" class="tag-update-input" path="tagName" />
						
					</div>
					<div class="select-link">
						<input id="save-tag" type="submit" value="<spring:message code="save" />">
					</div>
				</div>
				<div class="author-component">
						<div class="error-message" id="new-tag-error"></div>
					</div>
			</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>