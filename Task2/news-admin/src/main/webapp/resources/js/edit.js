$(".author-select .select-link").click(
		function() {
			$(this).parents(".author-select").find('.options').css('display',
					'block');
			$(this).parents(".author-select").find('.select-link').css(
					'display', 'none');
			$(this).parents(".author-select").find('.auhtor-update-input')
					.attr('readonly', false);

		});
$(".author-select .cancel").click(
		function() {
			$(this).parents(".author-select").find('.options').css('display',
					'none');
			$(this).parents(".author-select").find('.select-link').css(
					'display', 'block');
			$(this).parents(".author-select").find('.auhtor-update-input')
					.attr('readonly', true);
		});
$(".tag-select .select-link").click(
		function() {
			$(this).parents(".tag-select").find('.options').css('display',
					'block');
			$(this).parents(".tag-select").find('.select-link').css('display',
					'none');
			$(this).parents(".tag-select").find('input[type=text]').attr(
					'readonly', false);
		});
$(".tag-select .cancel").click(
		function() {
			$(this).parents(".tag-select").find('.options').css('display',
					'none');
			$(this).parents(".tag-select").find('.select-link').css('display',
					'block');
			$(this).parents(".tag-select").find('input[type=text]').attr(
					'readonly', true);
		});