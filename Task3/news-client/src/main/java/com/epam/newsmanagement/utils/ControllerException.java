package com.epam.newsmanagement.utils;

public class ControllerException extends Exception{
	
	private static final long serialVersionUID = -5018620626420191435L;

	public ControllerException() {
		super();
	}
	public ControllerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ControllerException(String message) {
		super(message);
	}

	public ControllerException(Throwable cause) {
		super(cause);
	}
}
