package com.epam.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.service.impl.AuthorService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class AuthorServiceTest {
	@InjectMocks
	private AuthorService authorService;
	@Mock
	private IAuthorDAO authorDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void createAuthorTest() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setName("testName4");
		authorService.create(author);
		Mockito.verify(authorDAO).create(author);
	}

	@Test
	public void getNewsByAuthorTest() throws Exception {
		authorService.getNewsByAuthor(1L);
		Mockito.verify(authorDAO).getNewsListByAuthor(1L);
	}
}
