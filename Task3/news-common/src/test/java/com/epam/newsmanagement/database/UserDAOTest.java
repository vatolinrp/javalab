package com.epam.newsmanagement.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.UserTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class UserDAOTest {
	private static IDataSet usersDataSet;
	
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private DataSource dataSource;
	@BeforeClass
	public static void beforeClass() {
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		usersDataSet = loader.load("/users.xml");
	}
	@Before
	public void setUp() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, usersDataSet);
		}

	}
	@After
	public void tearDown() throws Exception {
		try (Connection connect = DataSourceUtils.getConnection(dataSource)) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, usersDataSet);
		}

	}
	@Test
	public void getByLoginTest() throws Exception {
		UserTO user = userDAO.getUserByLogin("login1");
		assertNotNull(user);
		assertEquals(user.getFirstName(), "FirstName1");
	}
}
