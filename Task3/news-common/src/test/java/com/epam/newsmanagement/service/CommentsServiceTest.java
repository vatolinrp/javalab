package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.service.impl.CommentService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class CommentsServiceTest {
	@InjectMocks
	private CommentService commentService;
	@Mock
	private ICommentDAO commentDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void createCommentTest() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setCommentText("testName4");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		comment.setCreationDate(new Date(cal.getTime().getTime()));
		commentService.create(comment);
		Mockito.verify(commentDAO).create(comment);
	}

	@Test
	public void deleteCommentTest() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		ids.add(2L);
		commentService.delete(ids);
		Mockito.verify(commentDAO).delete(ids);
	}
}
