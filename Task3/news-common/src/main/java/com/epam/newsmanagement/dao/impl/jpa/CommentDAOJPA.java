package com.epam.newsmanagement.dao.impl.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;


//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;

@Transactional
//@Repository
public class CommentDAOJPA implements ICommentDAO{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Long create(CommentTO element) throws DAOException {
		try{
			element = entityManager.merge(element);
			entityManager.flush();
			return element.getCommentId();
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in create method",e);
		}
	}

	@Override
	public void update(CommentTO element) throws DAOException {
		//We do not update comments
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		try{
			CommentTO comment=null;
			for(Long id:elements){
				comment = entityManager.find(CommentTO.class,id);
				entityManager.remove(comment);
			}
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in delete method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CommentTO> getList() throws DAOException {
		try{
			List<CommentTO> list = entityManager.createQuery("SELECT comments FROM CommentTO comments").getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getList method",e);
		}
	}

	@Override
	public CommentTO getById(Long id) throws DAOException {
		try{
			CommentTO author = entityManager.find(CommentTO.class, id);
			return author;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getById method",e);
		}
	}
}
