package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.exceptions.ServiceException;

public interface ICommonService<E> {
	/**
	 * creates element in the DB
	 * 
	 * @param element
	 *            - element instance to be created
	 * @return Long - generated elements's id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long create(E element) throws ServiceException;

	/**
	 * Deletes a list of elements from the DB
	 * 
	 * @param ids
	 *            - selected list of element's ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void delete(List<Long> ids) throws ServiceException;

	/**
	 * Gets a list of all elements from DB
	 * 
	 * @return List<E> - expected list
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<E> getList() throws ServiceException;

	/**
	 * Updates elements
	 * 
	 * @param elements
	 *            - elements to update
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void update(List<E> elements) throws ServiceException;

	/**
	 * Returns element by id
	 * 
	 * @param authorId
	 *            - selected id
	 * @return E - expected element
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	E getById(Long element) throws ServiceException;

}