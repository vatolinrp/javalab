package com.epam.newsmanagement.service.entity;

import java.util.List;

/**
 * This class is used to set checks for deleting news
 * 
 * @author Rostislav_Vatolin
 *
 */
public class ListNewsVOs {
	//list of news value objects
	private List<NewsVO> newsVOList;
	//list of ids for delete
	private List<Long> forDelete;

	public List<NewsVO> getNewsVOList() {
		return newsVOList;
	}

	public void setNewsVOList(List<NewsVO> newsVOList) {
		this.newsVOList = newsVOList;
	}

	public List<Long> getForDelete() {
		return forDelete;
	}

	public void setForDelete(List<Long> forDelete) {
		this.forDelete = forDelete;
	}

	public void setForDelete(Long id) {
		forDelete.add(id);
	}
}
