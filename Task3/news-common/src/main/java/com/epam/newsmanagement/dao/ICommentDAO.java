package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.CommentTO;



/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ICommentDAO extends ICommonDAO<CommentTO>{
	
}
