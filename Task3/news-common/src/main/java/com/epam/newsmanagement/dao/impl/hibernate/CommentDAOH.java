package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;
@Repository
public class CommentDAOH implements ICommentDAO{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	@Transactional
	public Long create(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Long id = (Long)session.save(element);
			return id;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in create method!",e);
		}
	}

	@Override
	@Transactional
	public void update(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			session.update(element);
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in update method!",e);
		}
	}

	@Override
	@Transactional
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			for(Long authorId:elements){
				CommentTO comment = (CommentTO) session.get(CommentTO.class, authorId);
				session.delete(comment);
			}
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in delete method!",e);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CommentTO> getList() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(CommentTO.class);
			List<CommentTO> list =  cr.list();
			return list;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getList method!",e);
		}
	}
	@Override
	@Transactional
	public CommentTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		CommentTO comment=null;
		try{
			comment =  (CommentTO) session.get(CommentTO.class, id);
			return comment;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getById method!",e);
		}
	}

}
