package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IAuthorService extends ICommonService<AuthorTO> {
	/**
	 * Gets the list of news from NEWS table in DB by our custom author id
	 * 
	 * @param authorId
	 *            - the id of selected author
	 * @return List<NewsTO> - list of news, which belong to author
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;
	/**
	 * Sets author as expired
	 * 
	 * @param authorId
	 *            the id of author to set as expired
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void setExpired(Long authorId) throws ServiceException;
	/**
	 * Sets author
	 * 
	 * @param newsVO
	 *            - where to set
	 * @return newsVO - result object
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setAuthor(NewsVO newsVO) throws ServiceException;
}