package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ITagDAO extends ICommonDAO<TagTO> {
	/**
	 * Gets list of news by input tag ID
	 * 
	 * @param tagId
	 *            - selected ID
	 * @return List<News> - all news, which have selected tag
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws DAOException;
	/**
	 * Gets tags by ids
	 * @param ids - list of ids of the chosen tags
	 * @return List<TagTO> - the result list of tags
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<TagTO> getTagsByIds(List<Long> ids) throws DAOException;

}
