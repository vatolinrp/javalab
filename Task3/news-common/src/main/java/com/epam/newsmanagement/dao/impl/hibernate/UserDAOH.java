package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.util.HibernateUtil;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;
@Repository
public class UserDAOH implements IUserDAO{
	private SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	@SuppressWarnings("unchecked")
	public UserTO getUserByLogin(String login) throws DAOException {
		Transaction tx = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(UserTO.class);
			cr.add(Restrictions.eq("login", login));
			List<UserTO> list = cr.list();
			tx.commit();
			if (list.size() != 0) {
				return list.get(0);
			} else {
				return null;
			}
			
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getUserByLogin method!", e);
		}
	}

}
