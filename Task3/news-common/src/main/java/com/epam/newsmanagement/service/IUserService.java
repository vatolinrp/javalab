package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.ServiceException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IUserService {
	/**
	 * Gets user by selected login
	 * 
	 * @param login
	 *            - selected login
	 * @return UserTO object, which has our login
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	UserTO getUserByLogin(String login) throws ServiceException;
}