package com.epam.newsmanagement.dao.impl.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;



//import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;

//@Repository
public class UserDAOJPA implements IUserDAO{
	private final String JPQL="SELECT user FROM UserTO user WHERE user.login LIKE :login";
	@PersistenceContext
	private EntityManager entityManager;
	@Override
	@SuppressWarnings("unchecked")
	public UserTO getUserByLogin(String login) throws DAOException {
		try{
			Query query = entityManager.createQuery(JPQL);
			query.setParameter("login", login);
			List<UserTO> users = query.getResultList();
			if(users.size()!=0){
				return users.get(0);
			}
			else{
				return null;
			}
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in delete method",e);
		}
	}

}
