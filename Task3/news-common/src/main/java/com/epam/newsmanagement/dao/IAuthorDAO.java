package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IAuthorDAO extends ICommonDAO<AuthorTO> {
	/**
	 * By author id we get news id in NEWS_AUTHORS and using inner join we get
	 * all news, which have our news id from NEWS_AUTHORS
	 * 
	 * @param authorId
	 *            - selected author's
	 * @return List<News> - list of news, which belongs to author
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException;
	/**
	 * Sets author as expired
	 * @param authorId - the id of the chosen author
	 * @throws DAOException if any exceptions occur in DAO
	 */
	
	void setExpired(Long authorId) throws DAOException;
}
