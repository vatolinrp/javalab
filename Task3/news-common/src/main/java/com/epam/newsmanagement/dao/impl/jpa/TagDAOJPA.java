package com.epam.newsmanagement.dao.impl.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;


//import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;


@Transactional
//@Repository
public class TagDAOJPA implements ITagDAO{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Long create(TagTO element) throws DAOException {
		try{
			element = entityManager.merge(element);
			entityManager.flush();
			return element.getTagId();
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in create method",e);
		}
	}

	@Override
	public void update(TagTO element) throws DAOException {
		try{
			TagTO tag = entityManager.find(TagTO.class, element.getTagId());
			tag.setNewsList(element.getNewsList());
			tag.setTagName(element.getTagName());
			entityManager.merge(tag);
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in update method",e);
		}
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		TagTO tag=null;
		try{
			for(Long id:elements){
				tag = entityManager.find(TagTO.class,id);
				entityManager.remove(tag);
			}
			entityManager.getEntityManagerFactory().getCache().evictAll();
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in delete method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TagTO> getList() throws DAOException {
		try{
			List<TagTO> list = entityManager.createQuery("SELECT tags FROM TagTO tags").getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getList method",e);
		}
	}

	@Override
	public TagTO getById(Long id) throws DAOException {
		try{
			TagTO tag = entityManager.find(TagTO.class, id);
			return tag;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getList getById",e);
		}
	}

	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws DAOException {
		try{
			TagTO tag = entityManager.find(TagTO.class, tagId);
			List<NewsTO> list = tag.getNewsList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNewsByTag getById",e);
		}
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<TagTO> getTagsByIds(List<Long> ids) throws DAOException {
		try{
			String jpql = SQLBuilder.insertIds(ids, "SELECT tag FROM TagTO tag WHERE tag.tagId IN #");
			List<TagTO> list = entityManager.createQuery(jpql).getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getTagsByIds getById",e);
		}
	}

}
