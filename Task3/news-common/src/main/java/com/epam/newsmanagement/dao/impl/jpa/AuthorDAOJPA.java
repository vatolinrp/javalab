package com.epam.newsmanagement.dao.impl.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;





//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

public class AuthorDAOJPA implements IAuthorDAO{
	@PersistenceContext
	private EntityManager entityManager;
	@Transactional
	@Override
	public Long create(AuthorTO element) throws DAOException {
		try{
			element = entityManager.merge(element);
			entityManager.flush();
			return element.getAuthorId();
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in create method",e);
		}
		
	}
	@Transactional
	@Override
	public void update(AuthorTO element) throws DAOException {
		try{
			AuthorTO author = entityManager.find(AuthorTO.class, element.getAuthorId());
			author.setExpireDate(element.getExpireDate());
			author.setName(element.getName());
			author.setNewsList(element.getNewsList());
			entityManager.merge(author);
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in update method",e);
		}
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		//We do not delete authors!
	}
	
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<AuthorTO> getList() throws DAOException {
		try{
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<AuthorTO> criteria = builder.createQuery(AuthorTO.class);
			Root<AuthorTO> pRoot = criteria.from(AuthorTO.class);
			Query query = entityManager.createQuery(criteria.where(builder.isNull(pRoot.get("expireDate"))));
			List<AuthorTO> list = query.getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getList method",e);
		}
	}
	
	@Override
	@Transactional
	public AuthorTO getById(Long id) throws DAOException {
		try{
			AuthorTO author = entityManager.find(AuthorTO.class, id);
			return author;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getById method",e);
		}
		
	}
	
	@Override
	@Transactional
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		try{
			AuthorTO author = entityManager.find(AuthorTO.class, authorId);
			List<NewsTO> list = author.getNewsList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNewsListByAuthor method",e);
		}
	}
	
	@Override
	@Transactional
	public void setExpired(Long authorId) throws DAOException {
		try{
			AuthorTO author = entityManager.find(AuthorTO.class, authorId);
			author.setExpireDate(new Date());
			entityManager.merge(author);
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in setExpired method",e);
		}
	}

}
