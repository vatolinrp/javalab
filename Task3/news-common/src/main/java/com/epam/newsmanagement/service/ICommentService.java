package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.CommentTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ICommentService extends ICommonService<CommentTO> {
	
}