package com.epam.newsmanagement.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;



/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface INewsDAO extends ICommonDAO<NewsTO> {
	/**
	 * Gets list of news by selected ids
	 * 
	 * @param listOfNewsId
	 * @return List<NewsTO> - list of news, which has news with selected ids
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByIds(List<Long> listOfNewsId) throws DAOException;
	
	/**
	 * Gets the number of all news
	 * @return number of all news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Integer getNewsNum() throws DAOException;
	/**
	 * Gets news by page
	 * @param page - the page number
	 * @param perPage - number of news per page
	 * @return List<NewsTO> - list of 
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsTOsByPage(Integer page,Integer perPage) throws DAOException;
	/**
	 * Gets next or previous news
	 * @param newsId - the news id of the former news
	 * @param isNext - true if needed next, false - if previous
	 * @return NewsTO - needed news instance
	 * @throws DAOException if any exceptions occur in DAO
	 */
	NewsTO getNews(Long newsId,boolean isNext) throws DAOException;
	/**
	 * Gets list of filtered news for page 
	 * @param filter - filter param
	 * @param page - the page number
	 * @param perPage - number of news per page
	 * @return List<NewsTO> - list of expected news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page, Integer perPage) throws DAOException;
	/**
	 * @param newsId - selected news id
	 * @return List<CommentTO> - list of comment which belong to the news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException;
	/**
	 * Gets one news by filter
	 * @param filter - filter object
	 * @param newsId - the id of the current news
	 * @param isNextNeeded - true if next needed, false - if previous
	 * @return NewsTO - expected news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	NewsTO getOneFilteredNews(FilterVO filter, Long newsId, boolean isNextNeeded) throws DAOException;
	/**
	 * Gets number of filtered news
	 * @param filter - filter param
	 * @return Integer - number of filtered news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Integer getFilteredNewsNum(FilterVO filter) throws DAOException;
	/**
	 * Gets a map of comments by news ids
	 * @param newsIds selected ids
	 * @return Map<Long, ArrayList<CommentTO>> wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds) throws DAOException;
	/**
	 *  Gets tags by selected news id
	 * @param newsId - selected news id
	 * @return List<TagTO> - tags, which news has
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<TagTO> getTagsByNewsId(Long newsId) throws DAOException;
	/**
	 * Gets a map of tags by news ids
	 * @param newsIds selected ids
	 * @return Map<Long, ArrayList<TagTO>> wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds) throws DAOException;
	/**
	 * 
	 * @param newsId - selected news' ID
	 * @return AuthorTO - the author of the selected news
	 * @throws DAOException  if any exceptions occur in DAO
	 */
	AuthorTO getAuthorByNewsId(Long newsId) throws DAOException;
	/**
	 * Gets a map of authors by news ids
	 * @param newsIds - selected ids
	 * @return Map<Long, AuthorTO> - wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds) throws DAOException;
}
