package com.epam.newsmanagement.service.util;

import com.epam.newsmanagement.dao.util.FilterVO;

public class ServiceUtil {
	/**
	 * Gets code of filtering
	 * @param filter - filter param
	 * @return Integer - filterCode
	 */
	public static Integer getFilterCode(FilterVO filter){
		boolean isAuthorNull=false;
		boolean isTagsNull=false;
		if(filter.getAuthorId().equals(0l)){
			isAuthorNull=true;
		}
		if(filter.getTagIds().length==0){
			isTagsNull=true;
		}
		if((!isAuthorNull)&&(!isTagsNull)){
			return 3;
		}
		if(isAuthorNull){
			return 2;
		}
		if(isTagsNull){
			return 1;
		}
		return 0;
	}
	/**
	 * Returns true, if filter's criterias is null
	 * @param filter - filter param
	 * @return boolean answer
	 */
	public static boolean isFilterNull(FilterVO filter){
		if((filter.getAuthorId()==null)&&(filter.getTagIds()==null)){
			return true;
		}
		return false;
	}
}
