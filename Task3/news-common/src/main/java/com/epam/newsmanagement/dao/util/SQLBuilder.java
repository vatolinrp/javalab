package com.epam.newsmanagement.dao.util;

import java.util.Arrays;
import java.util.List;
/**
 * This class is used to get build sql query for filtering
 * 
 * @author Rostislav_Vatolin
 *
 */
public class SQLBuilder {
	
	public static String  getSQLForOneFilteredNews(FilterVO filter,Long id, boolean isNextNeeded){
		Integer category = getFilterCategory(filter);
		if(category>0){
			return oneFilteredNews(category,filter,id,isNextNeeded);
		}
		return null;
	}
	public static String getSQLForPartOfNews(Integer page, Integer perPage){
		return getPartOfNews(page,perPage);
	}
	
	public static String getSQLForPageOfFilteredNews(FilterVO filter,
			Integer page, Integer perPage) {
		int category = getFilterCategory(filter);
		if(category>0){
			return pageOfFilteredNews(category, filter,page,perPage);
		}
		return null;
	}
	public static String getSQLForNumOfFilteredNews(FilterVO filter) {
		int category = getFilterCategory(filter);
		if(category>0){
				return numOfFilteredNews(category,filter);
		}	
		return null;
	}
	public static String getSQLForNews(Long currentNewsId, boolean isNextNeeded){
		return getNews(currentNewsId,isNextNeeded);
	}
	
	
	public static String insertIds(List<Long> idList, String ql){
		String ids = idList.toString();
		ids = ids.replace("[", "(");
		ids = ids.replace("]", ")");
		return ql.replaceFirst("#", ids);
	}
	
	//----------------------------------helpful methods-------------------------------------
	
	private static String numOfFilteredNews(Integer category, FilterVO filter) {
		StringBuilder sb = new StringBuilder(
				"	SELECT COUNT(news_id) "+
				"	FROM news "+
				"	WHERE news.NEWS_ID IN "
					);
		String filterParam = getParamStr(category, filter);
		sb.append(filterParam);
		return sb.toString();
	}

	private static int getFilterCategory(FilterVO filter){
		boolean isAuthorNull=false;
		boolean isTagsNull=false;
		if((filter.getAuthorId()==null)||(filter.getAuthorId().equals(0l))){
			isAuthorNull=true;
		}
		if((filter.getTagIds()==null)||(filter.getTagIds().length==0)){
			isTagsNull=true;
		}
		if((!isAuthorNull)&&(!isTagsNull)){
			return 3;
		}
		if(isAuthorNull&&isTagsNull){
			return -1;
		}
		if(isAuthorNull){
			return 2;
		}
		if(isTagsNull){
			return 1;
		}
		return 0;
	}
	private static String getNews(Long currentNewsId, boolean isNextNeeded) {
		String sql = new String(
			"	SELECT news_id,"+
			"	  modification_date,"+
			"	  SHORT_TEXT,"+
			"	  full_text,"+
			"	  creation_date,"+
			"	  title"+
			"	FROM"+
			"	  (SELECT news.NEWS_ID,"+
			"	    news.modification_date,"+
			"	    news.SHORT_TEXT,"+
			"	    news.full_text,"+
			"	    news.creation_date,"+
			"	    news.title,"+
			"	    Row_number() over(ORDER BY NVL(com.comment_count,0) DESC,news.modification_date DESC) R"+
			"	  FROM news"+
			"	  LEFT JOIN"+
			"	    (SELECT comments.news_id,"+
			"	      COUNT(comments.news_id) AS comment_count"+
			"	    FROM COMMENTS"+
			"	    GROUP BY comments.news_id"+
			"	    ) com"+
			"	  ON news.NEWS_ID=com.news_id"+
			"	  )"+
			"	WHERE R=("+
			"	  (SELECT R"+
			"	  FROM"+
			"	    (SELECT news.NEWS_ID,"+
			"	      Row_number() over (ORDER BY NVL(com.comment_count,0) DESC, news.modification_date DESC) R"+
			"	    FROM news"+
			"	    LEFT JOIN"+
			"	      (SELECT comments.news_id,"+
			"	        COUNT(comments.news_id) AS comment_count"+
			"	      FROM COMMENTS"+
			"	      GROUP BY comments.news_id"+
			"	      ) com"+
			"	    ON news.NEWS_ID=com.news_id"+
			"	    )"+
			"	  WHERE news_id=#"+
			"	  )#)"
				);
		sql = sql.replaceFirst("#", currentNewsId.toString());
		if(isNextNeeded){
			sql = sql.replaceFirst("#", "+1");
		}
		else{
			sql = sql.replaceFirst("#", "-1");
		}
		return sql;
	}
	private static String getPartOfNews(Integer page, Integer perPage) {
		String sql = 
			"	SELECT news_id,"+
			"	  modification_date,"+
			"	  SHORT_TEXT,"+
			"	  full_text,"+
			"	  creation_date,"+
			"	  title"+
			"	FROM"+
			"	  (SELECT news.NEWS_ID,"+
			"	    news.SHORT_TEXT,"+
			"	    news.FULL_TEXT,"+
			"	    news.CREATION_DATE,"+
			"	    news.MODIFICATION_DATE,"+
			"	    news.title,"+
			"	    Row_number() over (ORDER BY NVL(com.comment_count,0) DESC, news.modification_date DESC) R"+
			"	  FROM news"+
			"	  LEFT JOIN"+
			"	    (SELECT comments.news_id,"+
			"	      COUNT(comments.news_id) AS comment_count"+
			"	    FROM COMMENTS"+
			"	    GROUP BY comments.news_id"+
			"	    ) com"+
			"	  ON news.NEWS_ID=com.news_id"+
			"	  )"+
			"	WHERE R BETWEEN # AND #";
		sql = sql.replaceFirst("#",  (new Integer(page * perPage - (perPage - 1))).toString());
		sql = sql.replaceFirst("#", (new Integer(page * perPage).toString())); 
		return sql;
	}
	private static String oneFilteredNews(Integer category, FilterVO filter,Long id,boolean isNextNeeded){
		StringBuilder sb = new StringBuilder(
			"	SELECT news_id,"+
			"	  short_text,"+
			"	  full_text,"+
			"	  title,"+
			"	  creation_date,"+
			"	  modification_date"+
			"	FROM"+
			"	  (SELECT n.NEWS_ID,"+
			"	    n.short_text,"+
			"	    n.full_text,"+
			"	    n.title,"+
			"	    n.creation_date,"+
			"	    n.modification_date,"+
			"	    Row_number() over (ORDER BY NVL(com.comment_count,0) DESC,n.modification_date DESC) R"+
			"	  FROM"+
			"	    (SELECT news.news_id,"+
			"	      news.short_text,"+
			"	      news.full_text,"+
			"	      news.title,"+
			"	      news.creation_date,"+
			"	      news.MODIFICATION_DATE"+
			"	    FROM news"+
			"	    WHERE news.NEWS_ID IN"
				    );
		String filterParam = getParamStr(category,filter);
		sb.append(filterParam);
		sb.append(
				
			"	) n LEFT JOIN"+
			"	(SELECT comments.news_id,"+
			"	  COUNT(comments.news_id) AS comment_count"+
			"	FROM COMMENTS"+
			"	GROUP BY comments.news_id"+
			"	) com ON n.NEWS_ID=com.news_id ) WHERE r =("+
			"	(SELECT r FROM"+
			"	  (SELECT n.NEWS_ID,"+
			"	    n.short_text,"+
			"	    n.full_text,"+
			"	    n.title,"+
			"	    n.creation_date,"+
			"	    n.modification_date,"+
			"	    Row_number() over (ORDER BY NVL(com.comment_count,0) DESC, n.modification_date DESC) R"+
			"	  FROM"+
			"	    (SELECT news.news_id,"+
			"	      news.short_text,"+
			"	      news.full_text,"+
			"	      news.title,"+
			"	      news.creation_date,"+
			"	      news.MODIFICATION_DATE"+
			"	    FROM news"+
			"	    WHERE news.NEWS_ID IN"
				);
		sb.append(filterParam);
		sb.append(
			"	) n LEFT JOIN"+
			"	(SELECT comments.news_id,"+
			"	  COUNT(comments.news_id) AS comment_count"+
			"	FROM COMMENTS"+
			"	GROUP BY comments.news_id"+
			"	) com ON n.NEWS_ID=com.news_id )"
			);
		sb.append("WHERE news_id=");
		sb.append(id);
		sb.append(" ) ");
		if(isNextNeeded){
			sb.append("+1");
		}
		else{
			sb.append("-1");
		}
		sb.append(")");
		return sb.toString();
	}
	
	private static String pageOfFilteredNews(Integer category, FilterVO filter,
			Integer page, Integer perPage) {
		StringBuilder sb = new StringBuilder(
				"	SELECT news_id,"+
				"	  short_text,"+
				"	  full_text,"+
				"	  title,"+
				"	  creation_date,"+
				"	  modification_date"+
				"	FROM"+
				"	  (SELECT n.NEWS_ID,"+
				"	    n.short_text,"+
				"	    n.full_text,"+
				"	    n.title,"+
				"	    n.creation_date,"+
				"	    n.modification_date,"+
				"	    Row_number() over (ORDER BY NVL(com.comment_count,0) DESC,n.modification_date DESC) R"+
				"	  FROM"+
				"	    (SELECT news.news_id,"+
				"	      news.short_text,"+
				"	      news.full_text,"+
				"	      news.title,"+
				"	      news.creation_date,"+
				"	      news.MODIFICATION_DATE"+
				"	    FROM news"+
				"	    WHERE news.NEWS_ID IN"
					    );
		String filterParam = getParamStr(category,filter);
		sb.append(filterParam);
		sb.append(
				"	) n LEFT JOIN"+
				"	(SELECT comments.news_id,"+
				"	  COUNT(comments.news_id) AS comment_count"+
				"	FROM COMMENTS"+
				"	GROUP BY comments.news_id"+
				"	) com ON n.NEWS_ID=com.news_id )"
				);
		sb.append("WHERE r BETWEEN ");
		sb.append(page * perPage - (perPage - 1));
		sb.append("AND ");
		sb.append(page * perPage);
		return sb.toString();
	}
	private static String getParamStr(Integer category, FilterVO filter){
		String filterParamStr=null;
		String tagidsStr=null;
		switch(category)
		{
		case 1:
			//only by author
			filterParamStr =
			"(SELECT NEWS_AUTHOR.NEWS_ID "+ 
			"FROM NEWS_AUTHOR "+
			"WHERE NEWS_AUTHOR.AUTHOR_ID =#)";
			filterParamStr = filterParamStr.replaceFirst("#", String.valueOf(filter.getAuthorId()));
			break;
		case 2:
			//only by tags
			filterParamStr =
			"(SELECT NEWS_TAG.NEWS_ID "+ 
			"FROM NEWS_TAG "+
			"WHERE NEWS_TAG.TAG_ID IN #	)";
			tagidsStr = Arrays.asList(filter.getTagIds()).toString();
			tagidsStr = tagidsStr.replace('[', '(');
			tagidsStr = tagidsStr.replace(']', ')');
			filterParamStr = filterParamStr.replaceFirst("#", tagidsStr);
			break;
		case 3:
			//by author and tags
			filterParamStr =
			"(SELECT NEWS_AUTHOR.NEWS_ID "+
			"FROM NEWS_AUTHOR "+
			"JOIN NEWS_TAG "+
			"ON NEWS_TAG.NEWS_ID=NEWS_AUTHOR.NEWS_ID "+
			"WHERE NEWS_TAG.TAG_ID IN # "+
			"AND NEWS_AUTHOR.AUTHOR_ID = # )";		
			tagidsStr = Arrays.asList(filter.getTagIds()).toString();
			tagidsStr = tagidsStr.replace('[', '(');
			tagidsStr = tagidsStr.replace(']', ')');
			filterParamStr = filterParamStr.replaceFirst("#", tagidsStr);
			filterParamStr = filterParamStr.replaceFirst("#", String.valueOf(filter.getAuthorId()));
			break;
		}
		return filterParamStr;
	}
	
}
