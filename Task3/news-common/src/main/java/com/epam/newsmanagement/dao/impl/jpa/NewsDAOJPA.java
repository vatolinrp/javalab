package com.epam.newsmanagement.dao.impl.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;


//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;


@Transactional
//@Repository
public class NewsDAOJPA implements INewsDAO{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	
	public Long create(NewsTO element) throws DAOException {
		try{
			entityManager.persist(element);
			entityManager.flush();
			return element.getNewsId();
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in create method",e);
		}
	}

	@Override
	
	public void update(NewsTO element) throws DAOException {
		try{
			NewsTO news = entityManager.find(NewsTO.class,element.getNewsId());
			news.setAuthor(element.getAuthor());
			news.setTagList(element.getTagList());
			news.setCommentList(element.getCommentList());
			news.setCreationDate(element.getCreationDate());
			news.setFullText(element.getFullText());
			news.setModificationDate(element.getModificationDate());
			news.setShortText(element.getShortText());
			news.setTitle(element.getTitle());
			entityManager.merge(news);
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in update method",e);
		}
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		try{
			NewsTO news=null;
			for(Long id:elements){
			news = entityManager.find(NewsTO.class,id);
			entityManager.remove(news);
			}
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in delete method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getList() throws DAOException {
		try{
			List<NewsTO> list = entityManager.createQuery("SELECT news FROM NewsTO news").getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getList method",e);
		}
	}

	@Override
	public NewsTO getById(Long id) throws DAOException {
		try{
			NewsTO news = entityManager.find(NewsTO.class, id);
			return news;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getById method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getNewsByIds(List<Long> listOfNewsId)
			throws DAOException {
		try{
			String jpql = SQLBuilder.insertIds(listOfNewsId, "SELECT news FROM NewsTO news WHERE news.newsId IN #");
			List<NewsTO> list = entityManager.createQuery(jpql).getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNewsByIds method",e);
		}
	}

	@Override
	public Integer getNewsNum() throws DAOException {
		try{
			Query q = entityManager.createQuery("SELECT COUNT(news.newsId) FROM NewsTO news");
			Integer count = (new Long(((long)q.getSingleResult()))).intValue();
			return count;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNewsNum method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws DAOException {
		try{
			String sql = SQLBuilder.getSQLForPartOfNews(page, perPage);
			Query q = entityManager.createNativeQuery(sql, NewsTO.class);
			List<NewsTO> list = q.getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNewsTOsByPage method",e);
		}
	}

	@Override
	public NewsTO getNews(Long newsId, boolean isNextNeeded) throws DAOException {
		try{
			String sql = SQLBuilder.getSQLForNews( newsId, isNextNeeded);
			Query q = entityManager.createNativeQuery(sql, NewsTO.class);
			NewsTO news=null;
			try{
				news = (NewsTO)q.getSingleResult();
			}
			catch(NoResultException ex){
				news = getById(newsId);
			}
			return news;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getNews method",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws DAOException {
		try{
			String sql = SQLBuilder.getSQLForPageOfFilteredNews(filter, page, perPage);
			Query q = entityManager.createNativeQuery(sql, NewsTO.class);
			List<NewsTO> list = q.getResultList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getPageOfFilteredNews method",e);
		}
	}

	@Override
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId,
			boolean isNextNeeded) throws DAOException {
		try{
			String sql = SQLBuilder.getSQLForOneFilteredNews(filter, newsId, isNextNeeded);
			Query q = entityManager.createNativeQuery(sql, NewsTO.class);
			NewsTO news=null;
			try{
				news = (NewsTO)q.getSingleResult();
			}
			catch(NoResultException ex){
				news = getById(newsId);
			}
			return news;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getOneFilteredNews method",e);
		}
	}

	@Override
	public Integer getFilteredNewsNum(FilterVO filter) throws DAOException {
		Integer newsNum=null;
		try{
			String sql = SQLBuilder.getSQLForNumOfFilteredNews(filter);
			Query q = entityManager.createNativeQuery(sql);
			newsNum = (((BigDecimal)q.getSingleResult())).intValue();	
			return newsNum;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getFilteredNewsNum method",e);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException {
		try{
			NewsTO news = getById(newsId);
			List<CommentTO> list = news.getCommentList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getCommentsByNewsId method",e);
		}
	}

	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws DAOException {
		try{
			NewsTO news = null;
			Map<Long, ArrayList<CommentTO>> resultMap = new HashMap<Long, ArrayList<CommentTO>>();
			for (Long id : newsIds) {
				news = getById(id);
				ArrayList<CommentTO> list = new ArrayList<CommentTO>(news.getCommentList());
				resultMap.put(id, list);
			}
			return resultMap;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getMapComments method",e);
		}
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws DAOException {
		try{
			NewsTO news = getById(newsId);
			List<TagTO> list = news.getTagList();
			return list;
		}
		catch(PersistenceException e){
			throw new DAOException("PersistenceException in getTagsByNewsId method",e);
		}
	}

	@Override
	public Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws DAOException {
		NewsTO news = null;
		try {
			Map<Long, ArrayList<TagTO>> resultMap = new HashMap<Long, ArrayList<TagTO>>();
			for (Long id : newsIds) {
				news = getById(id);
				List<TagTO> tagList = news.getTagList();
				resultMap.put(id, new ArrayList<TagTO>(tagList));
			}
			return resultMap;
		} catch (PersistenceException e) {
			throw new DAOException("PersistenceException in getMapTags method",e);
		}
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		try{
			NewsTO news = getById(newsId);
			AuthorTO author = news.getAuthor();
			return author;
		} catch (PersistenceException e) {
			throw new DAOException("PersistenceException in getAuthorByNewsId method",e);
		}
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds) throws DAOException {
		try{
			Map<Long, AuthorTO> resultMap = new HashMap<Long, AuthorTO>();
			NewsTO news = null;
			for(Long id:newsIds){
				news=getById(id);
				resultMap.put(id,news.getAuthor());
			}
			return resultMap;
		} catch (PersistenceException e) {
			throw new DAOException("PersistenceException in getMapAuthors method",e);
		}
	}

}
