package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

@Repository
public class AuthorDAOH implements IAuthorDAO {
	private final String SQL_GET_LIST_NEWS_BY_AUTHOR = "SELECT news.news_Id, news.short_Text, news.full_Text,"
			+ " news.title, news.creation_Date, news.modification_Date "
			+ "FROM NEWS INNER JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE author_Id = :author_id";
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	
	public Long create(AuthorTO element) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			Long id = (Long) session.save(element);
			return id;
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in create method!", e);
		}
	}

	@Override
	@Transactional
	public void update(AuthorTO element) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			session.update(element);
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in update method!", e);
		}

	}

	@Override
	@Transactional
	public void delete(List<Long> elements) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			for (Long id : elements) {
				AuthorTO author = (AuthorTO) session.get(AuthorTO.class, id);
				session.delete(author);
			}
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in delete method!", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<AuthorTO> getList() throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(AuthorTO.class);
			cr.add(Restrictions.isNull("expireDate"));
			List<AuthorTO> list = cr.list();
			return list;
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getList method!", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public AuthorTO getById(Long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(AuthorTO.class);
			cr.add(Restrictions.eq("authorId", id));
			List<AuthorTO> list = cr.list();
			if (list.size() != 0) {
				return list.get(0);
			} else {
				return null;
			}
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getById method!", e);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createSQLQuery(SQL_GET_LIST_NEWS_BY_AUTHOR);
			query.setLong("author_id", authorId);
			List<NewsTO> list = query.list();
			return list;
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getNewsListByAuthor method!", e);
		}
	}

	@Override
	@Transactional
	public void setExpired(Long authorId) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			AuthorTO existingAuthor = (AuthorTO) session.get(AuthorTO.class,authorId);
			existingAuthor.setExpireDate(new Date());
			session.update(existingAuthor);
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in setExpired method!", e);
		}
	}

}
