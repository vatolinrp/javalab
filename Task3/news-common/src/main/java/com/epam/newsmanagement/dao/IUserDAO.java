package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;
/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IUserDAO {
	/**
	 * Gets user by selected login
	 * @param login - selected login
	 * @return UserTO object, which has our login
	 * @throws DAOException if any exceptions occur in DAO
	 */
	UserTO getUserByLogin(String login) throws DAOException;
}
