package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class AuthorService implements IAuthorService {
	private final Logger logger = Logger.getLogger(AuthorService.class);
	@Autowired
	private IAuthorDAO authorDAO;

	@Override
	public Long create(AuthorTO author) throws ServiceException {
		try {
			return authorDAO.create(author);
		} catch (DAOException e) {
			logger.error("DAOException in create method!", e);
			throw new ServiceException("DAOException in create method!", e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			return (authorDAO
					.getNewsListByAuthor(authorId));
		} catch (DAOException e) {
			logger.error("DAOException in getNewsByAuthor method!", e);
			throw new ServiceException("DAOException in getNewsByAuthor method!",e);
		}
	}

	

	@Override
	public List<AuthorTO> getList() throws ServiceException {
		try {
			return (authorDAO.getList());
		} catch (DAOException e) {
			logger.error("DAOException in getList method!", e);
			throw new ServiceException("DAOException in getList method!", e);
		}
	}

	@Override
	public void update(List<AuthorTO> authors) throws ServiceException {
		try {
			for (AuthorTO author : authors) {
				authorDAO.update(author);
			}
		} catch (DAOException e) {
			logger.error("DAOException in updateAuthors method!", e);
			throw new ServiceException("DAOException in updateAuthors method!", e);
		}
	}

	@Override
	public void delete(List<Long> ids) throws ServiceException {
		try {
			authorDAO.delete(ids);
		} catch (DAOException e) {
			logger.error("DAOException in delete method!", e);
			throw new ServiceException("DAOException in delete method!", e);
		}
	}

	@Override
	public void setExpired(Long authorId) throws ServiceException {
		try {
			authorDAO.setExpired(authorId);
		} catch (DAOException e) {
			logger.error("DAOException in setExpired method!", e);
			throw new ServiceException("DAOException in setExpired method!", e);
		}
	}

	@Override
	public NewsVO setAuthor(NewsVO newsVO) throws ServiceException {
		newsVO.setAuthorTO(getById(Long.valueOf(newsVO.getAuthor())));
		return newsVO;
	}

	@Override
	public AuthorTO getById(Long element) throws ServiceException {
		try {
			return (authorDAO.getById(element));
		} catch (DAOException e) {
			logger.error("DAOException in getById method!", e);
			throw new ServiceException("DAOException in getById method!", e);
		}
	}

}