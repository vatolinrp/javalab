package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Rostislav_Vatolin
 *
 *         table named NEWS contains these entities in BD
 *
 */
@Entity
@Table(name="NEWS")
public class NewsTO implements Serializable {
	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = -8299786923007171499L;
	/**
	 * id of the news
	 */
	@Id
    @Column(name="NEWS_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEWS_SEQ")
    @SequenceGenerator(name="NEWS_SEQ", sequenceName="NEWS_SEQ", allocationSize=1)
	private Long newsId;
	/**
	 * short description of the news
	 */
	@Column(name="SHORT_TEXT")
	private String shortText;
	/**
	 * full description of the news
	 */
	@Column(name="FULL_TEXT")
	private String fullText;
	/**
	 * title of the news
	 */
	@Column(name="TITLE")
	private String title;
	/**
	 * Creation date of the news
	 */
	@Column(name="CREATION_DATE")
	private Date creationDate;
	/**
	 * Modification date of the news
	 */
	@Column(name="MODIFICATION_DATE")
	private Date modificationDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="newsTO", cascade = { CascadeType.REMOVE })
	private List<CommentTO> commentList;
	
	
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "NEWS_TAG",
	joinColumns = @JoinColumn(name="NEWS_ID"),
	inverseJoinColumns = @JoinColumn(name="TAG_ID"))
	private List <TagTO> tagList;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinTable(name="NEWS_AUTHOR",
	joinColumns= @JoinColumn(name="NEWS_ID", referencedColumnName="NEWS_ID"), 
	inverseJoinColumns= @JoinColumn(name="AUTHOR_ID", referencedColumnName="AUTHOR_ID", unique=true))
	private AuthorTO author;
	
	public NewsTO() {
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public List<CommentTO> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<CommentTO> commentList) {
		this.commentList = commentList;
	}

	public List <TagTO> getTagList() {
		return tagList;
	}

	public void setTagList(List <TagTO> tagList) {
		this.tagList = tagList;
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

}
