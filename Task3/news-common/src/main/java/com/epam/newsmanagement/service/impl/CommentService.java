package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class CommentService implements ICommentService {
	private final Logger logger = Logger.getLogger(CommentService.class);
	@Autowired
	private ICommentDAO commentDAO;

	@Override
	public Long create(CommentTO comment) throws ServiceException {
		try {
			return commentDAO.create(comment);
		} catch (DAOException e) {
			logger.error("DAOException in create method!", e);
			throw new ServiceException("DAOException in create method!", e);
		}

	}

	@Override
	public void delete(List<Long> commentIds) throws ServiceException {
		try {
			commentDAO.delete(commentIds);
		} catch (DAOException e) {
			logger.error("DAOException in delete method!", e);
			throw new ServiceException("DAOException in delete method!", e);
		}
	}

	@Override
	public List<CommentTO> getList() throws ServiceException {
		try {
			return commentDAO.getList();
		} catch (DAOException e) {
			logger.error("DAOException in getList method!", e);
			throw new ServiceException("DAOException in getList method!", e);
		}
	}

	@Override
	public void update(List<CommentTO> elements) throws ServiceException {
		try {
			for (CommentTO com : elements) {
				commentDAO.update(com);
			}
		} catch (DAOException e) {
			logger.error("DAOException in update method!", e);
			throw new ServiceException("DAOException in update method!", e);
		}

	}

	@Override
	public CommentTO getById(Long element) throws ServiceException {
		try {
			commentDAO.getById(element);
		} catch (DAOException e) {
			logger.error("DAOException in getById method!", e);
			throw new ServiceException("DAOException in getById method!", e);
		}
		return null;
	}
}