package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Rostislav_Vatolin
 *
 *         table named AUTHOR contains these entities in BD
 * 
 */
@Entity
@Table(name="AUTHOR")
public class AuthorTO implements Serializable {
	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = -2643421797281708593L;
	/**
	 * author's id
	 */
	@Id
    @Column(name="AUTHOR_ID")
	@SequenceGenerator(name="generator", sequenceName="AUTHORS_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="generator")
	private Long authorId;
	/**
	 * author's name
	 */
	@Column(name="NAME")
	private String name;
	/**
	 * the expire author's date
	 */
	@Column(name="EXPIRED")
	private Date expireDate;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="author")
	private List<NewsTO> newsList;
	
	public List<NewsTO> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<NewsTO> newsList) {
		this.newsList = newsList;
	}

	public AuthorTO() {
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((expireDate == null) ? 0 : expireDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (expireDate == null) {
			if (other.expireDate != null)
				return false;
		} else if (!expireDate.equals(other.expireDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthorTO [authorId=" + authorId + ", name=" + name
				+ ", expireDate=" + expireDate + "]";
	}

}
