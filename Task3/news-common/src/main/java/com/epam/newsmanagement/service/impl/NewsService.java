package com.epam.newsmanagement.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class NewsService implements INewsService {
	private final Logger logger = Logger.getLogger(NewsService.class);

	@Autowired
	private INewsDAO newsDAO;

	@Override
	public Long create(NewsTO news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			logger.error("DAOException in create method!", e);
			throw new ServiceException("DAOException in create method!", e);
		}
	}

	@Override
	public void delete(List<Long> newsIds) throws ServiceException {
		try {
			newsDAO.delete(newsIds);
		} catch (DAOException e) {
			logger.error("DAOException in delete method!", e);
			throw new ServiceException("DAOException in delete method!", e);
		}
	}

	@Override
	public NewsTO getById(Long newsId) throws ServiceException {
		try {
			return newsDAO.getById(newsId);
		} catch (DAOException e) {
			logger.error("DAOException in viewNews method!", e);
			throw new ServiceException("DAOException in viewNews method!", e);
		}
	}

	@Override
	public Integer getNumberOfPages(Integer perPage) throws ServiceException {
		try {
			Integer newsCount = newsDAO.getNewsNum();
			if (newsCount % perPage != 0) {
				return (newsCount / perPage + 1);
			} else {
				return (newsCount / perPage);
			}
		} catch (DAOException e) {
			logger.error("DAOException in getNumberOfPages method!", e);
			throw new ServiceException("DAOException in getNumberOfPages method!",
					e);
		}

	}

	@Override
	public NewsTO getNextNews(Long newsId) throws ServiceException {
		try {
			return (newsDAO.getNews(newsId, true));
		} catch (DAOException e) {
			logger.error("DAOException in getNextNews method!", e);
			throw new ServiceException("DAOException in getNextNews method!", e);
		}
	}

	@Override
	public NewsTO getPreviousNews(Long newsId) throws ServiceException {
		try {
			return newsDAO.getNews(newsId, false);
		} catch (DAOException e) {
			logger.error("DAOException in getPreviousNews method!", e);
			throw new ServiceException("DAOException in getPreviousNews method!",
					e);
		}

	}

	@Override
	public Integer getNumberOfCustomPages(FilterVO filter,
			Integer perPage) throws ServiceException {
		Integer newsCount = 0;
		try {
			newsCount = newsDAO.getFilteredNewsNum(filter);
			if (newsCount % perPage != 0) {
				return (newsCount / perPage + 1);
			} else {
				return (newsCount / perPage);
			}
		} catch (DAOException e) {
			logger.error("DAOException in getNumberOfCustomPages method!", e);
			throw new ServiceException("DAOException in getNumberOfCustomPages method!", e);
		}
	}

	@Override
	public NewsVO setNewsTO(NewsVO newsVO, Date date) throws ServiceException {
		NewsTO newsTO = newsVO.getNewsTO();
		if (newsTO.getNewsId() != null) {
			NewsTO newsTOdates = null;
			newsTOdates = getById(newsTO.getNewsId());
			newsTO.setCreationDate(newsTOdates.getCreationDate());
			newsTO.setModificationDate(date);
		} else {
			newsTO.setCreationDate(date);
			newsTO.setModificationDate(date);
		}
		newsVO.setNewsTO(newsTO);
		return newsVO;
	}

	@Override
	public Date getDate(NewsVO newsVO) throws ServiceException, ParseException {
		Locale locale = LocaleContextHolder.getLocale();
		DateFormat formatRU = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat formatEN = new SimpleDateFormat("MM/dd/yyyy");
		if (locale.getLanguage().equals("ru")) {
			return formatRU.parse(newsVO.getDate());
		}
		if (locale.getLanguage().equals("en")) {
			return formatEN.parse(newsVO.getDate());
		}
		return new Date();

	}

	@Override
	public Boolean isPrevExist(Long newsId) throws ServiceException {
		Boolean prevExists = true;
		NewsTO prevNews = null;
		prevNews = getPreviousNews(newsId);
		if (prevNews.getNewsId().equals(newsId)) {
			prevExists = false;
		}
		return prevExists;
	}

	@Override
	public Boolean isNextExist(Long newsId) throws ServiceException {
		Boolean nextExists = true;
		NewsTO nextNews = null;
		nextNews = getNextNews(newsId);
		if (nextNews.getNewsId().equals(newsId)) {
			nextExists = false;
		}
		return nextExists;
	}

	@Override
	public List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws ServiceException {
		try {
			return newsDAO.getNewsTOsByPage(page, perPage);
		} catch (DAOException e) {
			logger.error("DAOException in getNewsTOsByPage method!", e);
			throw new ServiceException("DAOException in getNewsTOsByPage method!",e);
		}
	}
	@Override
	public List<NewsTO> getList() throws ServiceException {
		try {
			return newsDAO.getList();
		} catch (DAOException e) {
			logger.error("DAOException in getList method!", e);
			throw new ServiceException("DAOException in getList method!", e);
		}
	}

	@Override
	public void update(List<NewsTO> elements) throws ServiceException {
		try {
			for (NewsTO news : elements) {
				newsDAO.update(news);
			}
		} catch (DAOException e) {
			logger.error("DAOException in update method!", e);
			throw new ServiceException("DAOException in update method!", e);
		}

	}
	@Override
	public Boolean isPrevExist(FilterVO filter, Long newsId)
			throws ServiceException {
		NewsTO prevNews = null;
			prevNews = getOneFilteredNews(filter, newsId, false);
		Boolean prevExists = true;
		if (prevNews.getNewsId().equals(newsId)) {
			prevExists = false;
		}
		return prevExists;
	}

	@Override
	public Boolean isNextExist(FilterVO filter, Long newsId)
			throws ServiceException {
		NewsTO nextNews = null;
		nextNews = getOneFilteredNews(filter, newsId, true);
		Boolean nextExists = true;
		if (nextNews.getNewsId().equals(newsId)) {
			nextExists = false;
		}
		return nextExists;
	}

	@Override
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId,
			boolean isNextNeeded) throws ServiceException {
		try {
			return newsDAO.getOneFilteredNews(filter, newsId, isNextNeeded);
		} catch (DAOException e) {
			logger.error("sql error in getOneFilteredNews method!", e);
			throw new ServiceException("sql error in getOneFilteredNews method!", e);
		}
	}

	@Override
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws ServiceException {
		try {
			return newsDAO.getPageOfFilteredNews(filter, page, perPage);
		} catch (DAOException e) {
			logger.error("DAOException in getPageOfFilteredNews method!", e);
			throw new ServiceException("DAOException in getPageOfFilteredNews method!", e);
		}
	}
	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId)
			throws ServiceException {
		try {
			return (newsDAO.getCommentsByNewsId(newsId));
		} catch (DAOException e) {
			logger.error("DAOException in getCommentsByNewsId method!", e);
			throw new ServiceException(
					"DAOException in getCommentsByNewsId method!", e);
		}
	}
	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws ServiceException {
		try {
			return newsDAO.getMapComments(newsIds);
		} catch (DAOException e) {
			logger.error("DAOException in getMapComments method!", e);
			throw new ServiceException("DAOException in getMapComments method!", e);
		}
	}
	@Override
	public Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws ServiceException {
		try {
			return newsDAO.getMapTags(newsIds);
		} catch (DAOException e) {
			logger.error("DAOException in getMapTags method!", e);
			throw new ServiceException("DAOException in getMapTags method!", e);
		}
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws ServiceException {
		try {
			return newsDAO.getTagsByNewsId(newsId);
		} catch (DAOException e) {
			logger.error("DAOException in getTagsByNewsId method!", e);
			throw new ServiceException("DAOException in getTagsByNewsId method!", e);
		}
	}
	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws ServiceException {
		try {
			return newsDAO.getMapAuthors(newsIds);
		} catch (DAOException e) {
			logger.error("DAOException in getMapAuthors method!", e);
			throw new ServiceException("DAOException in getMapAuthors method!", e);
		}
	}
	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		try {
			return (newsDAO
					.getAuthorByNewsId(newsId));
		} catch (DAOException e) {
			logger.error("DAOException in getAuthorByNewsId method!", e);
			throw new ServiceException(
					"DAOException in getAuthorByNewsId method!", e);
		}

	}

}