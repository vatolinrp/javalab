package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Rostislav_Vatolin
 *
 *         table named COMMENTS contains these entities in BD
 *
 */
@Entity
@Table(name="COMMENTS")
public class CommentTO implements Serializable {

	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = 320244195334683049L;

	/**
	 * id of the comment
	 */
	@Id
    @Column(name="COMMENT_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COMMENTS_SEQ")
    @SequenceGenerator(name="COMMENTS_SEQ", sequenceName="COMMENTS_SEQ", allocationSize=1)
	private Long commentId;
	/**
	 * content of the comment
	 */
	@Column(name="COMMENT_TEXT")
	private String commentText;
	/**
	 * creation date of the comment
	 */
	@Column(name="CREATION_DATE")
	private Date creationDate;
	/**
	 * news id, to which attached the comment
	 */
	@Column(name="NEWS_ID")
	private Long comNewsId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NEWS_ID", insertable=false, updatable=false)
	private NewsTO newsTO;

	public NewsTO getNewsTO() {
		return newsTO;
	}

	public void setNewsTO(NewsTO newsTO) {
		this.newsTO = newsTO;
	}

	public CommentTO() {
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long comentId) {
		this.commentId = comentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return comNewsId;
	}

	public void setNewsId(Long comNewsId) {
		this.comNewsId = comNewsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((comNewsId == null) ? 0 : comNewsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (comNewsId == null) {
			if (other.comNewsId != null)
				return false;
		} else if (!comNewsId.equals(other.comNewsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comments [comentId=" + commentId + ", commentText="
				+ commentText + ", creationDate=" + creationDate + ", newsId="
				+ comNewsId + "]";
	}

}
