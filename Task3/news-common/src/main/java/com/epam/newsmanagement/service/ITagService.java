package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ITagService extends ICommonService<TagTO> {
	
	/**
	 * Gets the list of news from NEWS table in DB by our custom tag
	 * 
	 * @param tagId
	 *            - the id of the selected tag
	 * @return List<NewsTO> - list of news by our tag
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws ServiceException;

	/**
	 * Gets tags by ids
	 * 
	 * @param ids
	 *            - the tag ids
	 * @return List<TagTO> - expected list
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<TagTO> getTagsByIds(List<Long> ids) throws ServiceException;

	/**
	 * Sets tags
	 * 
	 * @param newsVO
	 *            - where to set
	 * @return newsVO - result object
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setTags(NewsVO newsVO) throws ServiceException;
}