package com.epam.newsmanagement.dao.impl.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.dao.util.HibernateUtil;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;
@Repository
public class NewsDAOH implements INewsDAO{
	private final String SQL_GET_NEWS_BY_IDS = "SELECT news"
			+ " FROM NewsTO news WHERE news.newsId IN #";
	private SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public Long create(NewsTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Long id = (Long)session.save(element);
			return id;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in create method!",e);
		}
	}

	@Override
	public void update(NewsTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		NewsTO news = getById(element.getNewsId());
		news.setAuthor(element.getAuthor());
		news.setTagList(element.getTagList());
		news.setCommentList(element.getCommentList());
		news.setCreationDate(element.getCreationDate());
		news.setFullText(element.getFullText());
		news.setModificationDate(element.getModificationDate());
		news.setShortText(element.getShortText());
		news.setTitle(element.getTitle());
		session.update(news);
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			for(Long id:elements){
				NewsTO news = (NewsTO) session.get(NewsTO.class, id);
				session.delete(news);
			}
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in delete method!",e);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	
	public List<NewsTO> getList() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(NewsTO.class);
			List<NewsTO> list =  cr.list();
			return list;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getList method!",e);
		}
	}
	
	@Override
	
	public NewsTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(NewsTO.class);
			cr.add(Restrictions.eq("newsId", id));
			NewsTO news = (NewsTO)cr.uniqueResult();
			return news;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getById method!",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getNewsByIds(List<Long> listOfNewsId)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			String sql = SQLBuilder.insertIds(listOfNewsId, SQL_GET_NEWS_BY_IDS);
			List<NewsTO> newsList = session.createQuery(sql).list();
			return newsList;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getNewsByIds method!",e);
		}
	}

	@Override
	public Integer getNewsNum() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery("SELECT COUNT(news_id) FROM news");
			Integer newsNum = ((BigDecimal)query.uniqueResult()).intValue();
			return newsNum;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getNewsNum method!",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			String sql = SQLBuilder.getSQLForPartOfNews(page, perPage);
			Query query = session.createSQLQuery(sql).addEntity(NewsTO.class);
			List<NewsTO> list = query.list();
			return list;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getNewsTOsByPage method!",e);
		}
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public NewsTO getNews(Long newsId, boolean isNextNeeded) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		String sql = SQLBuilder.getSQLForNews(newsId, isNextNeeded);
		try{
			//SELECT news.title, news.modification_date, news.creation_date, news.short_text, news.news_id, news.full_text FROM testing.NEWS news
			Query query = session.createSQLQuery("select * from testing.NEWS").addEntity(NewsTO.class);
			
			List<NewsTO> list = query.list();
			System.out.println(list.toString());
			if((list.size()==0)){
				return getById(newsId);
			}
			return null;
			//return list.get(0);
			
		}
		catch(HibernateException e){
			
			throw new DAOException("HibernateException in getNews method!",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		String sql = SQLBuilder.getSQLForPageOfFilteredNews(filter, page, perPage);
		List<NewsTO> listOfNews = new LinkedList<NewsTO>();
		try{
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString()).addEntity(NewsTO.class);
				listOfNews = query.list();
			}
			else{
				return listOfNews;
			}
			return listOfNews;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getPageOfFilteredNews method!",e);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(NewsTO.class);
			cr.add(Restrictions.eq("newsId", newsId));
			NewsTO news = (NewsTO)cr.uniqueResult();
			return news.getCommentList();
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getById method!",e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId, boolean isNextNeeded) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		String sql = SQLBuilder.getSQLForOneFilteredNews(filter, newsId, isNextNeeded);
		List<NewsTO> listOfNews = new LinkedList<NewsTO>();
		try{
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString()).addEntity(NewsTO.class);
				listOfNews = query.list();
			}
			else{
				return getById(newsId);
			}
			if(listOfNews.size()!=0){
				return listOfNews.get(0);
			}
			else{
				return getById(newsId);
			}
			
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in create method!",e);
		}
	}

	@Override
	public Integer getFilteredNewsNum(FilterVO filter) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		Integer newsNum=null;
		tx = session.beginTransaction();
		String sql = SQLBuilder.getSQLForNumOfFilteredNews(filter);
		try{
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString());
				newsNum = ((BigDecimal)query.uniqueResult()).intValue();
				tx.commit();
			}
			else{
				return 0;
			}
			return newsNum;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in create method!",e);
		}
	
	}

	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws DAOException {
		try{
			NewsTO news = null;
			Map<Long, ArrayList<CommentTO>> resultMap = new HashMap<Long, ArrayList<CommentTO>>();
			for (Long id : newsIds) {
				news = getById(id);
				ArrayList<CommentTO> list = new ArrayList<CommentTO>(news.getCommentList());
				resultMap.put(id, list);
			}
			return resultMap;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getMapComments method",e);
		}
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws DAOException {
		try{
			NewsTO news = getById(newsId);
			List<TagTO> list = news.getTagList();
			return list;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getTagsByNewsId method",e);
		}
	}

	@Override
	public Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws DAOException {
		NewsTO news = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			Map<Long, ArrayList<TagTO>> resultMap = new HashMap<Long, ArrayList<TagTO>>();
			for (Long id : newsIds) {
				Criteria cr = session.createCriteria(NewsTO.class);
				cr.add(Restrictions.eq("newsId", id));
				news = (NewsTO)cr.uniqueResult();
				List<TagTO> tagList = news.getTagList();
				resultMap.put(id, new ArrayList<TagTO>(tagList));
			}
			return resultMap;
		} catch (HibernateException  e) {
			e.printStackTrace();
			throw new DAOException("HibernateException in getMapTags method",e);
		}
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		try{
			NewsTO news = getById(newsId);
			AuthorTO author = news.getAuthor();
			return author;
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getAuthorByNewsId method",e);
		}
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws DAOException {
		try{
			Map<Long, AuthorTO> resultMap = new HashMap<Long, AuthorTO>();
			NewsTO news = null;
			for(Long id:newsIds){
				news=getById(id);
				resultMap.put(id,news.getAuthor());
			}
			return resultMap;
		} catch (HibernateException e) {
			throw new DAOException("HibernateException in getMapAuthors method",e);
		}
	}

}
