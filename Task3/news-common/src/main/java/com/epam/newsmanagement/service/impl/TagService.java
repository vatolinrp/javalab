package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class TagService implements ITagService {
	private final Logger logger = Logger.getLogger(TagService.class);

	@Autowired
	private ITagDAO tagDAO;


	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws ServiceException {
		try {
			return (tagDAO.getNewsByTag(tagId));
		} catch (DAOException e) {
			logger.error("DAOException in getNewsByTag method!", e);
			throw new ServiceException("DAOException in getNewsByTag method!", e);
		}

	}

	@Override
	public List<TagTO> getList() throws ServiceException {
		try {
			return (tagDAO.getList());
		} catch (DAOException e) {
			logger.error("DAOException in getList method!", e);
			throw new ServiceException("DAOException in getList method!", e);
		}
	}

	@Override
	public void update(List<TagTO> tags) throws ServiceException {
		try {
			for (TagTO tag : tags) {
				tagDAO.update(tag);
			}
		} catch (DAOException e) {
			logger.error("DAOException in update method!", e);
			throw new ServiceException("DAOException in update method!", e);
		}
	}

	@Override
	public Long create(TagTO tag) throws ServiceException {
		try {
			return tagDAO.create(tag);
		} catch (DAOException e) {
			logger.error("DAOException in createTag method!", e);
			throw new ServiceException("DAOException in createTag method!", e);
		}
	}

	@Override
	public List<TagTO> getTagsByIds(List<Long> ids) throws ServiceException {
		try {
			return (tagDAO.getTagsByIds(ids));
		} catch (DAOException e) {
			logger.error("DAOException in getList method!", e);
			throw new ServiceException("DAOException in getList method!", e);
		}
	}
	@Override
	public void delete(List<Long> tagIds) throws ServiceException {
		try {
			tagDAO.delete(tagIds);
		} catch (DAOException e) {
			logger.error("DAOException in delete method!", e);
			throw new ServiceException("DAOException in delete method!", e);
		}

	}
	@Override
	public NewsVO setTags(NewsVO newsVO) throws ServiceException {
		List<TagTO> tags = new ArrayList<TagTO>();
		if (!(newsVO.getTagIds()).isEmpty()) {
			tags = getTagsByIds(newsVO.getTagIds());
		}
		newsVO.setTagTOs(tags);
		return newsVO;
	}
	@Override
	public TagTO getById(Long element) throws ServiceException {
		try {
			return tagDAO.getById(element);
		} catch (DAOException e) {
			logger.error("DAOException in getMapTags method!", e);
			throw new ServiceException("DAOException in getMapTags method!", e);
		}
	}
	
}