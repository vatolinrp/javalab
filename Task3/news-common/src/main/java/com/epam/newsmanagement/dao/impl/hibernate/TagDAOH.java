package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.util.HibernateUtil;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;

@Repository
public class TagDAOH implements ITagDAO{
	
	private SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	
	private final String SQL_GET_LIST_NEWS_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
			+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE"
			+ " FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
			+ "WHERE TAG_ID=:tag_id";
	private final String SQL_GET_TAGS_BY_IDS = "SELECT TAG_ID,TAG_NAME"
			+ " FROM TAG WHERE TAG_ID IN #";
	@Override
	@Transactional
	public Long create(TagTO element) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			Long id = (Long)session.save(element);
			tx.commit();
			return id;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in create method!",e);
		}
	}

	@Override
	@Transactional
	public void update(TagTO element) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			session.update(element);
			tx.commit();
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in update method!",e);
		}
		
	}

	@Override
	@Transactional
	public void delete(List<Long> elements) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			for(Long id:elements){
				TagTO tag = (TagTO) session.get(TagTO.class, id);
				session.delete(tag);
				tx.commit();
			}
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in delete method!",e);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<TagTO> getList() throws DAOException {
		Transaction tx = null;
		Session session = this.sessionFactory.openSession();
		tx = session.beginTransaction();
		try{
			Criteria cr = session.createCriteria(TagTO.class);
			List<TagTO> list =  cr.list();
			tx.commit();
			return list;
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in getList method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public TagTO getById(Long id) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			Criteria cr = session.createCriteria(TagTO.class);
			cr.add(Restrictions.eq("tagId", id));
			List<TagTO> list =  cr.list();
			tx.commit();
			if(list.size()!=0){
				return list.get(0);
			}
			else{
				return null;
			}
		}
		catch(HibernateException e){
			throw new DAOException("HibernateException in delete method!",e);
		}
		
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<NewsTO> getNewsByTag(Long tagId) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			Query query = session.createSQLQuery(SQL_GET_LIST_NEWS_BY_TAG);
			query.setLong("tag_id",tagId);
			List<NewsTO> list = query.list();
			tx.commit();
			return list;
		}
        catch(HibernateException e){
			throw new DAOException("HibernateException in getNewsByTag method!",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<TagTO> getTagsByIds(List<Long> tagIds) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.getCurrentSession();
		tx = session.beginTransaction();
		try{
			String sql = SQLBuilder.insertIds(tagIds, SQL_GET_TAGS_BY_IDS);
			List<TagTO> tagList = session.createSQLQuery(sql).addEntity(TagTO.class).list();
			tx.commit();
			return tagList;
		}
        catch(HibernateException e){
			throw new DAOException("HibernateException in getAuthorsByNewsIdList method!",e);
		}
	}

}
