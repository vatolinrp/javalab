function confirmation(lang) {
	var data = $("input[name=ForDelete]"),
		 not_checked = 0; 
	for(i=0;i<data.length;i++){ 
		if(!data[i].checked){ 
		 not_checked++; 
		} 
	} 
	if(not_checked==data.length){
		if (lang == "ru") {
			alert("Ничего не выбрано!");
		}
		if (lang == "en") {
			alert("Nothing was selected!");
		}
		return false;
	}
	if (lang == "ru") {
		var answer = confirm("Вы действительно хотите удалить выбранные новости?");
	}
	if (lang == "en") {
		var answer = confirm("Delete selected news?");
	}
	if (answer){
		return true;
	}
	else{
		return false;
	}
}
function confirmComment(string){
	var answer = confirm(string);
	if (answer){
		return true;
	}
	else{
		return false;
	}
}