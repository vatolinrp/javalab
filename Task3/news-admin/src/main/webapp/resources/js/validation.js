function validateForm(lang) {
	var x = document.forms["newsVO"]["newsTO.title"].value;
	var validation = true;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 30)) {
		if (lang == "en") {
			document.getElementById("title-error").innerHTML="Title is not valid! (From 2 to 30 letters)";
		}
		if (lang == "ru") {
			document.getElementById("title-error").innerHTML="Название новости неподходящее! (Необходимо от 2 до 30 символов)";
		}
		validation = false;
	}
	x = document.forms["newsVO"]["date"].value;
	if (lang == "ru") {
		if (!x.match(/^(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d$/)) {
			document.getElementById("date-error").innerHTML="Неподходящая дата! Нужно dd/MM/yyyy";
			validation = false;
		}
	}
	if (lang == "en") {
		if (!x.match(/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$/)) {
			document.getElementById("date-error").innerHTML="Invalid date! Must be MM/dd/yyyy";
			validation = false;
		}
	}
	
	x = document.forms["newsVO"]["newsTO.shortText"].value;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 100)) {
		if (lang == "en") {
			document.getElementById("brief-error").innerHTML = "Short description is invalid! (From 2 to 100 letters)";
		}
		if (lang == "ru") {
			document.getElementById("brief-error").innerHTML="Неподходящее краткое описание! (Необходимо от 2 до 100 символов)";
		}
		validation = false;
	}
	x = document.forms["newsVO"]["newsTO.fullText"].value;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 2000)) {

		if (lang == "en") {
			document.getElementById("content-error").innerHTML="Full description is invalid! (From 2 to 2000 letters)";
			
		}
		if (lang == "ru") {
			document.getElementById("content-error").innerHTML="Неподходящее полное описание! (Необходимо от 2 до 2000 символов)";
		}
		validation = false;
	}
	x = document.forms["newsVO"]["author"].value;
	if (x == "0") {
		if (lang == "en") {
			document.getElementById("author-error").innerHTML="Please choose author!";
		}
		if (lang == "ru") {
			document.getElementById("author-error").innerHTML="Выберите автора!";
		}
		validation = false;
	}
	if(validation==false){
		return false;
	}
}


function validateCommentForm(lang) {
	var x = document.forms["commentTO"]["commentText"].value;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 100)) {
		if (lang == "en") {
			document.getElementById("comment-error").innerHTML="Comment is not valid! (From 2 to 30 letters)";
		}
		if (lang == "ru") {
			document.getElementById("comment-error").innerHTML="Коммент неподходящий (Необходимо от 2 до 100 символов)";
		}
		return false;
	}
}
function validateNewTagForm(lang) {
	var x = document.forms["newTagTO"]["tagName"].value;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 30)) {
		if (lang == "en") {
			document.getElementById("new-tag-error").innerHTML="Tag is not valid! (From 2 to 30 letters)";
		}
		if (lang == "ru") {
			document.getElementById("new-tag-error").innerHTML="Тег неподходящий (Необходимо от 2 до 30 символов)";
		}
		return false;
	}
}

comment-error


function validateNewAuthorForm(lang) {
	var x = document.forms["newAuthorTO"]["name"].value;
	if (x == null || x == "" || ((x.length) < 2) || ((x.length) > 30)) {
		if (lang == "en") {
			document.getElementById("new-author-error").innerHTML="Author is not valid! (From 2 to 30 letters)";
		}
		if (lang == "ru") {
			document.getElementById("new-author-error").innerHTML="Автор неподходящий (Необходимо от 2 до 30 символов)";
		}
		return false;
	}
}

