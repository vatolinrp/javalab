<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div id="news-add-edit-wrapper">
				<form:form method="post" action="/news-admin/news-add-edit" onsubmit="return validateForm('${pageContext.response.locale.language}')" commandName="newsVO">
					<div id="editNews">
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.title" />
							</div>
							<div class="content-component">
								<form:input path="newsTO.title" id="title-input" />
							</div>
							<div class="content-component">
								<div class="error-message" id="title-error" ></div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.date" />
							</div>
							<div class="content-component">
								<fmt:message key="date.format" var="format" />
								<fmt:formatDate value="${newsVO.currentDate}" var="dateString" pattern="${format}" />
								<form:input id="date-input" type="text" path="date" value="${dateString}" />
							</div>
							<div class="content-component">
								<div class="error-message" id="date-error" ></div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.brief" />
							</div>
							<div class="content-component">
								<form:textarea id="brief-input" rows="5" cols="50" path="newsTO.shortText" />
							</div>
							<div class="content-component">
								<div class="error-message" id="brief-error"></div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.content" />
							</div>
							<div class="content-component">
								<form:textarea id="content-input" rows="10" cols="50"
									path="newsTO.fullText" />
							</div>
							<div class="content-component">
								<div class="error-message" id="content-error"></div>
							</div>
						</div>
					</div>
					<div id="news-add-edit">
						<div id="select-option">
							<div id="author-select">
								<form:select path="author" >
								<c:if test="${not edit}">
									<fmt:message key="default.author" var="author" />
									<form:option  value="null" label="${author}" />
								</c:if>
									<form:options items="${authors}" itemLabel="name" itemValue="authorId"/>
								</form:select>
								<div class="error-message" id="author-error"></div>
								
							</div>
							<div class="multiselect">
								<div class="selectBox"><spring:message code="tag.select" /></div>
								<div id="checkboxes" style="display: none;" >
									<form:checkboxes element="div" items="${tags}" itemValue="tagId" itemLabel="tagName" path="tags" />
								</div>
							</div>
						<script src="<c:url value="/resources/js/checkbox.js" />"></script>
						</div>
						<form:hidden path="newsTO.newsId" value="${newsTO.newsId}"/>
						<input type="submit" id="save-btn" value="<spring:message code="btn.save" />">
					</div>
				</form:form>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>