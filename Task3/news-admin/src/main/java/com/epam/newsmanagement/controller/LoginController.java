package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
public class LoginController{
	/**
	 * Displays messages above login form
	 * @param authfailed - comes from security
	 * @param logout comes from security
	 * @return new ModelAndView
	 */
	@RequestMapping(value={"/login","/"})
	public ModelAndView getLoginForm(
			@RequestParam(required = false) String authfailed, String logout) {
		return new ModelAndView("login");
	} 
}

