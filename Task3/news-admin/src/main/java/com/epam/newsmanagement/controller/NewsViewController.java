package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.util.ServiceUtil;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
@SessionAttributes({"filterVO"})
public class NewsViewController {
	private final Logger logger = Logger.getLogger(NewsViewController.class);
	@Autowired
	private INewsManagementService newsManServ;
	/**
	 * Displays news by id
	 * @param newsId
	 *            the id of the news
	 * @param model
	 *            map of objects
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-view/{newsId}", method = RequestMethod.GET)
	public String displayNews(@PathVariable String newsId,
			Map<String, Object> model,@ModelAttribute FilterVO filterVO) throws ControllerException {
		try {
			Long newsIdL = Long.valueOf(newsId);
			model.put("commentTOList", newsManServ.getCommentsByNewsId(newsIdL));
			model.put("authorTO", newsManServ.getAuthorByNewsId(newsIdL));
			model.put("commentTO", new CommentTO());
			model.put("newsTO", newsManServ.getNewsById(newsIdL));
			if (!ServiceUtil.isFilterNull(filterVO)){
				model.put("nextExists", newsManServ.isNextNewsExist(filterVO,newsIdL));
				model.put("prevExists", newsManServ.isPrevNewsExist(filterVO,newsIdL));
			}
			else{
				model.put("nextExists", newsManServ.isNextNewsExist(newsIdL));
				model.put("prevExists", newsManServ.isPrevNewsExist(newsIdL));
			}
			return "news-view";
		} catch (ServiceException e) {
			logger.error("service exception in displayList method", e);
			throw new ControllerException(
					"service error in displayList method!", e);
		}
	}
	/**
	 * Shows previous news in the DB
	 * @param newsId - the id of the current news
	 * @param model map of objects
	 * @return String - destination view
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-previous/{newsId}", method = RequestMethod.GET)
	public String displayPreviousNews(@PathVariable String newsId,
			Map<String, Object> model,@ModelAttribute FilterVO filterVO) throws ControllerException {
		try {
			if (!ServiceUtil.isFilterNull(filterVO)){
				return "forward:"+"/news-view/"+(newsManServ.getOneFilteredNews(filterVO,(Long.valueOf(newsId)),false).getNewsId());
			}
			return "forward:"+"/news-view/"+(newsManServ.getPrevNews(Long.valueOf(newsId))).getNewsId();
		} catch (ServiceException e) {
			logger.error("service exception in displayList method", e);
			throw new ControllerException(
					"service error in displayList method!", e);
		}
	}
	/**
	 * Shows next news in the DB
	 * @param newsId - the id of the current news
	 * @param model map of objects
	 * @return String - destination view
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-next/{newsId}", method = RequestMethod.GET)
	public String displayNextNews(@PathVariable String newsId,
			Map<String, Object> model,@ModelAttribute FilterVO filterVO) throws ControllerException {
		try {
			if (!ServiceUtil.isFilterNull(filterVO)){
				return "forward:"+"/news-view/"+(newsManServ.getOneFilteredNews(filterVO,(Long.valueOf(newsId)),true).getNewsId());
			}
			return "forward:"+"/news-view/"+(newsManServ.getNextNews(Long.valueOf(newsId))).getNewsId();
		} catch (ServiceException e) {
			logger.error("service exception in displayList method", e);
			throw new ControllerException(
					"service error in displayList method!", e);
		}
	}

	/**
	 * Deletes comment
	 * 
	 * @param newsId
	 *            - the id of the news
	 * @param commentVO
	 *            object for form
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/delete-comment/{commentId}/{newsId}", method = RequestMethod.GET)
	public String deleteComment(@PathVariable String commentId,
			@PathVariable String newsId) throws ControllerException {
		try {
			List<Long> idsForDelete = new ArrayList<Long>();
			idsForDelete.add(Long.valueOf(commentId));
			newsManServ.deleteComment(idsForDelete);
			return "redirect:" + "/news-view/" + Long.valueOf(newsId);
		} catch (ServiceException e) {
			logger.error("service exception in deleteComment method", e);
			throw new ControllerException(
					"service error in deleteComment method!", e);
		}
		
	}

	/**
	 * Adds comment to news
	 * 
	 * @param newsId
	 *            - the id of the news
	 * @param commentVO
	 *            object for form
	 * @return String - destination view
	 * @throws ControllerException
	 *             if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/comment-add", method = RequestMethod.POST)
	public String addComment(@ModelAttribute("commentTO") CommentTO commentTO)
			throws ControllerException {
		try {
			commentTO.setCreationDate(new Date());
			newsManServ.createComment(commentTO);
			return "redirect:" + "/news-view/" + commentTO.getNewsId();
		} catch (ServiceException e) {
			logger.error("service exception in addComment method", e);
			throw new ControllerException(
					"service error in addComment method!", e);
		}
	}
}
