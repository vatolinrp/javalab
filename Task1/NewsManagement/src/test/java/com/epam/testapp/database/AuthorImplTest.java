package com.epam.testapp.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.testapp.database.dao.IAuthorDAO;
import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.NewsTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class AuthorImplTest {
	private static IDataSet authorsDataSet;
	private static IDataSet newsDataSet;
	private static IDataSet newsAuthorsDataSet;
	@Autowired
	private IAuthorDAO authorDAO;
	
	@BeforeClass
	public static void beforeClass() {
		Locale.setDefault(Locale.ENGLISH);
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		authorsDataSet = loader.load("/authors.xml");
		newsDataSet = loader.load("/news.xml");
		newsAuthorsDataSet = loader.load("/news-authors.xml");
	}
	@Before
	public void setUp() throws Exception {
		try (Connection connect = authorDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, authorsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsAuthorsDataSet);
			
		}
	}
	@After
	public void tearDown() throws Exception {
		try (Connection connect = authorDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsAuthorsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, authorsDataSet);
		}
	}
	@Test
	public void getListTest() throws Exception {
		List<AuthorTO> authorList = authorDAO.getList();
		assertNotNull(authorList);
		assertEquals(authorList.size(), 3);
	}
	
	@Test
	public void getListByTest() throws Exception {
		List<NewsTO> newsList = authorDAO.getNewsListByAuthor(1L);
		assertEquals(newsList.size(), 3);
		assertNotNull(newsList);
	}
	
	@Test
	public void createTest() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setName("testName4");
		Long generatedId=authorDAO.create(author);
		assertNotNull(generatedId);
		author=authorDAO.getById(generatedId);
		assertNotNull(author);
		assertEquals(author.getName(), "testName4");
		
	}
	
	@Test
	public void deleteTest() throws Exception {
		AuthorTO author = new AuthorTO();
		List<Long> listAuthorIds = new ArrayList<Long>();
		listAuthorIds.add(3L);
		authorDAO.delete(listAuthorIds);
		author=authorDAO.getById(3L);
		assertEquals(author, null);
	}
	
	@Test
	public void updateTest() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setName("testNameX");
		author.setAuthorId(1L);
		authorDAO.update(author);
		author=authorDAO.getById(1L);
		assertNotNull(author);
		assertEquals(author.getName(),"testNameX");
	}
	
	@Test
	public void getByIdTest() throws Exception {
		AuthorTO author = new AuthorTO();
		author=authorDAO.getById(1L);
		assertNotNull(author);
		assertEquals(author.getName(),"testName1");
	}
	
	@Test
	public void attachAuthorTest() throws Exception {
		authorDAO.attachAuthor(2L,1L);
		List<NewsTO> newsList = authorDAO.getNewsListByAuthor(2L);
		assertEquals(newsList.size(), 1);
	}
}