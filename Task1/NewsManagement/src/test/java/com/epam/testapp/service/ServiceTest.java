package com.epam.testapp.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.testapp.database.dao.IAuthorDAO;
import com.epam.testapp.database.dao.ICommentDAO;
import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.database.dao.ITagDAO;
import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.CommentTO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.exceptions.ServiceException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class ServiceTest {
	private final Logger logger = Logger.getLogger(ServiceImp.class);
	@InjectMocks
	private ServiceImp serviceImpl;
	@Mock
	private IAuthorDAO authorDAO;
	@Mock
	private INewsDAO newsDAO;
	@Mock
	private ITagDAO tagDAO;
	@Mock
	private ICommentDAO commentDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getNewsTest() {
		try {
			serviceImpl.getListOfNews();
			Mockito.verify(newsDAO).getList();
		} catch (DAOException e) {
			logger.error("DAOException exception in getNewsTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in getNewsTest");
		}
	}

	@Test
	public void createNewsTest() {
		try {
			NewsTO news = new NewsTO();
			news.setTitle("title_4");
			news.setShortText("s_text4");
			news.setFullText("f_test4");
			GregorianCalendar cal = new GregorianCalendar(2015,
					Calendar.DECEMBER, 31);
			news.setCreationDate(new Date(cal.getTime().getTime()));
			news.setModificationDate(new Date(cal.getTime().getTime()));
			serviceImpl.createNews(news);
			Mockito.verify(newsDAO).create(news);
		} catch (DAOException e) {
			logger.error("DAOException exception in createNewsTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in createNewsTest");
		}
	}
	@Test
	public void createCommentTest() {
		try {
			CommentTO comment = new CommentTO();
			comment.setCommentText("testName4");
			GregorianCalendar cal = new GregorianCalendar(2015,Calendar.DECEMBER, 31);
			comment.setCreationDate(new Date(cal.getTime().getTime()));
			serviceImpl.createComment(comment);
			Mockito.verify(commentDAO).create(comment);
		} catch (DAOException e) {
			logger.error("DAOException exception in createCommentTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in createCommentTest");
		}
	}

	@Test
	public void updateNewsTest() {
		try {
			NewsTO news = new NewsTO();
			news.setTitle("title_4");
			news.setShortText("s_text4");
			news.setFullText("f_test4");
			GregorianCalendar cal = new GregorianCalendar(2015,
					Calendar.DECEMBER, 31);
			news.setCreationDate(new Date(cal.getTime().getTime()));
			news.setModificationDate(new Date(cal.getTime().getTime()));
			serviceImpl.updateNews(news);
			Mockito.verify(newsDAO).update(news);
		} catch (DAOException e) {
			logger.error("DAOException exception in updateNewsTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in updateNewsTest");
		}
	}

	@Test
	public void deleteNewsTest() {
		try {
			List<Long> ids = new ArrayList<Long>();
			ids.add(1L);
			ids.add(2L);
			serviceImpl.deleteNews(ids);
			Mockito.verify(newsDAO).delete(ids);
		} catch (DAOException e) {
			logger.error("DAOException exception in deleteNewsTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in deleteNewsTest");
		}
	}
	@Test
	public void deleteCommentTest() {
		try {
			List<Long> ids = new ArrayList<Long>();
			ids.add(1L);
			ids.add(2L);
			serviceImpl.deleteComments(ids);
			Mockito.verify(commentDAO).delete(ids);
		} catch (DAOException e) {
			logger.error("DAOException exception in deleteCommentTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in deleteCommentTest");
		}
	}
	
	@Test
	public void viewNewsTest() {
		try {
			serviceImpl.viewNews(1L);
			Mockito.verify(newsDAO).getById(1L);
		} catch (DAOException e) {
			logger.error("DAOException exception in viewNewsTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in viewNewsTest");
		}
	}

	@Test
	public void createAuthorTest() {
		try {
			AuthorTO author = new AuthorTO();
			author.setName("testName4");
			serviceImpl.createAuthor(author);
			Mockito.verify(authorDAO).create(author);
		} catch (DAOException e) {
			logger.error("DAOException exception in createAuthorTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in createAuthorTest");
		}
	}

	@Test
	public void getNewsByAuthorTest() {
		try {
			
			serviceImpl.getNewsByAuthor(1L);
			Mockito.verify(authorDAO).getNewsListByAuthor(1L);
		} catch (DAOException e) {
			logger.error("DAOException exception in getNewsByAuthorTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in getNewsByAuthorTest");
		}
	}

	@Test
	public void attachTagsTest() {
		try {
			List<Long> tags = new ArrayList<Long>();
			tags.add(2L);
			serviceImpl.attachTags(tags, 1L);
			Mockito.verify(tagDAO).attachTags(tags, 1L);
		} catch (ServiceException e) {
			logger.error("DAOException exception in attachTagsTest");
		} catch (DAOException e) {
			logger.error("ServiceException exception in attachTagsTest");
		}
	}
	
	@Test
	public void getNewsByTagTest() {
		try {
			serviceImpl.getNewsByTag(1L);
			Mockito.verify(tagDAO).getNewsByTag(1L);
		} catch (DAOException e) {
			logger.error("DAOException exception in getNewsByTagTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in getNewsByTagTest");
		}
	}
	@Test
	public void complexCreateTest() {
		try {
			NewsTO news = new NewsTO();
			news.setTitle("title_4");
			news.setShortText("s_text4");
			news.setFullText("f_test4");
			GregorianCalendar cal = new GregorianCalendar(2015,Calendar.DECEMBER, 31);
			news.setCreationDate(new Date(cal.getTime().getTime()));
			news.setModificationDate(new Date(cal.getTime().getTime()));
			TagTO tag = new TagTO();
			tag.setTagName("testName4");
			AuthorTO author = new AuthorTO();
			author.setName("testName4");
			List<TagTO> tags = new ArrayList<TagTO>();
			tags.add(tag);
			serviceImpl.complexCreate(news, author, tags);
			Mockito.verify(newsDAO).create(news);
			Mockito.verify(tagDAO).create(tag);
			Mockito.verify(authorDAO).create(author);
		} catch (DAOException e) {
			logger.error("DAOException exception in complexCreateTest");
		} catch (ServiceException e) {
			logger.error("ServiceException exception in complexCreateTest");
		}
	}
	

}
