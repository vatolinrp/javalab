package com.epam.testapp.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.entity.NewsTO;
/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class NewsImplTest {
	private static IDataSet newsDataSet;
	private static IDataSet commentsDataSet;
	@Autowired
	private INewsDAO newsDAO;
	@BeforeClass
	public static void beforeClass() {
		Locale.setDefault(Locale.ENGLISH);
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		newsDataSet = loader.load("/news.xml");
		commentsDataSet = loader.load("/comments.xml");
	}
	@Before
	public void setUp() throws Exception {
		try (Connection connect = newsDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, commentsDataSet);
		}
	}
	@After
	public void tearDown() throws Exception {
		try (Connection connect = newsDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, commentsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsDataSet);
		}
	}
	@Test
	public void getListTest() throws Exception {
		List<NewsTO> newsList = newsDAO.getList();
		assertEquals(3, newsList.size());
	}
	@Test
	public void getNewsByIdsTest() throws Exception {
		List<Long> newsIdList = new ArrayList<Long>();
		newsIdList.add(1L);
		newsIdList.add(2L);
		List<NewsTO> newsList = newsDAO.getNewsByIds(newsIdList);
		assertNotNull(newsList);
		assertEquals(2, newsList.size());
	}
	@Test
	public void createTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_test4");
		GregorianCalendar cal = new GregorianCalendar(2015,Calendar.DECEMBER, 31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		Long generatedId = newsDAO.create(news);
		assertNotNull(generatedId);
		news=newsDAO.getById(generatedId);
		assertNotNull(news);
	}
	
	@Test
	public void updateTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_text4");
		news.setNewsId(1L);
		GregorianCalendar cal = new GregorianCalendar(2015,Calendar.DECEMBER, 31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		newsDAO.update(news);
		news=newsDAO.getById(1L);
		assertNotNull(news);
		assertEquals(news.getTitle(),"title_4");
	}
	@Test
	public void getByIdTest() throws Exception {
		NewsTO newsId1= newsDAO.getById(1L);
		assertNotNull(newsId1);
		assertEquals(newsId1.getShortText(), "s_text1");
	}
	@Test
	public void deleteTest() throws Exception {
		List<Long> listTags = new ArrayList<Long>();
		listTags.add(3L);
		newsDAO.delete(listTags);
		NewsTO news;
		news=newsDAO.getById(3L);
		assertEquals(news,null);
	}
}