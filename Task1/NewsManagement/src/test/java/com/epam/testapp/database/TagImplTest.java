package com.epam.testapp.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.testapp.database.dao.ITagDAO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;
/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class TagImplTest {
	private static IDataSet tagsDataSet;
	private static IDataSet newsDataSet;
	private static IDataSet newsTagsDataSet;
	
	@Autowired
	private ITagDAO tagDAO;
	
	@BeforeClass
	public static void beforeClass() {
		Locale.setDefault(Locale.ENGLISH);
		FullXmlDataFileLoader loader = new FullXmlDataFileLoader();
		tagsDataSet = loader.load("/tags.xml");
		newsDataSet = loader.load("/news.xml");
		newsTagsDataSet = loader.load("/news-tags.xml");
	}
	@Before
	public void setUp() throws Exception {
		try (Connection connect = tagDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, tagsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsDataSet);
			DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnect, newsTagsDataSet);
		}
		
	}
	@After
	public void tearDown() throws Exception {
		try (Connection connect = tagDAO.getDataSource().getConnection()) {
			IDatabaseConnection dbUnitConnect = new DatabaseConnection(connect);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsTagsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, tagsDataSet);
			DatabaseOperation.DELETE_ALL.execute(dbUnitConnect, newsDataSet);
		}
		
	}
	@Test
	public void getByIdTest() throws Exception {
		TagTO tagId1 =tagDAO.getById(1L);
		assertNotNull(tagId1);
		assertEquals(tagId1.getTagName(), "testName1");
	}
	@Test
	public void getListTest() throws Exception {
		List<TagTO> newsList = tagDAO.getList();
		assertEquals(3, newsList.size());
	}
	@Test
	public void createTest() throws Exception {
		TagTO tag = new TagTO();
		tag.setTagName("testName4");
		Long generatedId = tagDAO.create(tag);
		assertNotNull(generatedId);
		tag=tagDAO.getById(generatedId);
		assertNotNull(tag);
	}
	@Test
	public void deleteTest() throws Exception {
		List<Long> listTags = new ArrayList<Long>();
		listTags.add(3L);
		tagDAO.delete(listTags);
		TagTO tag;
		tag=tagDAO.getById(3L);
		assertEquals(tag,null);
	}
	@Test
	public void updateTest() throws Exception {
		TagTO tag = new TagTO();
		tag.setTagName("testNameX");
		tag.setTagId(1L);
		tagDAO.update(tag);
		tag=tagDAO.getById(1L);
		assertNotNull(tag);
		assertEquals(tag.getTagName(),"testNameX");
	}
	@Test
	public void getNewsByTagTest() throws Exception {
		List<NewsTO> news = tagDAO.getNewsByTag(1L);
		assertNotNull(news);
		assertEquals(news.size(),2);
	}
	@Test
	public void attachTagsTest() throws Exception {
		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tagDAO.attachTags(tags, 1L);
		List<NewsTO> newsList = tagDAO.getNewsByTag(2L);
		assertEquals(newsList.size(), 2);
	}

}