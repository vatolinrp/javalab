package com.epam.testapp.database.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class NewsDAO implements INewsDAO {
	// SQL
	private final String SQL_DELETE = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private final String SQL_DELETE_NA = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private final String SQL_DELETE_NT = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private final String SQL_DELETE_COMMENTS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private final String SQL_GET_NEWS_BY_IDS = "SELECT NEWS_ID,SHORT_TEXT,FULL_TEXT,"
			+ " TITLE,CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID IN (";
	private final String SQL_UPDATE_NEWS = "UPDATE NEWS SET SHORT_TEXT=?, FULL_TEXT=?,"
			+ " TITLE=?, CREATION_DATE=?, MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private final String SQL_GET_NEWS_WITH_COM = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE"
			+ " FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID";
	private final String SQL_CREATE_NEWS = "INSERT INTO NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,"
			+ "CREATION_DATE,MODIFICATION_DATE) values (NEWS_SEQ.nextVal,?,?,?,?,?) ";
	private final String SQL_FIND_NEWS = "SELECT NEWS_ID,SHORT_TEXT,FULL_TEXT, TITLE,"
			+ "CREATION_DATE, MODIFICATION_DATE  FROM NEWS WHERE NEWS_ID=?";
	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(NewsTO element) throws DAOException {
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_CREATE_NEWS,
					new String[] { "NEWS_ID" });
			statement.setString(1, element.getShortText());
			statement.setString(2, element.getFullText());
			statement.setString(3, element.getTitle());
			statement.setTimestamp(4, new Timestamp(element.getCreationDate()
					.getTime()));
			statement.setDate(5, new java.sql.Date(element
					.getModificationDate().getTime()));
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating a news!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating a news!", e);
		} finally {
			close(statement, resultSet, con);
		}
	}

	@Override
	public NewsTO getById(Long newsId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		NewsTO news = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_FIND_NEWS);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news = buildNewsTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return news;
	}

	@Override
	public void update(NewsTO element) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_UPDATE_NEWS);
			statement.setString(1, element.getShortText());
			statement.setString(2, element.getFullText());
			statement.setString(3, element.getTitle());
			statement.setTimestamp(4, new Timestamp(element.getCreationDate()
					.getTime()));
			statement.setDate(5, new java.sql.Date(element
					.getModificationDate().getTime()));
			statement.setLong(6, element.getNewsId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			close(statement, con);
		}
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;
		PreparedStatement statement4 = null;
		try {
			con = myDataSource.getConnection();
			con.setAutoCommit(false);
			statement1 = con.prepareStatement(SQL_DELETE_NA);
			for (Long id : ids) {
				statement1.setLong(1, id);
				statement1.addBatch();
			}
			statement1.executeBatch();
			
			statement2 = con.prepareStatement(SQL_DELETE_NT);
			for (Long id : ids) {
				statement2.setLong(1, id);
				statement2.addBatch();
			}
			statement2.executeBatch();
			
			statement3 = con.prepareStatement(SQL_DELETE_COMMENTS);
			for (Long id : ids) {
				statement3.setLong(1, id);
				statement3.addBatch();
			}
			statement3.executeBatch();
			
			statement4 = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement4.setLong(1, id);
				statement4.addBatch();
			}
			statement4.executeBatch();
			con.commit();
		} catch (SQLException e) {
			try {
				if(con!=null)
					con.rollback();
	        } catch (SQLException sqx) {
	        	throw new DAOException("sql error in delete method!", e); 
	        }
		} finally {
			close(statement1, con);
			close(statement2, con);
			close(statement3, con);
			close(statement4, con);
		}

	}

	@Override
	public List<NewsTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<NewsTO> list=null;
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		Map<NewsTO, Integer> counter = new HashMap<NewsTO, Integer>();
		NewsTO news = null;
		try {
			con = myDataSource.getConnection();
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_NEWS_WITH_COM);
			while (resultSet.next()) {
				news = buildNewsTO(resultSet);
				newsList.add(news);

			}
			for (NewsTO news1 : newsList)
				counter.put(news1,1 + (counter.containsKey(news1) ? counter.get(news1): 0));
			list = new ArrayList<NewsTO>(counter.keySet());
			Collections.sort(list, new Comparator<NewsTO>() {
				@Override
				public int compare(NewsTO x, NewsTO y) {
					return counter.get(y) - counter.get(x);
				}
			});

		} catch (SQLException e) {
			throw new DAOException("sql exception in getList method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return list;
	}

	public static NewsTO buildNewsTO(ResultSet resultSet) throws SQLException {
		NewsTO news = new NewsTO();
		news.setNewsId(resultSet.getLong("NEWS_ID"));
		news.setTitle(resultSet.getString("TITLE"));
		news.setShortText(resultSet.getString("SHORT_TEXT"));
		news.setFullText(resultSet.getString("FULL_TEXT"));
		Date date1 = new Date(resultSet.getTimestamp("CREATION_DATE").getTime());
		news.setCreationDate(date1);
		Date date2 = resultSet.getDate("MODIFICATION_DATE");
		news.setModificationDate(date2);
		return news;
	}

	@Override
	public List<NewsTO> getNewsByIds(List<Long> listOfNewsId)
			throws DAOException {
		List<NewsTO> newslist = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		StringBuilder sb = new StringBuilder();
		sb.append(SQL_GET_NEWS_BY_IDS);
		for (Long newsId : listOfNewsId) {
			sb.append(newsId).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		try {
			con = myDataSource.getConnection();
			System.out.println(sb.toString());
			statement = con.prepareStatement(sb.toString());
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = buildNewsTO(resultSet);
				newslist.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsByIds method", e);
		} finally {
			close(statement, resultSet, con);
		}
		return newslist;
	}

	@Override
	public DataSource getDataSource() {
		return myDataSource;
	}

	/**
	 * Closes statement, resultSet and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param resultSet
	 *            - used resultSet
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, ResultSet resultSet, Connection con)
			throws DAOException {
		try {
			if (resultSet != null)
				resultSet.close();
			close(statement, con);
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes statement and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, Connection con) throws DAOException {
		try {
			if (statement != null)
				statement.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}
}
