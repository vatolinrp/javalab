package com.epam.testapp.service;

import java.util.List;

import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.CommentTO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;
import com.epam.testapp.exceptions.ServiceException;

public interface IService {
	/**
	 * Adds news to NEWS table in DB
	 * 
	 * @param news
	 *            - news instance to create
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createNews(NewsTO news) throws ServiceException;
	/**
	 * Saves news, author and tags in a transaction
	 * 
	 * @param news
	 *            - selected news for saving
	 * @param author
	 *            - selected author for saving
	 * @param tags
	 *            - selected list of tags for saving
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void complexCreate(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException;
	/**
	 * Updates news in NEWS table in DB
	 * 
	 * @param news
	 *            - selected news for update
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void updateNews(NewsTO news) throws ServiceException;
	/**
	 * Deletes a list of news in NEWS table in DB
	 * 
	 * @param newsIds
	 *            - selected list of Ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteNews(List<Long> newsIds) throws ServiceException;
	/**
	 * Gets all news from NEWS table in DB
	 * 
	 * @return List<NewsTO> - all news from DB
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getListOfNews() throws ServiceException;
	/**
	 * Gets news from DB by its id
	 * 
	 * @param newsId
	 *            - selected news Id to view
	 * @return NewsTO - news entity, which we get by id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsTO viewNews(Long newsId) throws ServiceException;
	/**
	 * Creates author in AUTHORS DB
	 * 
	 * @param author
	 *            - author instance to create
	 * @return Long - generated authors Id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createAuthor(AuthorTO author) throws ServiceException;
	/**
	 * Gets the list of news from NEWS table in DB by our custom author id
	 * 
	 * @param authorId
	 *            - the id of selected author
	 * @return List<NewsTO> - list of news, which belong to author
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;
	/**
	 * Adds tags to chosen news
	 * 
	 * @param tagIds
	 *            - list of tag ids
	 * @param newsId
	 *            - id of the chosen news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void attachTags(List<Long> tagIds, Long newsId)
			throws ServiceException;
	/**
	 * Gets the list of news from NEWS table in DB by our custom tag
	 * 
	 * @param tagId
	 *            - the id of the selected tag
	 * @return List<NewsTO> - list of news by our tag
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws ServiceException;
	/**
	 * Adds a comment to chosen news in table COMMENTS
	 * 
	 * @param comment
	 *            - comment instance to be created
	 * @return Long - generated comment's id
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Long createComment(CommentTO comment) throws ServiceException;
	/**
	 * Deletes comments from COMMENTS table in DB
	 * 
	 * @param commentIds
	 *            - list of news ids to be deleted
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteComments(List<Long> commentIds) throws ServiceException;
}
