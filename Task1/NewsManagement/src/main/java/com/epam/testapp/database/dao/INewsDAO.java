package com.epam.testapp.database.dao;

import java.util.List;

import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface INewsDAO extends ICommonDAO<NewsTO> {
	/**
	 * Gets list of news by selected ids
	 * 
	 * @param listOfNewsId
	 * @return List<News> - list of news, which has news with selected ids
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByIds(List<Long> listOfNewsId) throws DAOException;
}
