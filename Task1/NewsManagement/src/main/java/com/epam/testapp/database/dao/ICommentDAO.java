package com.epam.testapp.database.dao;

import com.epam.testapp.entity.CommentTO;


/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ICommentDAO extends ICommonDAO<CommentTO>{
	
}
