package com.epam.testapp.database.dao;

import java.util.List;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.NewsTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IAuthorDAO extends ICommonDAO<AuthorTO> {
	/**
	 * Attaches author to a news in NEWS_AUTHORS table
	 * 
	 * @param authorId
	 *            - selected author's ID
	 * @param newsId
	 *            - selected news' ID
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	void attachAuthor(Long authorId, Long newsId) throws DAOException;

	/**
	 * By author id we get news id in NEWS_AUTHORS and using inner join we get
	 * all news, which have our news id from NEWS_AUTHORS
	 * 
	 * @param authorId
	 *            - selected author's
	 * @return List<News> - list of news, which belongs to author
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException;
}
