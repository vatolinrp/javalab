package com.epam.testapp.database.dao;

import java.util.List;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ITagDAO extends ICommonDAO<TagTO> {
	/**
	 * Gets list of news by input tag ID
	 * 
	 * @param tagId
	 *            - selected ID
	 * @return List<News> - all news, which have selected tag
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws DAOException;

	/**
	 * Creates a line in DB NEWS_TAGS
	 * 
	 * @param tagIds
	 *            - list of selected tag IDs
	 * @param newsIds
	 *            - selected news ID, for which tag IDs will be attached
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */

	void attachTags(List<Long> tagIds, Long newsIds) throws DAOException;

}
