package com.epam.testapp.database.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.database.dao.ITagDAO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class TagDAO implements ITagDAO {
	// SQL
	private final String SQL_DELETE = "DELETE FROM TAG WHERE TAG_ID = ?";
	private final String SQL_DELETE_NT = "DELETE FROM NEWS_TAG WHERE TAG_ID=?";
	private final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME=? WHERE TAG_ID=?";
	private final String SQL_CREATE = "INSERT INTO TAG (TAG_ID,TAG_NAME) "
											+ "VALUES (TAGS_SEQ.nextVal,?) ";
	private final String SQL_CREATE_NEWS_TAGS = "INSERT INTO NEWS_TAG (NEWS_TAG_ID, TAG_ID, NEWS_ID) "
														+ "VALUES (NT_SEQ.nextVal,?,?)";
	private final String SQL_GET_ALL_TAGS = "SELECT TAG_NAME,TAG_ID FROM TAG";
	private final String SQL_FIND_TAG = "SELECT TAG_ID, TAG_NAME FROM TAG "
											+ "WHERE TAG_ID=?";
	private final String SQL_GET_LIST_NEWS_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
				+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE"
				+" FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
				+ "WHERE TAG_ID=?";

	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(TagTO element) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_CREATE,
					new String[] { "TAG_ID" });
			statement.setString(1, element.getTagName());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating a tag!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating a tag!", e);
		} finally {
			close(statement, resultSet, con);

		}
	}

	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws DAOException {
		List<NewsTO> listNews = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_GET_LIST_NEWS_BY_TAG);
			statement.setLong(1, tagId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = NewsDAO.buildNewsTO(resultSet);
				listNews.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsByTagId method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return listNews;
	}

	@Override
	public void update(TagTO element) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_UPDATE_TAG);
			statement.setString(1, element.getTagName());
			statement.setLong(2, element.getTagId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			close(statement, con);
		}
	}

	@Override
	public void attachTags(List<Long> tagIds, Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = myDataSource.getConnection();
			for (Long tagId : tagIds) {
				statement = con.prepareStatement(SQL_CREATE_NEWS_TAGS);
				statement.setLong(1, tagId);
				statement.setLong(2, newsId);
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in attachTags method!", e);
		} finally {
			close(statement, resultSet, con);
		}
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		try {
			con = myDataSource.getConnection();
			con.setAutoCommit(false);
			statement1 = con.prepareStatement(SQL_DELETE_NT);
			for (Long id : ids) {
				statement1.setLong(1, id);
				statement1.addBatch();
			}
			statement1.executeBatch();
			statement2 = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement2.setLong(1, id);
				statement2.addBatch();
			}
			statement2.executeBatch();
			con.commit();
		} catch (SQLException e) {
			try {
				if(con!=null)
					con.rollback();
	        } catch (SQLException sqx) {
	        	throw new DAOException("sql error in delete method!", e); 
	        }
		} finally {
			close(statement1, con);
			close(statement2, con);
		}
	}

	@Override
	public TagTO getById(Long tagId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_FIND_TAG);
			statement.setLong(1, tagId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				return  buildTagTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return null;
	}

	@Override
	public List<TagTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<TagTO> tags = new LinkedList<TagTO>();
		TagTO tag = null;
		try {
			con = myDataSource.getConnection();
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_TAGS);
			while (resultSet.next()) {
				tag = buildTagTO(resultSet);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getList method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return tags;
	}

	public static TagTO buildTagTO(ResultSet resultSet) throws SQLException {
		TagTO tag = new TagTO();
		tag.setTagId(resultSet.getLong("TAG_ID"));
		tag.setTagName(resultSet.getString("TAG_NAME"));
		return tag;
	}

	@Override
	public DataSource getDataSource() {
		return myDataSource;
	}

	/**
	 * Closes statement, resultSet and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param resultSet
	 *            - used resultSet
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, ResultSet resultSet, Connection con)
			throws DAOException {
		try {
			if (resultSet != null)
				resultSet.close();
			close(statement, con);
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes statement and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, Connection con) throws DAOException {
		try {
			if (statement != null)
				statement.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}
}
