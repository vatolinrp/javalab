package com.epam.testapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.testapp.database.dao.IAuthorDAO;
import com.epam.testapp.database.dao.ICommentDAO;
import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.database.dao.ITagDAO;
import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.CommentTO;
import com.epam.testapp.entity.NewsTO;
import com.epam.testapp.entity.TagTO;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.exceptions.ServiceException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class ServiceImp implements IService{

	private final Logger logger = Logger.getLogger(ServiceImp.class);
	@Autowired
	private IAuthorDAO authorDAO;
	@Autowired
	private INewsDAO newsDAO;
	@Autowired
	private ITagDAO tagDAO;
	@Autowired
	private ICommentDAO commentDAO;
	
	@Override
	public Long createNews(NewsTO news) throws ServiceException {

		try {
			if (news == null) {
				throw new ServiceException(
						"parameter news is null in createNews method!");
			}
			return newsDAO.create(news);
		} catch (DAOException e) {
			logger.error("sql error in createNews method!", e);
			throw new ServiceException("sql error in createNews method!", e);
		}
	}
	@Override
	@Transactional
	public void complexCreate(NewsTO news, AuthorTO author, List<TagTO> tags)
			throws ServiceException {
		try {
			List<Long> tagIds = new ArrayList<Long>();
			if (news != null) {
				newsDAO.create(news);
				if (author != null) {
					authorDAO.create(author);
					authorDAO.attachAuthor(author.getAuthorId(),
							news.getNewsId());
				}
				if (tags != null) {
					for (TagTO tag : tags) {
						tagDAO.create(tag);
						tagIds.add(tag.getTagId());
					}
					tagDAO.attachTags(tagIds, news.getNewsId());
				}
			} else {
				if (author != null) {
					authorDAO.create(author);
				}
				if (tags != null) {
					for (TagTO tag : tags) {
						tagDAO.create(tag);
					}
				}
			}
		} catch (DAOException e) {
			logger.error("sql error in complexSave method!", e);
			throw new ServiceException("sql error in complexSave method!", e);
		}
	}
	@Override
	public void updateNews(NewsTO news) throws ServiceException {
		try {
			if (news == null) {
				throw new ServiceException(
						"parameter news is null in updateNews method!");
			}
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("sql error in updateNews method!", e);
			throw new ServiceException("sql error in updateNews method!", e);
		}
	}
	@Override
	public void deleteNews(List<Long> newsIds) throws ServiceException {
		try {
			if (newsIds == null) {
				throw new ServiceException(
						"parameter newsIds is null in deleteNews method!");
			}
			newsDAO.delete(newsIds);
		} catch (DAOException e) {
			logger.error("sql error in deleteNews method!", e);
			throw new ServiceException("sql error in deleteNews method!", e);
		}
	}
	@Override
	public List<NewsTO> getListOfNews() throws ServiceException {
		List<NewsTO> list = null;
		try {
			list = newsDAO.getList();
		} catch (DAOException e) {
			logger.error("sql error in getListOfNews method!", e);
			throw new ServiceException("sql error in getListOfNews method!", e);
		}
		return list;
	}
	@Override
	public NewsTO viewNews(Long newsId) throws ServiceException {
		NewsTO listOfNews = null;
		try {
			if (newsId == null) {
				throw new ServiceException(
						"parameter newsIds is null in viewNews method!");
			}
			listOfNews = newsDAO.getById(newsId);
		} catch (DAOException e) {
			logger.error("sql error in viewNews method!", e);
			throw new ServiceException("sql error in viewNews method!", e);
		}
		return listOfNews;

	}
	@Override
	public Long createAuthor(AuthorTO author) throws ServiceException {
		try {
			if (author == null) {
				throw new ServiceException(
						"parameter author is null in createAuthor method!");
			}
			return authorDAO.create(author);
		} catch (DAOException e) {
			logger.error("sql error in createAuthor method!", e);
			throw new ServiceException("sql error in createAuthor method!", e);
		}
	}
	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			if (authorId == null) {
				throw new ServiceException(
						"parameter authorId is null in getNewsByAuthor method!");
			}
			return authorDAO.getNewsListByAuthor(authorId);
		} catch (DAOException e) {
			logger.error("sql error in getNewsByAuthor method!", e);
			throw new ServiceException("sql error in getNewsByAuthor method!",
					e);
		}
	}
	@Override
	public void attachTags(List<Long> tagIds, Long newsId)
			throws ServiceException {
		try {
			if ((tagIds == null) || (newsId == null)) {
				throw new ServiceException(
						"parameter authorId is null in attachTags method!");
			}
			tagDAO.attachTags(tagIds, newsId);
		} catch (DAOException e) {
			logger.error("sql error in attachTags method!", e);
			throw new ServiceException("sql error in attachTags method!", e);
		}
	}
	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws ServiceException {
		try {
			if ((tagId == null)) {
				throw new ServiceException(
						"parameter tagId is null in getNewsByTag method!");
			}
			return tagDAO.getNewsByTag(tagId);
		} catch (DAOException e) {
			logger.error("sql error in getNewsByTag method!", e);
			throw new ServiceException("sql error in getNewsByTag method!", e);
		}

	}
	@Override
	public Long createComment(CommentTO comment) throws ServiceException {
		try {
			if ((comment == null)) {
				throw new ServiceException(
						"parameter comment is null in saveComment method!");
			}
			return commentDAO.create(comment);
		} catch (DAOException e) {
			logger.error("sql error in saveComment method!", e);
			throw new ServiceException("sql error in saveComment method!", e);
		}

	}
	@Override
	public void deleteComments(List<Long> commentIds) throws ServiceException {
		try {
			if ((commentIds == null)) {
				throw new ServiceException(
						"parameter commentIds is null in deleteComment method!");
			}
			commentDAO.delete(commentIds);
		} catch (DAOException e) {
			logger.error("sql error in deleteComment method!", e);
			throw new ServiceException("sql error in deleteComment method!", e);
		}
	}
}
