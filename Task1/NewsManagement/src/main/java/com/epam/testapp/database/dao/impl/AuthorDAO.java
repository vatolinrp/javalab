package com.epam.testapp.database.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.database.dao.IAuthorDAO;
import com.epam.testapp.entity.AuthorTO;
import com.epam.testapp.entity.NewsTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */

@Repository
public class AuthorDAO implements IAuthorDAO {
	 // SQL
	private final String SQL_DELETE = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private final String SQL_DELETE_NA = "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID = ?";
	private final String SQL_FIND_AUTHOR = "SELECT AUTHOR_ID, NAME "
										+ "FROM AUTHOR where AUTHOR_ID=?";
	private final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID,NAME) "
											+ "values (AUTHORS_SEQ.nextVal,?) ";
	private final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, NAME FROM AUTHOR";
	private final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET NAME=? WHERE AUTHOR_ID=?";
	private final String SQL_GET_LIST_NEWS_BY_AUTHOR = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
													+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
													+"FROM NEWS INNER JOIN NEWS_AUTHOR "
													+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID "
													+ "WHERE AUTHOR_ID=?";
	private final String SQL_CREATE_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHOR "
													+ "(NEWS_AUTHOR_ID,NEWS_ID,AUTHOR_ID) "
													+ "VALUES (NA_SEQ.nextVal,?,?)";
	
	
	@Autowired
	private DataSource myDataSource;

	@Override
	public void update(AuthorTO author) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_UPDATE_AUTHOR);
			statement.setString(1, author.getName());
			statement.setLong(2, author.getAuthorId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			close(statement, con);
		}
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_CREATE_NEWS_AUTHORS);
			statement.setLong(1, newsId);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in attachAuthor method!", e);
		} finally {
			close(statement, resultSet, con);
		}
	}

	@Override
	public Long create(AuthorTO author) throws DAOException {
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_CREATE_AUTHOR,
					new String[] { "AUTHOR_ID" });
			statement.setString(1, author.getName());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException("sql error in create method, while creating an author!");
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in create method, while creating an author!", e);
		} finally {
			close(statement, resultSet, con);
		}
	}

	@Override
	public List<AuthorTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<AuthorTO> authors = new LinkedList<AuthorTO>();
		AuthorTO news = null;
		try {
			con = myDataSource.getConnection();
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_AUTHORS);
			while (resultSet.next()) {
				news = buildAuthorTO(resultSet);
				authors.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getList method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return authors;
	}

	@Override
	public AuthorTO getById(Long authorId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		AuthorTO author = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_FIND_AUTHOR);
			statement.setLong(1, authorId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				author = buildAuthorTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return author;
	}

	public static AuthorTO buildAuthorTO(ResultSet resultSet)
			throws SQLException {
		AuthorTO author = new AuthorTO();
		author.setName(resultSet.getString("NAME"));
		author.setAuthorId(resultSet.getLong("AUTHOR_ID"));
		return author;
	}

	@Override
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		List<NewsTO> listNews = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_GET_LIST_NEWS_BY_AUTHOR);
			statement.setLong(1, authorId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = NewsDAO.buildNewsTO(resultSet);
				listNews.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsListByAuthor method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return listNews;
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		try {
			con = myDataSource.getConnection();
			con.setAutoCommit(false);
			statement1 = con.prepareStatement(SQL_DELETE_NA);
			for (Long id : ids) {
				statement1.setLong(1, id);
				statement1.addBatch();
			}
			statement1.executeBatch();
			statement2 = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement2.setLong(1, id);
				statement2.addBatch();
			}
			statement2.executeBatch();
			con.commit();
		} catch (SQLException e) {
			try {
				if(con!=null)
					con.rollback();
	        } catch (SQLException sqx) {
	        	throw new DAOException("sql error in delete method!", e); 
	        }
		} finally {
			close(statement1, con);
			close(statement2, con);
		}

	}

	@Override
	public DataSource getDataSource() {
		return myDataSource;
	}

	/**
	 * Closes statement, resultSet and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param resultSet
	 *            - used resultSet
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, ResultSet resultSet, Connection con)
			throws DAOException {
		try {
			if (resultSet != null)
				resultSet.close();
			close(statement, con);
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes statement and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, Connection con) throws DAOException {
		try {
			if (statement != null)
				statement.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

}
