package com.epam.testapp.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Rostislav_Vatolin
 *
 *	table named COMMENTS contains these entities in BD
 *
 */
public class CommentTO implements Serializable{

	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = 320244195334683049L;
	
	/**
	 * id of the comment
	 */
	private Long commentId;
	/**
	 * content of the comment
	 */
	private String commentText;
	/**
	 * creation date of the comment
	 */
	private Date creationDate;
	/**
	 * news id, to which attached the comment
	 */
	private Long newsId;
	
	public CommentTO() {
	}
	
	public Long getComentId() {
		return commentId;
	}

	public void setComentId(Long comentId) {
		this.commentId = comentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comments [comentId=" + commentId + ", commentText="
				+ commentText + ", creationDate=" + creationDate + ", newsId="
				+ newsId + "]";
	}

}
