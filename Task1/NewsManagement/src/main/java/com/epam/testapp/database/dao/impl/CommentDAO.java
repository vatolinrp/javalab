package com.epam.testapp.database.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.database.dao.ICommentDAO;
import com.epam.testapp.entity.CommentTO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class CommentDAO implements ICommentDAO {
	 // SQL
	private final String SQL_DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT=?,CREATION_DATE=?,NEWS_ID=?"
			+ " WHERE COMMENT_ID=?";
	private final String SQL_FIND_COMMENT = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS"
			+ " WHERE COMMENT_ID=?";
	private final String SQL_GET_ALL_COMMENTS = "SELECT COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID FROM COMMENTS";
	private final String SQL_CREATE_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID)"
			+ " values (COMMENTS_SEQ.nextVal,?,?,?)";
	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(CommentTO element) throws DAOException {
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_CREATE_COMMENT,
					new String[] { "COMMENT_ID" });
			statement.setString(1, element.getCommentText());
			statement.setTimestamp(2, new Timestamp(element.getCreationDate()
					.getTime()));
			statement.setLong(3, element.getNewsId());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException("sql error in create method, while creating a comment!");
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in create method, while creating a comment!",e);
		} finally {
			close(statement, resultSet, con);
		}

	}

	@Override
	public CommentTO getById(Long commentId) throws DAOException {
		Connection con = null;
		CommentTO comment = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_FIND_COMMENT);
			statement.setLong(1, commentId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				comment = buildCommentTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return comment;
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in delete method!", e);
		} finally {
			close(statement, con);
		}

	}

	@Override
	public List<CommentTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<CommentTO> comments = new LinkedList<CommentTO>();
		CommentTO comment = null;
		try {
			con = myDataSource.getConnection();
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_COMMENTS);
			while (resultSet.next()) {
				comment = buildCommentTO(resultSet);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getList method!", e);
		} finally {
			close(statement, resultSet, con);
		}
		return comments;
	}
	public static CommentTO buildCommentTO(ResultSet resultSet)
			throws SQLException {
		CommentTO comment = new CommentTO();
		comment.setComentId(resultSet.getLong("COMMENT_ID"));
		comment.setCommentText(resultSet.getString("COMMENT_TEXT"));
		Date date = new Date(resultSet.getTimestamp("CREATION_DATE").getTime());
		comment.setCreationDate(date);
		comment.setNewsId(resultSet.getLong("NEWS_ID"));
		return comment;
	}

	@Override
	public void update(CommentTO element) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = myDataSource.getConnection();
			statement = con.prepareStatement(SQL_UPDATE_COMMENT);
			statement.setString(1, element.getCommentText());
			Timestamp timeStp = new Timestamp(element.getCreationDate().getTime());
			statement.setTimestamp(2,timeStp );
			statement.setLong(3, element.getNewsId());
			statement.setLong(4, element.getComentId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			close(statement, con);
		}
	}

	@Override
	public DataSource getDataSource() {
		return myDataSource;
	}

	/**
	 * Closes statement, resultSet and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param resultSet
	 *            - used resultSet
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, ResultSet resultSet, Connection con)
			throws DAOException {
		try {
			if (resultSet != null)
				resultSet.close();
			close(statement, con);
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}

	/**
	 * Closes statement and connection
	 * 
	 * @param statement
	 *            - used statement
	 * @param con
	 *            - used connection
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	private void close(Statement statement, Connection con) throws DAOException {
		try {
			if (statement != null)
				statement.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			throw new DAOException("sql error in close method!", e);
		}
	}
}
