--------------------------------------------------------
--  File created - Friday-February-27-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence AUTHORS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."AUTHORS_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 6 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COMMENTS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."COMMENTS_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NA_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."NA_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NEWS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."NEWS_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 10 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."NT_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 6 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAGS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERNAME"."TAGS_SEQ"  MINVALUE 4 MAXVALUE 1000 INCREMENT BY 1 START WITH 13 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE "USERNAME"."AUTHOR" 
   (	"AUTHOR_ID" NUMBER, 
	"NAME" VARCHAR2(30 BYTE)
   );
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "USERNAME"."COMMENTS" 
   (	"COMMENT_ID" NUMBER, 
	"COMMENT_TEXT" VARCHAR2(100 BYTE), 
	"CREATION_DATE" TIMESTAMP (6), 
	"NEWS_ID" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "USERNAME"."NEWS" 
   (	"NEWS_ID" NUMBER, 
	"SHORT_TEXT" VARCHAR2(100 BYTE), 
	"FULL_TEXT" VARCHAR2(2000 BYTE), 
	"TITLE" VARCHAR2(30 BYTE), 
	"CREATION_DATE" TIMESTAMP (6), 
	"MODIFICATION_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE "USERNAME"."NEWS_AUTHOR" 
   (	"NEWS_AUTHOR_ID" NUMBER, 
	"NEWS_ID" NUMBER, 
	"AUTHOR_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE "USERNAME"."NEWS_TAG" 
   (	"NEWS_TAG_ID" NUMBER, 
	"NEWS_ID" NUMBER, 
	"TAG_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "USERNAME"."TAG" 
   (	"TAG_ID" NUMBER, 
	"TAG_NAME" VARCHAR2(30 BYTE)
   ) ;

--------------------------------------------------------
--  DDL for Index NEWS_AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."NEWS_AUTHOR_PK" ON "USERNAME"."NEWS_AUTHOR" ("NEWS_AUTHOR_ID");
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."AUTHOR_PK" ON "USERNAME"."AUTHOR" ("AUTHOR_ID");
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."COMMENTS_PK" ON "USERNAME"."COMMENTS" ("COMMENT_ID");
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."TAG_PK" ON "USERNAME"."TAG" ("TAG_ID");
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."NEWS_PK" ON "USERNAME"."NEWS" ("NEWS_ID");
--------------------------------------------------------
--  DDL for Index NEWS_TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERNAME"."NEWS_TAG_PK" ON "USERNAME"."NEWS_TAG" ("NEWS_TAG_ID");
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "USERNAME"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID");
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("SHORT_TEXT" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("FULL_TEXT" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("TITLE" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "USERNAME"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_PK" PRIMARY KEY ("NEWS_AUTHOR_ID");
 
  ALTER TABLE "USERNAME"."NEWS_AUTHOR" MODIFY ("NEWS_AUTHOR_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS_AUTHOR" MODIFY ("NEWS_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS_AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "USERNAME"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_PK" PRIMARY KEY ("NEWS_TAG_ID");
 
  ALTER TABLE "USERNAME"."NEWS_TAG" MODIFY ("NEWS_TAG_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS_TAG" MODIFY ("NEWS_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."NEWS_TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "USERNAME"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID");
 
  ALTER TABLE "USERNAME"."COMMENTS" MODIFY ("COMMENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."COMMENTS" MODIFY ("COMMENT_TEXT" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."COMMENTS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."COMMENTS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "USERNAME"."TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."TAG" MODIFY ("TAG_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."TAG" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID");
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

  ALTER TABLE "USERNAME"."AUTHOR" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID");
 
  ALTER TABLE "USERNAME"."AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERNAME"."AUTHOR" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "USERNAME"."COMMENTS" ADD CONSTRAINT "COMMENTS_FK" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "USERNAME"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "USERNAME"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "USERNAME"."NEWS" ("NEWS_ID") ENABLE;
 
  ALTER TABLE "USERNAME"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK2" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES "USERNAME"."AUTHOR" ("AUTHOR_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "USERNAME"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK" FOREIGN KEY ("TAG_ID")
	  REFERENCES "USERNAME"."TAG" ("TAG_ID") ENABLE;
 
  ALTER TABLE "USERNAME"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK2" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "USERNAME"."NEWS" ("NEWS_ID") ENABLE;
