package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;

public class CommentDAOH implements ICommentDAO{
	private SessionFactory sessionFactory;
	 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	@Override
	public Long create(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Long id = (Long)session.save(element);
		tx.commit();
		return id;
	}

	@Override
	public void update(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(element);
		tx.commit();
		
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		for(Long authorId:elements){
			AuthorTO author = (AuthorTO) session.get(AuthorTO.class, authorId);
			session.delete(author);
		}
		tx.commit();
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<CommentTO> getList() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<CommentTO> list = session.createQuery("SELECT commentId, commentText, creationDate, newsId FROM CommentTO").list();
        session.close();
        return list;
	}

	@Override
	public CommentTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		CommentTO comment = (CommentTO) session.get(CommentTO.class, id);
		tx.commit();
		return comment;
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteByNewsId(List<Long> newsIds) throws DAOException {
		// TODO Auto-generated method stub
		
	}

}
