package com.epam.newsmanagement.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;




import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;
@Repository
public class AuthorDAOH implements IAuthorDAO {
	
	private final String SQL_CREATE_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHOR "
			+ "(NEWS_AUTHOR_ID,NEWS_ID,AUTHOR_ID) "
			+ "VALUES (NA_SEQ.nextVal,:news_id,:author_id)";
	private final String SQL_GET_LIST_NEWS_BY_AUTHOR = "SELECT news.news_Id, news.short_Text, news.full_Text,"
			+ " news.title, news.creation_Date, news.modification_Date "
			+ "FROM NEWS INNER JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE author_Id = :author_id";
	
	private SessionFactory sessionFactory;
	 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	@Override
	public Long create(AuthorTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Long id = (Long)session.save(element);
		tx.commit();
		return id;
	}

	@Override
	public void update(AuthorTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(element);
		tx.commit();
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		for(Long authorId:elements){
			AuthorTO author = (AuthorTO) session.get(AuthorTO.class, authorId);
			session.delete(author);
		}
		tx.commit();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<AuthorTO> getList() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<AuthorTO> list = session.createQuery("SELECT authorId, name FROM AuthorTO where expireDate is null").list();
        session.close();
        return list;
	}

	@Override
	public AuthorTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		AuthorTO author = (AuthorTO) session.get(AuthorTO.class, id);
		tx.commit();
		return author;
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Query query = session.createSQLQuery(SQL_CREATE_NEWS_AUTHORS);
		query.setLong("news_id",newsId);
		query.setLong("author_id",authorId);
		query.executeUpdate();
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Query query = session.createSQLQuery(SQL_GET_LIST_NEWS_BY_AUTHOR);
		query.setLong("author_id",authorId);
		List<NewsTO> list = query.list();
        session.close();
        return list;
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuthorTO> getAuthorsByNewsIdList(List<Long> newsIds)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setExpired(Long authorId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		AuthorTO existingAuthor = (AuthorTO) session.get(AuthorTO.class, authorId);
		existingAuthor.setExpireDate(new Date());
		session.update(existingAuthor);
		tx.commit();
	}
}
