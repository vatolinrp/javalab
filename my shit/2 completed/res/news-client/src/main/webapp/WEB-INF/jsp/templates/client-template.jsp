<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title><spring:message code="page.news" /></title>
<link href="<c:url value="/resources/css/styles.css" />"	rel="stylesheet">
<link href="<c:url value="/resources/css/queryLoader.css" />"	rel="stylesheet">
<link href="<c:url value="/resources/css/common-styles.css" />"	rel="stylesheet">




<link href="<c:url value="/resources/css/filter.css" />"	rel="stylesheet">
<link href="<c:url value="/resources/css/footer-tile.css" />"	rel="stylesheet">
<link href="<c:url value="/resources/css/header-tile.css" />"	rel="stylesheet">
<link href="<c:url value="/resources/css/pagination.css" />"	rel="stylesheet">


<script src="<c:url value="/resources/js/checkbox.js" />"></script>
<script src="<c:url value="/resources/js/jquery.js" />"></script>
<script src="<c:url value="/resources/js/paginate.js" />"></script>
<script src="<c:url value="/resources/js/custom.js" />"></script>
<script src="<c:url value="/resources/js/validation.js" />"></script>
<script src="<c:url value="/resources/js/queryLoader.js" />"></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js'></script>
</head>
<body>
	<div id="wrapper">
		<tiles:insertAttribute name="header" />
		<div id="admin-content">
			<tiles:insertAttribute name="body" />
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>