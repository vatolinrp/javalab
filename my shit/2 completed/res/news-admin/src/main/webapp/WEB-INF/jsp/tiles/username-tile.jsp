<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="user">
	<form method="post" action="/news-admin/j_spring_security_logout">
	<div class="btn-logout">
		<input class="btn-logout"  type="submit" value="<spring:message code="logout" />">
	</div>
	<div class="user-name">
		<spring:message code="greeting" />, ${user.firstName} ${user.lastName}!
	</div>
	</form>
</div>