<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="menu-wrapper">
	<div id="menu-list">
		<ul>
			<li>
				<spring:url value="/news-list/1" var="newslistUrl" htmlEscape="true" /> 
				<a href="${newslistUrl}"><spring:message code="news.list" /></a>
			</li>
			<li>
				<spring:url value="/news-add-edit" var="newsaddeditUrl" htmlEscape="true" />
				<a href="${newsaddeditUrl}"><spring:message code="news.add" /></a>
			</li>
			<li>
				<spring:url value="/authors-add-edit" var="authorsaddeditUrl" htmlEscape="true" />
				<a href="${authorsaddeditUrl}"><spring:message code="news.authors" /></a>
			</li>
			<li>
				<spring:url value="/tags-add-edit" var="tagsaddeditUrl" htmlEscape="true" />
				<a href="${tagsaddeditUrl}"><spring:message code="news.tags" /></a>
			</li>
		</ul>
	</div>
</div>