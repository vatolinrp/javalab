
$(function() {
	$("form label a").on(
			'click',
			function(event) {
				if ($(this).text() == "edit") {
					$(this).parents("label").find('input[type=text]').prop(
							'readonly', false);
					$(this).text("update");
					return false;
				} else if ($(this).text() == "update") {
					$(this).parents("label").find('input[type=text]').prop(
							'readonly', true);
					$(this).text("edit");
					return false;
				}
			});
})
