package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IAuthorService extends ICommonService<AuthorTO> {
	/**
	 * Gets the list of news from NEWS table in DB by our custom author id
	 * 
	 * @param authorId
	 *            - the id of selected author
	 * @return List<NewsTO> - list of news, which belong to author
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;

	/**
	 * Gets author by news id
	 * 
	 * @return AuthorTO - author of the news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException;

	/**
	 * Sets author as expired
	 * 
	 * @param authorId
	 *            the id of author to set as expired
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void setExpired(Long authorId) throws ServiceException;

	/**
	 * Sets author
	 * 
	 * @param newsVO
	 *            - where to set
	 * @return newsVO - result object
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setAuthor(NewsVO newsVO) throws ServiceException;

	/**
	 * Gets a map of authors by news ids
	 * 
	 * @param newsIds
	 *            - selected ids
	 * @return Map<Long, AuthorTO> - wanted map
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws ServiceException;

	/**
	 * Attaches author to a news in NEWS_AUTHORS table
	 * 
	 * @param authorId
	 *            - selected author's ID
	 * @param newsId
	 *            - selected news' ID
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void attachAuthor(Long authorId, Long newsId) throws ServiceException;
}