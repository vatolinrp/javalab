package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;



/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface INewsDAO extends ICommonDAO<NewsTO> {
	/**
	 * Gets list of news by selected ids
	 * 
	 * @param listOfNewsId
	 * @return List<NewsTO> - list of news, which has news with selected ids
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByIds(List<Long> listOfNewsId) throws DAOException;
	
	/**
	 * Gets the number of all news
	 * @return number of all news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Integer getNewsNum() throws DAOException;
	/**
	 * Gets news by page
	 * @param page - the page number
	 * @param perPage - number of news per page
	 * @return List<NewsTO> - list of 
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsTOsByPage(Integer page,Integer perPage) throws DAOException;
	/**
	 * Gets next or previous news
	 * @param newsId - the news id of the former news
	 * @param isNext - true if needed next, false - if previous
	 * @return NewsTO - needed news instance
	 * @throws DAOException if any exceptions occur in DAO
	 */
	NewsTO getNews(Long newsId,boolean isNext) throws DAOException;
	/**
	 * Deletes tags from news_tag table
	 * @param ids - list of tag ids
	 * @throws DAOException if any exceptions occur in DAO
	 */
	void deleteNT(List<Long> ids) throws DAOException;
	/**
	 * Deletes authors from news_author table
	 * @param ids - list of tag ids
	 * @throws DAOException if any exceptions occur in DAO
	 */
	void deleteNA(List<Long> ids) throws DAOException;
	/**
	 * Gets list of filtered news for page 
	 * @param filter - filter param
	 * @param page - the page number
	 * @param perPage - number of news per page
	 * @return List<NewsTO> - list of expected news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page, Integer perPage) throws DAOException;

	/**
	 * Gets one news by filter
	 * @param filter - filter object
	 * @param newsId - the id of the current news
	 * @param isNextNeeded - true if next needed, false - if previous
	 * @return NewsTO - expected news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	NewsTO getOneFilteredNews(FilterVO filter, Long newsId, boolean isNextNeeded) throws DAOException;
	/**
	 * Gets number of filtered news
	 * @param filter - filter param
	 * @return Integer - number of filtered news
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Integer getFilteredNewsNum(FilterVO filter) throws DAOException;
}
