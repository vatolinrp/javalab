package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.ServiceException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ICommentService extends ICommonService<CommentTO> {
	/**
	 * Gets a list of comments by news id
	 * 
	 * @param newsId
	 *            - selected news id
	 * @return List<CommentTO> wanted list
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException;

	/**
	 * Gets a map of comments by news ids
	 * 
	 * @param newsIds
	 *            selected ids
	 * @return Map<Long, ArrayList<CommentTO>> wanted map
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws ServiceException;

	/**
	 * Deletes comments by news id
	 * 
	 * @param ids
	 *            - news ids
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteByNewsId(List<Long> ids) throws ServiceException;
}