package com.epam.newsmanagement.dao;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface IAuthorDAO extends ICommonDAO<AuthorTO> {
	/**
	 * Attaches author to a news in NEWS_AUTHORS table
	 * 
	 * @param authorId
	 *            - selected author's ID
	 * @param newsId
	 *            - selected news' ID
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	void attachAuthor(Long authorId, Long newsId) throws DAOException;

	/**
	 * By author id we get news id in NEWS_AUTHORS and using inner join we get
	 * all news, which have our news id from NEWS_AUTHORS
	 * 
	 * @param authorId
	 *            - selected author's
	 * @return List<News> - list of news, which belongs to author
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException;
	/**
	 * 
	 * @param newsId - selected news' ID
	 * @return AuthorTO - the author of the selected news
	 * @throws DAOException  if any exceptions occur in DAO
	 */
	AuthorTO getAuthorByNewsId(Long newsId) throws DAOException;
	/**
	 * 
	 * @param newsIds - the list of news ids
	 * @return List<AuthorTO> - list of expected authors
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<AuthorTO> getAuthorsByNewsIdList(List<Long> newsIds) throws DAOException;
	
	/**
	 * Gets a map of authors by news ids
	 * @param newsIds - selected ids
	 * @return Map<Long, AuthorTO> - wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds) throws DAOException;
	/**
	 * Sets author as expired
	 * @param authorId - the id of the chosen author
	 * @throws DAOException if any exceptions occur in DAO
	 */
	void setExpired(Long authorId) throws DAOException;
}
