package com.epam.newsmanagement.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ITagDAO extends ICommonDAO<TagTO> {
	/**
	 * Gets list of news by input tag ID
	 * 
	 * @param tagId
	 *            - selected ID
	 * @return List<News> - all news, which have selected tag
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws DAOException;

	/**
	 * Creates a line in DB NEWS_TAGS
	 * 
	 * @param tagIds
	 *            - list of selected tag IDs
	 * @param newsIds
	 *            - selected news ID, for which tag IDs will be attached
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	void attachTags(List<Long> tagIds, Long newsIds) throws DAOException;
	
	/**
	 *  Gets tags by selected news id
	 * @param newsId - selected news id
	 * @return List<TagTO> - tags, which news has
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<TagTO> getTagsByNewsId(Long newsId) throws DAOException;
	/**
	 * Gets a map of tags by news ids
	 * @param newsIds selected ids
	 * @return Map<Long, ArrayList<TagTO>> wanted map
	 * @throws DAOException if any exceptions occur in DAO
	 */
	Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds) throws DAOException;
	/**
	 * Gets tags by ids
	 * @param ids - list of ids of the chosen tags
	 * @return List<TagTO> - the result list of tags
	 * @throws DAOException if any exceptions occur in DAO
	 */
	List<TagTO> getTagsByIds(List<Long> ids) throws DAOException;
	/**
	 * Deletes tags in news_tag database
	 * @param ids - id of the tags
	 * @throws DAOException if any exceptions occur in DAO
	 */
	void deleteNT(List<Long> ids) throws DAOException;

}
