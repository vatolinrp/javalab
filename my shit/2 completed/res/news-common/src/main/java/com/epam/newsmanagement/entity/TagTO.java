package com.epam.newsmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Rostislav_Vatolin
 * 
 *         table named TAG contains these entities in BD
 * 
 */
@Entity
@Table(name="TAG")
public class TagTO implements Serializable {
	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = 1576653101010645788L;

	/**
	 * id of the tag
	 */
	@Id
    @Column(name="TAG_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAGS_SEQ")
    @SequenceGenerator(name="TAGS_SEQ", sequenceName="TAGS_SEQ", allocationSize=1000)
	private Long tagId;
	/**
	 * content of the tag
	 */
	@Column(name="TAG_NAME")
	private String tagName;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public TagTO() {
	}

	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}
}
