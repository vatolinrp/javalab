package com.epam.newsmanagement.service.entity;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;

/**
 * This class is used to display all authors
 * 
 * @author Rostislav_Vatolin
 *
 */
public class EditAuthorsVO {
	//list of displaying authors
	private List<AuthorTO> authorTOList;
	//new author
	private AuthorTO newAuthor;

	public List<AuthorTO> getAuthorTOList() {
		return authorTOList;
	}

	public void setAuthorTOList(List<AuthorTO> authorTOList) {
		this.authorTOList = authorTOList;
	}

	public AuthorTO getNewAuthor() {
		return newAuthor;
	}

	public void setNewAuthor(AuthorTO newAuthor) {
		this.newAuthor = newAuthor;
	}
}
