package com.epam.newsmanagement.dao.util;

/**
 * This class is used to get chosen author and tags for filtering the news
 * 
 * @author Rostislav_Vatolin
 *
 */
public class FilterVO {
	private Long authorId;
	private Long[] tagIds;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long[] getTagIds() {
		return tagIds;
	}

	public void setTagIds(Long[] tagIds) {
		this.tagIds = tagIds;
	}
	public boolean getIsEmpty(){
		if(tagIds==null){
			return true;
		}
		if(tagIds.length==0){
			return true;
		}
		return false;
	}
}
