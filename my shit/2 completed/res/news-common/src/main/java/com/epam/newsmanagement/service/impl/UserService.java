package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.IUserService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class UserService implements IUserService {
	private final Logger logger = Logger.getLogger(UserService.class);
	@Autowired
	private IUserDAO userDAO;

	@Override
	public UserTO getUserByLogin(String login) throws ServiceException {
		try {
			return userDAO.getUserByLogin(login);
		} catch (DAOException e) {
			logger.error("sql error in getUserByLogin method!", e);
			throw new ServiceException("sql error in getUserByLogin method!", e);
		}
	}

	@Override
	public String getGreetingLine(String login) throws ServiceException {
		UserTO user = getUserByLogin(login);
		String userName = "Hello, " + user.getFirstName() + " "
				+ user.getLastName();
		return userName;
	}
}