package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.service.impl.AuthorService;
import com.epam.newsmanagement.service.impl.NewsManagementService;
import com.epam.newsmanagement.service.impl.NewsService;
import com.epam.newsmanagement.service.impl.TagService;

public class NewsManagementServiceTest {
	@InjectMocks
	private NewsManagementService newsManService;
	@Mock
	private AuthorService authorServ;
	@Mock
	private TagService tagServ;
	@Mock
	private NewsService newsServ;
	@Mock
	private IUserService userServ;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void complexCreateNewsTest() throws Exception {
		NewsTO news = new NewsTO();
		news.setTitle("title_4");
		news.setShortText("s_text4");
		news.setFullText("f_test4");
		GregorianCalendar cal = new GregorianCalendar(2015, Calendar.DECEMBER,
				31);
		news.setCreationDate(new Date(cal.getTime().getTime()));
		news.setModificationDate(new Date(cal.getTime().getTime()));
		TagTO tag = new TagTO();
		tag.setTagName("testName4");
		tag.setTagId(1l);
		AuthorTO author = new AuthorTO();
		author.setName("testName4");
		author.setAuthorId(1l);
		List<TagTO> tags = new ArrayList<TagTO>();
		tags.add(tag);
		newsManService.complexCreateNews(news, author, tags);
		Mockito.verify(newsServ).create(news);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1l);
		Long authorId = 1l;
		Mockito.verify(tagServ).attachTags(tagIds, news.getNewsId());
		Mockito.verify(authorServ).attachAuthor(authorId, news.getNewsId());
	}
}