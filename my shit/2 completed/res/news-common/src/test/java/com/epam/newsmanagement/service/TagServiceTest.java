package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.service.impl.TagService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class TagServiceTest {
	@InjectMocks
	private TagService tagService;
	@Mock
	private ITagDAO tagDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getNewsByTagTest() throws Exception {
		tagService.getNewsByTag(1L);
		Mockito.verify(tagDAO).getNewsByTag(1L);
	}

	@Test
	public void attachTagsTest() throws Exception {
		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tagService.attachTags(tags, 1L);
		Mockito.verify(tagDAO).attachTags(tags, 1L);

	}
}
