package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;
//@Repository
public class CommentDAOH implements ICommentDAO{
	private final String SQL_GET_LIST_BY_NEWS_ID = "select comment_id,comment_text,CREATION_DATE,news_id "
			+ "from comments where news_id=:news_id";
	private final String SQL_GET_LIST_BY_NEWS_IDS ="select comment_id,comment_text,CREATION_DATE,news_id "
			+ "from comments where news_id in # order by news_id";
	private final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE NEWS_ID = :news_id";
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public Long create(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Long id = (Long)session.save(element);
			return id;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in create method!",e);
		}
	}

	@Override
	public void update(CommentTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			session.update(element);
		}
		catch(Exception e){
			throw new DAOException("hibernate error in update method!",e);
		}
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			for(Long authorId:elements){
				CommentTO comment = (CommentTO) session.get(CommentTO.class, authorId);
				session.delete(comment);
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in delete method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CommentTO> getList() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(CommentTO.class);
			List<CommentTO> list =  cr.list();
			return list;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getList method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public CommentTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(CommentTO.class);
			cr.add(Restrictions.eq("commentId", id));
			List<CommentTO> list =  cr.list();
			if(list.size()!=0){
				return list.get(0);
			}
			else{
				return null;
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in delete method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery(SQL_GET_LIST_BY_NEWS_ID).addEntity(CommentTO.class);
			query.setLong("news_id",newsId);
			return query.list();
		}
		catch(Exception e){
			throw new DAOException("hibernate error in delete method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Collections.sort(newsIds);
			String sql = SQLBuilder.getSQLList(newsIds, SQL_GET_LIST_BY_NEWS_IDS);
			Map<Long, ArrayList<CommentTO>> resultMap = new HashMap<Long, ArrayList<CommentTO>>();
			List<CommentTO> commentList = session.createSQLQuery(sql).addEntity(CommentTO.class).list();
			for(Long id:newsIds){
				ArrayList<CommentTO> list=new ArrayList<CommentTO>();
				for(int i=0;i<commentList.size();i++){
					if(commentList.get(i).getNewsId().equals(id)){
						list.add(commentList.get(i));
					}
				}
				if(list.size()!=0){
					resultMap.put(id, list);
				}
			}
			return resultMap;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in delete method!",e);
		}
	}

	@Override
	public void deleteByNewsId(List<Long> newsIds) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			for(Long id:newsIds){
				Query query = session.createSQLQuery(SQL_DELETE_BY_NEWS_ID);
				query.setLong("news_id",id);
				query.executeUpdate();
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in delete method!",e);
		}
	}
}
