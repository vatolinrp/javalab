package com.epam.newsmanagement.dao.impl;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;

//@Repository
public class UserDAOH implements IUserDAO{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public UserTO getUserByLogin(String login) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(UserTO.class);
		cr.add(Restrictions.eq("login", login));
		List<UserTO> list =  cr.list();
		if(list.size()!=0){
			return list.get(0);
		}
		else{
			return null;
		}
	}
}
