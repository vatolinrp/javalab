package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Rostislav_Vatolin
 *
 *         table named NEWS contains these entities in BD
 *
 */
@Entity
@Table(name="NEWS")
public class NewsTO implements Serializable {
	/**
	 * generated serial version id
	 */
	private static final long serialVersionUID = -8299786923007171499L;
	/**
	 * id of the news
	 */
	@Id
    @Column(name="NEWS_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEWS_SEQ")
    @SequenceGenerator(name="NEWS_SEQ", sequenceName="NEWS_SEQ", allocationSize=1000)
	private Long newsId;
	/**
	 * short description of the news
	 */
	@Column(name="SHORT_TEXT")
	private String shortText;
	/**
	 * full description of the news
	 */
	@Column(name="FULL_TEXT")
	private String fullText;
	/**
	 * title of the news
	 */
	@Column(name="TITLE")
	private String title;
	/**
	 * Creation date of the news
	 */
	@Column(name="CREATION_DATE")
	private Date creationDate;
	/**
	 * Modification date of the news
	 */
	@Column(name="MODIFICATION_DATE")
	private Date modificationDate;

	public NewsTO() {
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTO other = (NewsTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", shortText=" + shortText
				+ ", fullText=" + fullText + ", title=" + title
				+ ", creationDate=" + creationDate + ", modificationDate="
				+ modificationDate + "]";
	}
}
