package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.util.Close;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class CommentDAO implements ICommentDAO {
	// SQL
	private final String SQL_DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT=?,CREATION_DATE=?,NEWS_ID=?"
			+ " WHERE COMMENT_ID=?";
	private final String SQL_FIND_COMMENT = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS"
			+ " WHERE COMMENT_ID=?";
	private final String SQL_GET_ALL_COMMENTS = "SELECT COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID FROM COMMENTS";
	private final String SQL_CREATE_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID)"
			+ " values (COMMENTS_SEQ.nextVal,?,?,?)";
	private final String SQL_GET_LIST_BY_NEWS_ID = "select comment_id,comment_text,CREATION_DATE,news_id "
			+ "from comments where news_id=?";
	private final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(CommentTO author) throws DAOException {
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_CREATE_COMMENT,
					new String[] { "COMMENT_ID" });
			statement.setString(1, author.getCommentText());
			statement.setTimestamp(2, new Timestamp(author.getCreationDate()
					.getTime()));
			statement.setLong(3, author.getNewsId());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating a comment!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating a comment!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}

	}

	@Override
	public CommentTO getById(Long commentId) throws DAOException {
		Connection con = null;
		CommentTO comment = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_FIND_COMMENT);
			statement.setLong(1, commentId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				comment = buildCommentTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return comment;
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in delete method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public List<CommentTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<CommentTO> comments = new LinkedList<CommentTO>();
		CommentTO comment = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_COMMENTS);
			while (resultSet.next()) {
				comment = buildCommentTO(resultSet);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getList method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return comments;
	}

	public static CommentTO buildCommentTO(ResultSet resultSet)
			throws SQLException {
		CommentTO comment = new CommentTO();
		comment.setCommentId(resultSet.getLong("COMMENT_ID"));
		comment.setCommentText(resultSet.getString("COMMENT_TEXT"));
		Date date = new Date(resultSet.getTimestamp("CREATION_DATE").getTime());
		comment.setCreationDate(date);
		comment.setNewsId(resultSet.getLong("NEWS_ID"));
		return comment;
	}

	@Override
	public void update(CommentTO author) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_UPDATE_COMMENT);
			statement.setString(1, author.getCommentText());
			Timestamp timeStp = new Timestamp(author.getCreationDate()
					.getTime());
			statement.setTimestamp(2, timeStp);
			statement.setLong(3, author.getNewsId());
			statement.setLong(4, author.getCommentId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		List<CommentTO> comments = new LinkedList<CommentTO>();
		CommentTO comment = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_LIST_BY_NEWS_ID);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				comment = buildCommentTO(resultSet);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql exception in getCommentsByNewsId method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return comments;
	}

	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		Map<Long, ArrayList<CommentTO>> resultMap = new HashMap<Long, ArrayList<CommentTO>>();
		ArrayList<CommentTO> comments = new ArrayList<CommentTO>();
		CommentTO comment = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_LIST_BY_NEWS_ID);
			for (Long id : newsIds) {
				statement.setLong(1, id);
				resultSet = statement.executeQuery();
				if (resultSet.next()) {
					comment = buildCommentTO(resultSet);
					comments.add(comment);
					while (resultSet.next()) {
						comment = buildCommentTO(resultSet);
						comments.add(comment);
					}
					resultMap.put(id, comments);
					comments = new ArrayList<CommentTO>();
				} else {
					resultMap.put(id, new ArrayList<CommentTO>());
				}
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getMapComments method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return resultMap;
	}

	@Override
	public void deleteByNewsId(List<Long> newsIds) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE_BY_NEWS_ID);
			for (Long id : newsIds) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in deleteByNewsId method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}

	}
}
