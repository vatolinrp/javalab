package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public interface ITagService extends ICommonService<TagTO> {
	/**
	 * Adds tags to chosen news
	 * 
	 * @param tagIds
	 *            - list of tag ids
	 * @param newsId
	 *            - id of the chosen news
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void attachTags(List<Long> tagIds, Long newsId) throws ServiceException;

	/**
	 * Gets the list of news from NEWS table in DB by our custom tag
	 * 
	 * @param tagId
	 *            - the id of the selected tag
	 * @return List<NewsTO> - list of news by our tag
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<NewsTO> getNewsByTag(Long tagId) throws ServiceException;

	/**
	 * Gets a list of tags by news id
	 * 
	 * @param newsId
	 *            selected news id
	 * @return List<TagTO> wanted list
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<TagTO> getTagsByNewsId(Long newsId) throws ServiceException;

	/**
	 * Gets tags by ids
	 * 
	 * @param ids
	 *            - the tag ids
	 * @return List<TagTO> - expected list
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	List<TagTO> getTagsByIds(List<Long> ids) throws ServiceException;

	/**
	 * Sets tags
	 * 
	 * @param newsVO
	 *            - where to set
	 * @return newsVO - result object
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	NewsVO setTags(NewsVO newsVO) throws ServiceException;

	/**
	 * Gets a map of tags by news ids
	 * 
	 * @param newsIds
	 *            news ids
	 * @return Map<Long, ArrayList<TagTO>> wanted map
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws ServiceException;
	/**
	 * Deletes a list of rows from the NEWS_TAG table
	 * 
	 * @param ids
	 *            - selected list of tag ids for delete
	 * @throws ServiceException
	 *             if any exceptions occur in Service layer
	 */
	void deleteNT(List<Long> ids) throws ServiceException;
}