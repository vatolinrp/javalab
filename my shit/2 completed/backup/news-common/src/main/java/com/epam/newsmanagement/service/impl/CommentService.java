package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class CommentService implements ICommentService {
	private final Logger logger = Logger.getLogger(CommentService.class);
	@Autowired
	private ICommentDAO commentDAO;

	@Override
	public Long create(CommentTO comment) throws ServiceException {
		try {
			return commentDAO.create(comment);
		} catch (DAOException e) {
			logger.error("sql error in create method!", e);
			throw new ServiceException("sql error in create method!", e);
		}

	}

	@Override
	public void delete(List<Long> commentIds) throws ServiceException {
		try {
			commentDAO.delete(commentIds);
		} catch (DAOException e) {
			logger.error("sql error in delete method!", e);
			throw new ServiceException("sql error in delete method!", e);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId)
			throws ServiceException {
		try {
			return (commentDAO
					.getCommentsByNewsId(newsId));
		} catch (DAOException e) {
			logger.error("sql error in getCommentsByNewsId method!", e);
			throw new ServiceException(
					"sql error in getCommentsByNewsId method!", e);
		}
	}

	@Override
	public Map<Long, ArrayList<CommentTO>> getMapComments(List<Long> newsIds)
			throws ServiceException {
		try {
			return commentDAO.getMapComments(newsIds);
		} catch (DAOException e) {
			logger.error("sql error in getMapComments method!", e);
			throw new ServiceException("sql error in getMapComments method!", e);
		}
	}

	@Override
	public void deleteByNewsId(List<Long> ids) throws ServiceException {
		try {
			commentDAO.deleteByNewsId(ids);
		} catch (DAOException e) {
			logger.error("sql error in deleteByNewsId method!", e);
			throw new ServiceException("sql error in deleteByNewsId method!", e);
		}

	}

	@Override
	public List<CommentTO> getList() throws ServiceException {
		try {
			return commentDAO.getList();
		} catch (DAOException e) {
			logger.error("sql error in getList method!", e);
			throw new ServiceException("sql error in getList method!", e);
		}
	}

	@Override
	public void update(List<CommentTO> elements) throws ServiceException {
		try {
			for (CommentTO com : elements) {
				commentDAO.update(com);
			}
		} catch (DAOException e) {
			logger.error("sql error in getList method!", e);
			throw new ServiceException("sql error in getList method!", e);
		}

	}

	@Override
	public CommentTO getById(Long element) throws ServiceException {
		try {
			commentDAO.getById(element);
		} catch (DAOException e) {
			logger.error("sql error in getById method!", e);
			throw new ServiceException("sql error in getById method!", e);
		}
		return null;
	}
}