package com.epam.newsmanagement.service.entity;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;

/**
 * This class is used to display all tags
 * 
 * @author Rostislav_Vatolin
 *
 */
public class EditTagsVO {
	//list of displaying tags
	private List<TagTO> tagTOList;
	//new tag
	private TagTO newTag;

	public List<TagTO> getTagTOList() {
		return tagTOList;
	}

	public void setTagTOList(List<TagTO> tagTOList) {
		this.tagTOList = tagTOList;
	}

	public TagTO getNewTag() {
		return newTag;
	}

	public void setNewTag(TagTO newTag) {
		this.newTag = newTag;
	}

}
