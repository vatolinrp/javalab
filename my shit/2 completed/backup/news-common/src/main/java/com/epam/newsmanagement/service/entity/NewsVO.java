package com.epam.newsmanagement.service.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;

/**
 * This class is used to get and display the data
 * 
 * @author Rostislav_Vatolin
 *
 */
public class NewsVO {
	//newsTO object
	private NewsTO newsTO;
	//author of the news
	private AuthorTO authorTO;
	//list of tags for this news
	private List<TagTO> tagTOs;
	//list of comments for this news
	private List<CommentTO> comments;
	//here we get id from the list
	private String author;
	//here we get date from news creation.
	private String date;
	//here we get tag ids from dropdown list
	private String[] tags;

	public NewsTO getNewsTO() {
		return newsTO;
	}

	public void setNewsTO(NewsTO newsTO) {
		this.newsTO = newsTO;
	}

	public AuthorTO getAuthorTO() {
		return authorTO;
	}

	public void setAuthorTO(AuthorTO authorTO) {
		this.authorTO = authorTO;
	}

	public List<TagTO> getTagTOs() {
		return tagTOs;
	}

	public void setTagTOs(List<TagTO> tagTOs) {
		this.tagTOs = tagTOs;
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}

	public String[] getTags() {
		return tags;
	}

	public List<Long> getTagIds() {
		List<Long> ids = new ArrayList<Long>();
		for (String strId : tags) {
			ids.add(Long.valueOf(strId));
		}
		return ids;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public void setTags(List<TagTO> tags) {
		List<String> strTags = new ArrayList<String>();
		for (TagTO tag : tags) {
			strTags.add(String.valueOf(tag.getTagId()));
		}
		this.tags = new String[strTags.size()];
		this.tags = strTags.toArray(this.tags);
	}
	public String getTagsStr() {
		if (tagTOs == null) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		for (TagTO tag : tagTOs) {
			builder.append(tag.getTagName());
			builder.append(", ");
		}
		if (builder.length() > 2)
			builder.setLength(builder.length() - 2);
		return builder.toString();
	}
	public Integer getCommentsNum() {
		if (comments == null)
			return 0;
		return this.comments.size();
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Date getCurrentDate() {
		return new Date();
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
