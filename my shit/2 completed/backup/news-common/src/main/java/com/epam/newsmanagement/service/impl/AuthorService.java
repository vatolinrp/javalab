package com.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;
import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.entity.NewsVO;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Service
public class AuthorService implements IAuthorService {
	private final Logger logger = Logger.getLogger(AuthorService.class);
	@Autowired
	private IAuthorDAO authorDAO;

	@Override
	public Long create(AuthorTO author) throws ServiceException {
		try {
			return authorDAO.create(author);
		} catch (DAOException e) {
			logger.error("sql error in create method!", e);
			throw new ServiceException("sql error in create method!", e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			return (authorDAO
					.getNewsListByAuthor(authorId));
		} catch (DAOException e) {
			logger.error("sql error in getNewsByAuthor method!", e);
			throw new ServiceException("sql error in getNewsByAuthor method!",
					e);
		}
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		try {
			return (authorDAO
					.getAuthorByNewsId(newsId));
		} catch (DAOException e) {
			logger.error("sql error in getAuthorByNewsId method!", e);
			throw new ServiceException(
					"sql error in getAuthorByNewsId method!", e);
		}

	}

	@Override
	public List<AuthorTO> getList() throws ServiceException {
		try {
			return (authorDAO.getList());
		} catch (DAOException e) {
			logger.error("sql error in getList method!", e);
			throw new ServiceException("sql error in getList method!", e);
		}
	}

	@Override
	public void update(List<AuthorTO> authors) throws ServiceException {
		try {
			for (AuthorTO author : authors) {
				authorDAO.update(author);
			}

		} catch (DAOException e) {
			logger.error("sql error in updateAuthors method!", e);
			throw new ServiceException("sql error in updateAuthors method!", e);
		}
	}

	@Override
	public void delete(List<Long> ids) throws ServiceException {
		try {
			authorDAO.delete(ids);
		} catch (DAOException e) {
			logger.error("sql error in delete method!", e);
			throw new ServiceException("sql error in delete method!", e);
		}
	}

	@Override
	public void setExpired(Long authorId) throws ServiceException {
		try {
			authorDAO.setExpired(authorId);
		} catch (DAOException e) {
			logger.error("sql error in setExpired method!", e);
			throw new ServiceException("sql error in setExpired method!", e);
		}
	}

	@Override
	public NewsVO setAuthor(NewsVO newsVO) throws ServiceException {
		newsVO.setAuthorTO(getById(Long.valueOf(newsVO.getAuthor())));
		return newsVO;
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws ServiceException {
		try {
			return authorDAO.getMapAuthors(newsIds);
		} catch (DAOException e) {
			logger.error("sql error in getMapAuthors method!", e);
			throw new ServiceException("sql error in getMapAuthors method!", e);
		}
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId)
			throws ServiceException {
		try {
			authorDAO.attachAuthor(authorId, newsId);
		} catch (DAOException e) {
			logger.error("sql error in getMapAuthors method!", e);
			throw new ServiceException("sql error in getMapAuthors method!", e);
		}

	}

	@Override
	public AuthorTO getById(Long element) throws ServiceException {
		try {
			return (authorDAO.getById(element));
		} catch (DAOException e) {
			logger.error("sql error in getById method!", e);
			throw new ServiceException("sql error in getById method!", e);
		}
	}

}