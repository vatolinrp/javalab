package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.exceptions.DAOException;



/**
 * 
 * @author Rostislav_Vatolin
 *
 * @param <E>
 *            the entity
 */
public interface ICommonDAO<E> {
	/**
	 * Creates an element in DB and then returns its ID
	 * 
	 * @param element
	 *            - entity instance to create
	 * 
	 * @return Long - generated ID
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	Long create(E element) throws DAOException;

	/**
	 * Updates the content of an element in DB
	 * 
	 * @param element
	 *            - entity instance to update
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	void update(E element) throws DAOException;

	/**
	 * Deletes all elements in DB, which list has
	 * 
	 * @param elements
	 *            - list of IDs for delete
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */

	void delete(List<Long> elements) throws DAOException;

	/**
	 * Gets all elements from DB and puts them in a list
	 * 
	 * @return List<E> - list of all elements from DataBase
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	List<E> getList() throws DAOException;

	/**
	 * Gets E by its ID
	 * 
	 * @param id
	 *            - selected ID
	 * @return E - entity instance which has selected ID
	 * @throws DAOException
	 *             if any exceptions occur in DAO
	 */
	E getById(Long id) throws DAOException;

}
