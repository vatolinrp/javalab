package com.epam.newsmanagement.exceptions;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
public class DAOException extends Exception {

	private static final long serialVersionUID = 2355238789488026594L;

	public DAOException() {
		super();
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}
}
