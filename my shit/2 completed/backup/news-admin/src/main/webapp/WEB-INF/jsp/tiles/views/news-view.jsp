<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div id="news-view-wrapper">
				<div class="news-with-comment">
					<div id="news-view">
						<div id="news-view-header">
							<div class="news-view-title">
								<c:out value="${newsTO.title}" />
							</div>
							<div class="news-view-author">
								(
								<spring:message code="by" />
								<c:out value="${authorTO.name}" />
								)
							</div>
							<div class="news-view-date">
								<div class="date">
									<fmt:message key="date.format" var="format" />
									<fmt:formatDate value="${newsTO.modificationDate}"
										pattern="${format}" />
								</div>
							</div>
						</div>
						<div class="news-view-text">
							<pre><c:out value="${newsTO.shortText}" /></pre>
						</div>
						<div class="news-view-text">
							<pre><c:out value="${newsTO.fullText}" /></pre>
						</div>
					</div>
					<div class="comments-element">
						<form:form method="post" action="/news-admin/comment-add"
							commandName="commentTO"
							onsubmit="return validateCommentForm('${pageContext.response.locale.language}')">
							<c:if test="${not empty commentTOList}">
								<c:forEach var="listValue" items="${commentTOList}"
									varStatus="status">
									<div class="comment-component">
										<div class="comment-date">
											<div class="date">
												<fmt:formatDate value="${listValue.creationDate}"
													pattern="${format}" />
											</div>

											<div class="delete-comment">
												<spring:url
													value="/delete-comment/${listValue.commentId}/${newsTO.newsId}"
													var="deleteUrl" htmlEscape="true" />
												<fmt:message key="delete.confirm.comment" var="confirm" />

												<a href="${deleteUrl} " class="delete-link"
													onclick="return confirmComment('${confirm}');"></a>
											</div>
										</div>
										<div class="comment-text">
											<c:out value="${listValue.commentText}" />
										</div>
									</div>
								</c:forEach>
							</c:if>
							<script src="<c:url value="/resources/js/confirm.js" />"></script>
							<div class="new-comment">
								<form:textarea id="new-comment" rows="5" cols="50"
									path="commentText" />
								<div class="error-message" id="comment-error"></div>
								<form:hidden path="newsId" value="${newsTO.newsId}" />
							</div>
							<input type="submit" id="new-comment-btn"
								value="<spring:message code="post.comment" />">

						</form:form>
					</div>
				</div>
				<div id="navigation">
					<div id="previous">
						<spring:url value="/news-previous/${newsTO.newsId}"
							var="previousUrl" htmlEscape="true" />
						<c:if test="${prevExists}">
							<a href="${previousUrl}"><spring:message code="prev" /></a>
						</c:if>
						<c:if test="${not prevExists}">
							<a class="disabled"><spring:message code="prev" /></a>
						</c:if>
					</div>
					<div id="next">
						<spring:url value="/news-next/${newsTO.newsId}" var="nextUrl"
							htmlEscape="true" />
						<c:if test="${nextExists}">
							<a href="${nextUrl}"><spring:message code="next" /></a>
						</c:if>
						<c:if test="${not nextExists}">
							<a class="disabled"><spring:message code="next" /></a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>