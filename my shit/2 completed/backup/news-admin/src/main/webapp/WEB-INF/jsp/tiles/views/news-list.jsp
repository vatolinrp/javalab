<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div id="filter">
				<form:form id="reset" action="/news-admin/news-list/1" method="get">
					<input type="submit" value="<spring:message code="reset" />">
				</form:form>
				<form:form  action="/news-admin/filter/1" commandName="filterVO" method="get">
					<div id="select-option">
						<div id="author-select">
							<form:select path="authorId" >
								<fmt:message key="default.author" var="author" />
								<form:option  value="0" label="${author}" />
								<form:options items="${authors}" itemValue="authorId" itemLabel="name" />
							</form:select>
						</div>
						<div class="multiselect">
							<div class="selectBox"><spring:message code="tag.select" /></div>
							<div id="checkboxes" style="display: none;" >
								<form:checkboxes element="div" items="${tags}" itemValue="tagId" itemLabel="tagName" path="TagIds" />
							</div>
						</div>
						<script src="<c:url value="/resources/js/checkbox.js" />"></script>
						<input type="submit" value="<spring:message code="btn.filter" />">
					</div>
				</form:form>
			</div>
			<script src="<c:url value="/resources/js/confirm.js" />"></script>
			<form:form method="post" action="/news-admin/delete" onsubmit="return confirmation('${pageContext.response.locale.language}');"
				commandName="deleteNewsVO">
				<c:if test="${not empty deleteNewsVO.newsVOList}">
					<div class="list-of-posts">
						<c:forEach var="listValue" items="${deleteNewsVO.newsVOList}"
							varStatus="status">
							<div class="news-component">
								<div class="title-component">
									<div class="title">
										<spring:url value="/news-view/${listValue.newsTO.newsId}" var="viewUrl" htmlEscape="true" />
										<a href="${viewUrl}"><c:out value="${listValue.newsTO.title}"/></a>
									</div>
									<div class="author">
										(
										<spring:message code="by" />
										<c:out value="${listValue.authorTO.name}"/> )
									</div>
									<div class="date-list">
										<fmt:message key="date.format" var="format" />
										<fmt:formatDate value="${listValue.newsTO.modificationDate}"
											pattern="${format}" />
									</div>
								</div>
								<div class="news-content" style="padding-left: 200px; width:300px;">
									<pre><c:out value="${listValue.newsTO.shortText}"/></pre>
								</div>
								<div class="news-footer">
									<div class="checkbox">
										<form:checkbox path="ForDelete"
											value="${listValue.newsTO.newsId}" />
									</div>
									<form:hidden path="newsVOList[${status.index}].newsTO.newsId"
										value="${listValue.newsTO.newsId}" />
									<div class="edit-link">
										<spring:url value="/news-add-edit/${listValue.newsTO.newsId}"
											var="editUrl" htmlEscape="true" />
										<a href="${editUrl}"><spring:message
												code="link.edit" /></a>
									</div>
									<div class="comments">
										<spring:message code="comments" />
										(${listValue.commentsNum})
									</div>
									<div class="tags"><c:out value="${listValue.tagsStr}"></c:out></div>
								</div>
							</div>
						</c:forEach>
					</div>
				</c:if>
				
				<div id="delete-btn">
					<input id="delete"  type="submit"
						value="<spring:message code="btn.delete" />">
				</div>
				<div class="pagination">
					<div class="page-button-wrapper">
						<c:if test="${pages!=1}">
							<c:forEach var="i" begin="1" end="${pages}">
							    	<a class="pagination-link" href="/news-admin/news-list/${i}">
								    	<span class="page-button">
								    	${i}
								    	</span>
							    	</a>
							</c:forEach>
						</c:if>
						<c:if test="${not empty cpages}">
							<c:if test="${cpages!=1}">
								<c:forEach var="i" begin="1" end="${cpages}">
								    	<a class="pagination-link" href="/news-admin/filter/${i}">
									    	<span class="page-button">
									    	${i}
									    	</span>
								    	</a>
								</c:forEach>
							</c:if>
						</c:if>
					</div>
				</div>
			</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>