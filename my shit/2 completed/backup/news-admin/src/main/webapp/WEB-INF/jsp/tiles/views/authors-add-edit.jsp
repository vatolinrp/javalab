<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
				<c:if test="${not empty authorTOList}">
					<div class="list-of-authors">
						<c:forEach var="listValue" items="${authorTOList}" varStatus="status">
							<form:form action="/news-admin/update-author" method="post" commandName="authorTO">
									<div class="author-select">
									
										<div class="author-title"><spring:message code="author.title" /></div>
										
										<div class="author-name">
											<form:input type="text" class="auhtor-update-input" path="name" value="${listValue.name}" readonly="true" />
											<form:hidden path="authorId" value="${listValue.authorId}"/>
										</div>
										<div style="display: block;" class="select-link"><spring:message code="edit" /></div>
										<fmt:message key="delete.confirm.author" var="confirm" />
										<div class="options" style="display: none;">
											<a href="expire-author/${listValue.authorId}" onclick="return confirmComment('${confirm}');"><spring:message code="expire" /></a>
											<input id="save-author" type="submit" value="<spring:message code="update" />">
											<a class="cancel"><spring:message code="cancel" /></a>
										</div>
									</div>
								<script src="<c:url value="/resources/js/edit.js" />"></script>
								<script src="<c:url value="/resources/js/confirm.js" />"></script>
							</form:form>
						</c:forEach>
					</div>
				</c:if>
				<form:form method="post" action="save-author" commandName="newAuthorTO" 
				onsubmit="return validateNewAuthorForm('${pageContext.response.locale.language}')">
					<div class="author-component">
						<div class="author-title"><spring:message code="author.add" /></div>
						<div class="author-name">
							<form:input type="text" class="author-update-input"
								path="name" />
								
						</div>
						<div class="select-link">
							<input id="save-author" type="submit" value="<spring:message code="save" />">
						</div>
					</div>
					<div class="author-component">
						<div class="error-message" id="new-author-error"></div>
					</div>
				</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>