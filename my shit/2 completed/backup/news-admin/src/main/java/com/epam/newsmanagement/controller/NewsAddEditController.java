package com.epam.newsmanagement.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.exceptions.ServiceException;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.entity.NewsVO;
import com.epam.newsmanagement.utils.ControllerException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Controller
@RequestMapping()
public class NewsAddEditController {
	private final Logger logger = Logger.getLogger(NewsAddEditController.class);
	@Autowired
	private INewsManagementService newsManServ;
	/**
	 * Clears input form
	 * @param newsVO - contains all objects needed for the form
	 * @param model which contains needed objects
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-add-edit", method = RequestMethod.GET)
	public String prepareForCreation(Map<String, Object> model) throws ControllerException {
		model.put("newsVO", new NewsVO());
		try {
			model.put("tags",newsManServ.getListOfTags());
			model.put("authors",newsManServ.getListOfAuthors());
			return "news-add-edit";
		} catch (ServiceException e) {
			logger.error("service exception in prepareForCreation method", e);
			throw new ControllerException("service error in prepareForCreation method!", e);
		}
	}
	/**
	 * Displays selected news
	 * @param newsVO - contains all objects needed for the form
	 * @param newsId - the id of news for edit
	 * @param model which contains needed objects
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 * @throws NumberFormatException if any exceptions occur while parsing id
	 */
	@RequestMapping(value = "/news-add-edit/{newsId}", method = RequestMethod.GET)
	public String displayNews(@PathVariable String newsId, Map<String, Object> model)
			throws NumberFormatException, ControllerException {
		try {
			model.put("newsVO", newsManServ.getNewsVO(Long.valueOf(newsId)));
			model.put("tags",newsManServ.getListOfTags());
			model.put("authors",newsManServ.getAuthorsForEdit(Long.valueOf(newsId)));
			model.put("edit", true);
			return "news-add-edit";
		} catch (ServiceException e) {
			logger.error("service exception in prepareForCreation method", e);
			throw new ControllerException("service error in prepareForCreation method!", e);
		}
		
	}
	/**
	 * Creates or edits news
	 * @param newsVO contains all objects needed for the form
	 * @param model map, which contains needed objects
	 * @return String which points to page
	 * @throws ControllerException if any exceptions occur in Controller
	 */
	@RequestMapping(value = "/news-add-edit", method = RequestMethod.POST)
	public String createOrEdit(@ModelAttribute("newsVO") NewsVO newsVO,
			Map<String, Object> model) throws ControllerException {
		Date date=null;
		Long idOfCurrentNews=null;
		try {
			date=newsManServ.getDate(newsVO);
			newsVO=newsManServ.setNewsTO(newsVO,date);
			newsVO=newsManServ.setAuthor(newsVO);
			newsVO=newsManServ.setTags(newsVO);
			if (newsVO.getNewsTO().getNewsId() == null) {
				idOfCurrentNews=newsManServ.complexCreateNews(newsVO.getNewsTO(),
						newsVO.getAuthorTO(), newsVO.getTagTOs());
			} else {
				idOfCurrentNews=newsVO.getNewsTO().getNewsId();
				newsManServ.updateNews(newsVO.getNewsTO(), newsVO.getAuthorTO(),
						newsVO.getTagTOs());
			}
			return "redirect:"+"/news-view/"+idOfCurrentNews;
		} catch (ServiceException e) {
			logger.error("service exception in createOrEdit method", e);
			throw new ControllerException("service error in createOrEdit method!", e);
		} catch (ParseException e) {
			logger.error("ParseException in createOrEdit method", e);
			throw new ControllerException("service error in createOrEdit method!", e);
		}
		
	}
}
