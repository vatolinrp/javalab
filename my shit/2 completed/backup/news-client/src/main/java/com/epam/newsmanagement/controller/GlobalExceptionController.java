package com.epam.newsmanagement.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.utils.ControllerException;

@ControllerAdvice
public class GlobalExceptionController {
	
	@ExceptionHandler(ControllerException.class)
	public ModelAndView handleCustomException(ControllerException ex) {
		ModelAndView model = new ModelAndView("error");
		return model;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {
		ModelAndView model = new ModelAndView("error");
		return model;
	}
}
