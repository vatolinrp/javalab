package com.epam.newsmanagement.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

public class AuthorDAOJPA implements IAuthorDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	@Override
	public Long create(AuthorTO element) throws DAOException {
		entityManager.persist(element);
		return 1l;
	}

	@Override
	public void update(AuthorTO element) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<AuthorTO> getList() throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AuthorTO getById(Long id) throws DAOException {
		AuthorTO author = entityManager.find(AuthorTO.class, id);
		return author;
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuthorTO> getAuthorsByNewsIdList(List<Long> newsIds)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setExpired(Long authorId) throws DAOException {
		// TODO Auto-generated method stub
		
	}

}
