package com.epam.newsmanagement.dao.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;




import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.util.FilterVO;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

//@Repository
public class NewsDAOH implements INewsDAO{
	private final String SQL_GET_NEWS_NUM = "SELECT COUNT(news_id) FROM news";
	private final String SQL_DELETE_NA = "DELETE FROM NEWS_TAG WHERE NEWS_ID = :news_id";
	private final String SQL_DELETE_NT = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = :news_id";
	private final String SQL_GET_NEWS_BY_IDS = "SELECT NEWS_ID,SHORT_TEXT,FULL_TEXT,"
			+ " TITLE,CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID IN #";
	private final String SQL_GET_A_PART_OF_NEWS ="Select news_id, modification_date, SHORT_TEXT, full_text, creation_date, title from (SELECT news.NEWS_ID, news.SHORT_TEXT, news.FULL_TEXT, news.CREATION_DATE, news.MODIFICATION_DATE, news.title, Row_number()"
			+" over (ORDER BY NVL(com.comment_count,0) DESC,news.modification_date DESC) R FROM news "
			+"LEFT JOIN (SELECT comments.news_id, COUNT(comments.news_id) as comment_count "
			+"FROM COMMENTS GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where R between :from_row and :to_row";
	private final String SQL_GET_NEWS = "Select news_id, modification_date, SHORT_TEXT, full_text, creation_date, title from (SELECT news.NEWS_ID, news.modification_date, news.SHORT_TEXT, news.full_text, news.creation_date, news.title, Row_number() "
			+"over(ORDER BY NVL(com.comment_count,0) DESC,news.modification_date DESC) R FROM news "
			+"LEFT JOIN(SELECT comments.news_id, COUNT(comments.news_id) as comment_count FROM COMMENTS "
			+"GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where"
			+" R=((Select R from (SELECT news.NEWS_ID, Row_number() over (ORDER BY NVL(com.comment_count,0) DESC,"
			+"news.modification_date DESC) R FROM news"
            +" LEFT JOIN (SELECT comments.news_id, COUNT(comments.news_id) as comment_count"
			+" FROM COMMENTS GROUP BY comments.news_id) com ON  news.NEWS_ID=com.news_id) where news_id=:news_id)#)";
	
	@Autowired 
	private SessionFactory sessionFactory;
	 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	@Override
	public Long create(NewsTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Long id = (Long)session.save(element);
			return id;
		}
		catch(Exception e){
			throw new DAOException("sql error in create method!",e);
		}
	}

	@Override
	public void update(NewsTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			session.update(element);
		}
		catch(Exception e){
			throw new DAOException("sql error in update method!",e);
		}
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			for(Long id:elements){
				NewsTO news = (NewsTO) session.get(NewsTO.class, id);
				session.delete(news);
			}
		}
		catch(Exception e){
			throw new DAOException("sql error in delete method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getList() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(NewsTO.class);
			List<NewsTO> list =  cr.list();
			return list;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getList method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public NewsTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Criteria cr = session.createCriteria(NewsTO.class);
			cr.add(Restrictions.eq("newsId", id));
			List<NewsTO> list =  cr.list();
			if(list.size()!=0){
				return list.get(0);
			}
			else{
				return null;
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getById method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getNewsByIds(List<Long> listOfNewsId)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			String sql = SQLBuilder.getSQLList(listOfNewsId, SQL_GET_NEWS_BY_IDS);
			List<NewsTO> newsList = session.createSQLQuery(sql).addEntity(NewsTO.class).list();
			return newsList;
		}
		catch(Exception e){
			throw new DAOException("sql error in getNewsByIds method!",e);
		}
	}
	@Override
	@Transactional
	public Integer getNewsNum() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery(SQL_GET_NEWS_NUM);
			Integer newsNum = ((BigDecimal)query.uniqueResult()).intValue();
			return newsNum;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getNewsNum method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getNewsTOsByPage(Integer page, Integer perPage)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery(SQL_GET_A_PART_OF_NEWS).addEntity(NewsTO.class);
			query.setInteger("from_row", page * perPage - (perPage - 1));
			query.setInteger("to_row", page * perPage);
			List<NewsTO> list = query.list();
			return list;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getNewsTOsByPage method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public NewsTO getNews(Long newsId, boolean isNext) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		String sql = SQL_GET_NEWS;
		if (isNext) {
			sql = sql.replaceFirst("#", "+1");
		} else {
			sql = sql.replaceFirst("#", "-1");
		}
		try{
			Query query = session.createSQLQuery(sql.toString()).addEntity(NewsTO.class);
			query.setLong("news_id", newsId);
			List<NewsTO> list = query.list();
			if((list.size()==0)){
				return getById(newsId);
			}
			return list.get(0);
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getNews method!",e);
		}
	}

	@Override
	public void deleteNT(List<Long> ids) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery(SQL_DELETE_NT);
			for(Long id:ids){
				query.setLong("news_id", id);
				query.executeUpdate();
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in deleteNT method!",e);
		}
	}

	@Override
	public void deleteNA(List<Long> ids) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createSQLQuery(SQL_DELETE_NA);
			for(Long id:ids){
				query.setLong("news_id", id);
				query.executeUpdate();
			}
		}
		catch(Exception e){
			throw new DAOException("hibernate error in create method!",e);
		}
		
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getPageOfFilteredNews(FilterVO filter, Integer page,
			Integer perPage) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		String sql = SQLBuilder.getSQLForPageOfFilteredNews(filter, page, perPage);
		List<NewsTO> listOfNews = new LinkedList<NewsTO>();
		try{
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString()).addEntity(NewsTO.class);
				listOfNews = query.list();
			}
			else{
				return listOfNews;
			}
			return listOfNews;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in getPageOfFilteredNews method!",e);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public NewsTO getOneFilteredNews(FilterVO filter, Long newsId,
			boolean isNextNeeded) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = SQLBuilder.getSQLForOneFilteredNews(filter, newsId, isNextNeeded);
		List<NewsTO> listOfNews = new LinkedList<NewsTO>();
		try{
			
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString()).addEntity(NewsTO.class);
				listOfNews = query.list();
			}
			else{
				return getById(newsId);
			}
			return listOfNews.get(0);
		}
		catch(Exception e){
			
			throw new DAOException("hibernate error in create method!",e);
		}
	}
	
	@Override
	@Transactional
	public Integer getFilteredNewsNum(FilterVO filter) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Integer newsNum=null;
		String sql = SQLBuilder.getSQLForNumOfFilteredNews(filter);
		try{
			if(sql!=null){
				Query query = session.createSQLQuery(sql.toString());
				newsNum = ((BigDecimal)query.uniqueResult()).intValue();
			}
			else{
				return 0;
			}
			return newsNum;
		}
		catch(Exception e){
			throw new DAOException("hibernate error in create method!",e);
		}
	}

}
