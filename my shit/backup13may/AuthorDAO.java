package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.util.Close;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */

@Repository
public class AuthorDAO implements IAuthorDAO {
	// SQL
	private final String SQL_DELETE_AUTHORS = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private final String SQL_FIND_AUTHOR = "SELECT AUTHOR_ID, NAME, EXPIRED "
			+ "FROM AUTHOR where AUTHOR_ID=?";
	private final String SQL_GET_AUTHORS_BY_NEWS_IDS = "select author.author_id, author.name, author.EXPIRED from author"
			+ " inner join news_author on"
			+ " (news_author.author_id=author.author_id)"
			+ " where news_author.news_id in #";
	private final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID,NAME,EXPIRED) "
			+ "values (AUTHORS_SEQ.nextVal,?,NULL) ";
	private final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, NAME,EXPIRED FROM AUTHOR where expired is null";
	private final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET NAME=? WHERE AUTHOR_ID=?";
	private final String SQL_EXPIRE_AUTHOR = "UPDATE AUTHOR SET EXPIRED=? WHERE AUTHOR_ID=?";
	private final String SQL_GET_LIST_NEWS_BY_AUTHOR = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
			+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
			+ "FROM NEWS INNER JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID " + "WHERE AUTHOR_ID=?";
	private final String SQL_CREATE_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHOR "
			+ "(NEWS_AUTHOR_ID,NEWS_ID,AUTHOR_ID) "
			+ "VALUES (NA_SEQ.nextVal,?,?)";
	private final String SQL_FIND_AUTHOR_BY_NEWS_ID = "select author.author_id, author.name, author.EXPIRED from author"
			+ " inner join news_author on"
			+ " (news_author.author_id=author.author_id)"
			+ " where news_author.news_id = ?";
	private final String SQL_FIND_AUTHOR_BY_NEWS_IDS="select author.author_id, author.name, author.EXPIRED from author"
			+" inner join news_author on"
			+" (news_author.author_id=author.author_id)"
			+" where news_author.news_id in # order by news_author.NEWS_ID";

	@Autowired
	private DataSource dataSource;

	@Override
	public void update(AuthorTO author) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_UPDATE_AUTHOR);
			statement.setString(1, author.getName());
			statement.setLong(2, author.getAuthorId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_CREATE_NEWS_AUTHORS);
			statement.setLong(1, newsId);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in attachAuthor method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public Long create(AuthorTO author) throws DAOException {
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_CREATE_AUTHOR,
					new String[] { "AUTHOR_ID" });
			statement.setString(1, author.getName());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating an author!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating an author!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<AuthorTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<AuthorTO> authors = new LinkedList<AuthorTO>();
		AuthorTO news = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_AUTHORS);
			while (resultSet.next()) {
				news = buildAuthorTO(resultSet);
				authors.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getList method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authors;
	}

	@Override
	public AuthorTO getById(Long authorId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		AuthorTO author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_FIND_AUTHOR);
			statement.setLong(1, authorId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				author = buildAuthorTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	public static AuthorTO buildAuthorTO(ResultSet resultSet)
			throws SQLException {
		AuthorTO author = new AuthorTO();
		author.setAuthorId(resultSet.getLong("AUTHOR_ID"));
		author.setName(resultSet.getString("NAME"));
		Timestamp time = resultSet.getTimestamp("EXPIRED");
		if(time!=null){
			author.setExpireDate(new Date(time.getTime()));
		}
		return author;
	}

	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws DAOException {
		Map<Long, AuthorTO> resultMap = new HashMap<Long, AuthorTO>();
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<AuthorTO> authorList=new ArrayList<AuthorTO>();
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.createStatement();
			Collections.sort(newsIds);
			String sql = SQLBuilder.getSQLList(newsIds, SQL_FIND_AUTHOR_BY_NEWS_IDS);
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				authorList.add(buildAuthorTO(resultSet));
			}
			int i=0;
			for(Long id:newsIds){
				resultMap.put(id, authorList.get(i));
				i++;
			}
			return resultMap;
		} catch (SQLException e) {
			throw new DAOException("sql exception in getMapAuthors method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

	@Override
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		List<NewsTO> listNews = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_GET_LIST_NEWS_BY_AUTHOR);
			statement.setLong(1, authorId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = NewsDAO.buildNewsTO(resultSet);
				listNews.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsListByAuthor method!",
					e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return listNews;
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_DELETE_AUTHORS);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in delete method!", e);

		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		AuthorTO author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_FIND_AUTHOR_BY_NEWS_ID);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				author = buildAuthorTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getAuthorByNewsId method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	@Override
	public List<AuthorTO> getAuthorsByNewsIdList(List<Long> newsIds)
			throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		List<AuthorTO> authorTO = new ArrayList<AuthorTO>();
		AuthorTO author = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			String sql = SQLBuilder.getSQLList(newsIds,SQL_GET_AUTHORS_BY_NEWS_IDS);
			statement = con.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				author = buildAuthorTO(resultSet);
				authorTO.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql exception in getAuthorsByNewsIdList method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorTO;
	}

	@Override
	public void setExpired(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			statement = con.prepareStatement(SQL_EXPIRE_AUTHOR);
			statement.setTimestamp(1, new Timestamp((new Date()).getTime()));
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

}
