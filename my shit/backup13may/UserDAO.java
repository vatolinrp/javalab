package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.util.Close;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class UserDAO implements IUserDAO {
	// SQL
	private final String SQL_FIND_USER = "SELECT USER_ID, FIRSTNAME, LASTNAME, LOGIN, PASS FROM USERS WHERE LOGIN=?";
	@Autowired
	private DataSource myDataSource;

	@Override
	public UserTO getUserByLogin(String login) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_FIND_USER);
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				return buildUserTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getUserByLogin method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return null;
	}

	private UserTO buildUserTO(ResultSet resultSet) throws SQLException {
		UserTO user = new UserTO();
		user.setUserId(resultSet.getLong("USER_ID"));
		user.setFirstName(resultSet.getString("FIRSTNAME"));
		user.setLastName(resultSet.getString("LASTNAME"));
		user.setLogin(resultSet.getString("LOGIN"));
		user.setPassHash(resultSet.getString("PASS"));
		return user;
	}
}
