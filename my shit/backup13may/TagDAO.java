package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.util.Close;
import com.epam.newsmanagement.dao.util.SQLBuilder;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;

/**
 * 
 * @author Rostislav_Vatolin
 *
 */
@Repository
public class TagDAO implements ITagDAO {
	// SQL
	private final String SQL_DELETE = "DELETE FROM TAG WHERE TAG_ID = ?";
	private final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME=? WHERE TAG_ID=?";
	private final String SQL_CREATE = "INSERT INTO TAG (TAG_ID,TAG_NAME) "
			+ "VALUES (TAGS_SEQ.nextVal,?) ";
	private final String SQL_GET_TAGS_BY_IDS = "SELECT TAG_ID,TAG_NAME"
			+ " FROM TAG WHERE TAG_ID IN #";
	private final String SQL_CREATE_NEWS_TAGS = "INSERT INTO NEWS_TAG (NEWS_TAG_ID, TAG_ID, NEWS_ID) "
			+ "VALUES (NT_SEQ.nextVal,?,?)";
	private final String SQL_GET_ALL_TAGS = "SELECT TAG_NAME,TAG_ID FROM TAG";
	private final String SQL_FIND_TAG = "SELECT TAG_ID, TAG_NAME FROM TAG "
			+ "WHERE TAG_ID=?";
	private final String SQL_GET_LIST_NEWS_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
			+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE"
			+ " FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
			+ "WHERE TAG_ID=?";
	private final String SQL_GET_LIST_BY_NEWS_ID = "select tag.tag_id, tag.tag_name from tag inner join news_tag"
			+ " on (news_tag.tag_id=tag.tag_id) where news_tag.news_id=?";
	private final String SQL_DELETENT = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";

	@Autowired
	private DataSource myDataSource;

	@Override
	public Long create(TagTO tag) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_CREATE,
					new String[] { "TAG_ID" });
			statement.setString(1, tag.getTagName());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"sql error in create method, while creating a tag!");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"sql error in create method, while creating a tag!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws DAOException {
		List<NewsTO> listNews = new ArrayList<NewsTO>();
		Connection con = null;
		NewsTO news = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_LIST_NEWS_BY_TAG);
			statement.setLong(1, tagId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = NewsDAO.buildNewsTO(resultSet);
				listNews.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getNewsByTag method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return listNews;
	}

	@Override
	public void update(TagTO tag) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_UPDATE_TAG);
			statement.setString(1, tag.getTagName());
			statement.setLong(2, tag.getTagId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("sql error in update method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		Map<Long, ArrayList<TagTO>> resultMap = new HashMap<Long, ArrayList<TagTO>>();
		ArrayList<TagTO> tags = new ArrayList<TagTO>();
		TagTO tag = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_LIST_BY_NEWS_ID);
			for (Long id : newsIds) {
				statement.setLong(1, id);
				resultSet = statement.executeQuery();
				if (resultSet.next()) {
					tag = buildTagTO(resultSet);
					tags.add(tag);
					while (resultSet.next()) {
						tag = buildTagTO(resultSet);
						tags.add(tag);
					}
					resultMap.put(id, tags);
					tags = new ArrayList<TagTO>();
				} else {
					resultMap.put(id, new ArrayList<TagTO>());
				}
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getMapTags method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return resultMap;
	}

	@Override
	public void attachTags(List<Long> tagIds, Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			for (Long tagId : tagIds) {
				statement = con.prepareStatement(SQL_CREATE_NEWS_TAGS);
				statement.setLong(1, tagId);
				statement.setLong(2, newsId);
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in attachTags method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in delete method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}

	@Override
	public TagTO getById(Long tagId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_FIND_TAG);
			statement.setLong(1, tagId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				return buildTagTO(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getById method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return null;
	}

	@Override
	public List<TagTO> getList() throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		Statement statement = null;
		List<TagTO> tags = new ArrayList<TagTO>();
		TagTO tag = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_TAGS);
			while (resultSet.next()) {
				tag = buildTagTO(resultSet);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getList method!", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return tags;
	}

	public static TagTO buildTagTO(ResultSet resultSet) throws SQLException {
		TagTO tag = new TagTO();
		tag.setTagId(resultSet.getLong("TAG_ID"));
		tag.setTagName(resultSet.getString("TAG_NAME"));
		return tag;
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws DAOException {
		ResultSet resultSet = null;
		Connection con = null;
		PreparedStatement statement = null;
		List<TagTO> tags = new ArrayList<TagTO>();
		TagTO tag = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_GET_LIST_BY_NEWS_ID);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tag = buildTagTO(resultSet);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("sql exception in getTagsByNewsId method!",
					e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return tags;
	}

	@Override
	public List<TagTO> getTagsByIds(List<Long> ids) throws DAOException {
		List<TagTO> taglist = new ArrayList<TagTO>();
		Connection con = null;
		TagTO tag = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		String sql = SQLBuilder.getSQLList(ids, SQL_GET_TAGS_BY_IDS);
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tag = buildTagTO(resultSet);
				taglist.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("sql error in getTagsByIds method", e);
		} finally {
			Close.close(statement);
			Close.close(resultSet);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
		return taglist;
	}
	@Override
	public void deleteNT(List<Long> ids) throws DAOException {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DataSourceUtils.getConnection(myDataSource);
			statement = con.prepareStatement(SQL_DELETENT);
			for (Long id : ids) {
				statement.setLong(1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("sql error in deleteNT method!", e);
		} finally {
			Close.close(statement);
			DataSourceUtils.releaseConnection(con, myDataSource);
		}
	}
}
