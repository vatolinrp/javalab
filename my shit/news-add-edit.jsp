<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div id="news-add-edit-wrapper">
				<form:form method="post" action="/news-admin/news-add-edit" commandName="newsVO">
					<div id="editNews">
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.title" />
							</div>
							<div class="content-component">
								<form:input path="newsTO.title" id="title-input" />
							</div>
							<div class="content-component">
								<div class="error-message" >${messages.title}</div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.date" />
							</div>
							<div class="content-component">
								<form:input type="text" path="date" id="date-input" />
							</div>
							<div class="content-component">
								<div class="error-message" >${messages.date}</div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.brief" />
							</div>
							<div class="content-component">
								<form:textarea id="brief-input" rows="5" cols="50" path="newsTO.shortText" />
							</div>
							<div class="content-component">
								<div class="error-message" >${messages.brief}</div>
							</div>
						</div>
						<div class="input-component">
							<div class="component-name">
								<spring:message code="news.content" />
							</div>
							<div class="content-component">
								<form:textarea id="content-input" rows="10" cols="50"
									path="newsTO.fullText" />
							</div>
							<div class="content-component">
								<div class="error-message" >${messages.content}</div>
							</div>
						</div>
					</div>
					<div id="news-add-edit">
						<div id="select-option">
							<div id="author-select">
							
								<form:select path="author" items="${authors}" />
							</div>
							<div class="multiselect">
								<div class="selectBox" onclick="showCheckboxes()">
									<select>
										<option><spring:message code="tag.select" /></option>
									</select>
									<div class="overSelect"></div>
								</div>
								<div id="checkboxes">
									<form:checkboxes element="div" items="${tags}" path="tags" />
								</div>
							</div>
						</div>
						<form:hidden path="newsTO.newsId" value="${newsTO.newsId}"/>
						<input type="submit" id="save-btn" value="<spring:message code="btn.save" />">
					</div>
				</form:form>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>