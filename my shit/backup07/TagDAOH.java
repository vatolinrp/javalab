package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exceptions.DAOException;

public class TagDAOH implements ITagDAO{
	private final String SQL_GET_LIST_NEWS_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
			+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE"
			+ " FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
			+ "WHERE TAG_ID=:tag_id";
	private final String SQL_CREATE_NEWS_TAGS = "INSERT INTO NEWS_TAG (NEWS_TAG_ID, TAG_ID, NEWS_ID) "
			+ "VALUES (NT_SEQ.nextVal,:tag_id,:news_id)";
	private final String SQL_GET_LIST_BY_NEWS_ID = "select tag.tag_id, tag.tag_name from tag inner join news_tag"
			+ " on (news_tag.tag_id=tag.tag_id) where news_tag.news_id= :news_id";
	private final String SQL_GET_TAGS_BY_IDS = "SELECT TAG_ID,TAG_NAME"
			+ " FROM TAG WHERE TAG_ID IN #";
	private final String SQL_DELETENT = "DELETE FROM NEWS_TAG WHERE TAG_ID = :tag_id";
	
	private SessionFactory sessionFactory;
	 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	@Override
	public Long create(TagTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Long id = (Long)session.save(element);
			return id;
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in create method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}

	@Override
	public void update(TagTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			session.update(element);
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in update method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
		
	}

	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			for(Long id:elements){
				TagTO tag = (TagTO) session.get(TagTO.class, id);
				session.delete(tag);
			}
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in delete method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TagTO> getList() throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Criteria cr = session.createCriteria(TagTO.class);
			List<TagTO> list =  cr.list();
			return list;
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getList method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public TagTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Criteria cr = session.createCriteria(TagTO.class);
			cr.add(Restrictions.eq("tagId", id));
			List<TagTO> list =  cr.list();
			if(list.size()!=0){
				return list.get(0);
			}
			else{
				return null;
			}
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in delete method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<NewsTO> getNewsByTag(Long tagId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Query query = session.createSQLQuery(SQL_GET_LIST_NEWS_BY_TAG);
			query.setLong("tag_id",tagId);
			List<NewsTO> list = query.list();
			return list;
		}
        catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getNewsByTag method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}

	@Override
	public void attachTags(List<Long> tagIds, Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Query query = session.createSQLQuery(SQL_CREATE_NEWS_TAGS);
			for(Long tagId:tagIds){
				query.setLong("tag_id",tagId);
				query.setLong("news_id",newsId);
				query.executeUpdate();
			}
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in attachAuthor method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Query query = session.createSQLQuery(SQL_GET_LIST_BY_NEWS_ID);
			query.setLong("news_id",newsId);
			List<TagTO> list = query.list();
			return list;
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in attachAuthor method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, ArrayList<TagTO>> getMapTags(List<Long> newsIds)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Map<Long, ArrayList<TagTO>> resultMap = new HashMap<Long, ArrayList<TagTO>>();
			Query query = session.createSQLQuery(SQL_GET_LIST_BY_NEWS_ID).addEntity(TagTO.class);
			ArrayList<TagTO> tagList=null;
			for(Long id:newsIds){
				query.setLong("news_id", id);
				tagList = new ArrayList<TagTO>(query.list());
				resultMap.put(id,tagList);
			}
			return resultMap;
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getMapTags method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TagTO> getTagsByIds(List<Long> tagIds) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			String ids = tagIds.toString();
			ids = ids.replace("[", "(");
			ids = ids.replace("]", ")");
			String sql = SQL_GET_TAGS_BY_IDS.replace("#", ids);
			List<TagTO> tagList = session.createSQLQuery(sql).addEntity(TagTO.class).list();
			return tagList;
		}
        catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getAuthorsByNewsIdList method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}
	@Override
	public void deleteNT(List<Long> ids) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Query query = session.createSQLQuery(SQL_DELETENT);
			for(Long tagId:ids){
				query.setLong("tag_id", tagId);
				query.executeUpdate();
			}
		}
        catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getAuthorsByNewsIdList method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
		
		
	}

}
