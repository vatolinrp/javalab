package com.epam.newsmanagement.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exceptions.DAOException;

public class UserDAOH implements IUserDAO{
	private SessionFactory sessionFactory;
	 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @SuppressWarnings("unchecked")
	@Override
	public UserTO getUserByLogin(String login) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx=null;
		try{
			tx= session.beginTransaction();
			Criteria cr = session.createCriteria(UserTO.class);
			cr.add(Restrictions.eq("login", login));
			List<UserTO> list =  cr.list();
			if(list.size()!=0){
				return list.get(0);
			}
			else{
				return null;
			}
		}
		catch(Exception e){
			if (tx!=null){
				tx.rollback();
			}
			throw new DAOException("sql error in getUserByLogin method!",e);
		}
		finally {
			if (!tx.wasCommitted())
			    tx.commit();
			if(session.isOpen()){
			   session.close();
			}
		}
	}

}
