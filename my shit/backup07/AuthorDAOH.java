package com.epam.newsmanagement.dao.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exceptions.DAOException;
@Repository
public class AuthorDAOH implements IAuthorDAO {
	
	private final String SQL_CREATE_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHOR "
			+ "(NEWS_AUTHOR_ID,NEWS_ID,AUTHOR_ID) "
			+ "VALUES (NA_SEQ.nextVal,:news_id,:author_id)";
	private final String SQL_GET_LIST_NEWS_BY_AUTHOR = "SELECT news.news_Id, news.short_Text, news.full_Text,"
			+ " news.title, news.creation_Date, news.modification_Date "
			+ "FROM NEWS INNER JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE author_Id = :author_id";
	private final String SQL_FIND_AUTHOR_BY_NEWS_ID = "select author.author_id, author.name, author.EXPIRED from author"
			+ " inner join news_author on"
			+ " (news_author.author_id=author.author_id)"
			+ " where news_author.news_id=:news_id";
	private final String SQL_GET_AUTHORS_BY_NEWS_IDS = "select author.author_id, author.name, author.EXPIRED from author"
			+ " inner join news_author on"
			+ " (news_author.author_id=author.author_id)"
			+ " where news_author.news_id in (";
	private final String SQL_FIND_AUTHOR_BY_NEWS_IDS="select author.author_id, author.name, author.EXPIRED from author"
			+" inner join news_author on"
			+" (news_author.author_id=author.author_id)"
			+" where news_author.news_id in # order by news_author.NEWS_ID";
	@Autowired
	private SessionFactory sessionFactory;
	 
   
	@Override
	public Long create(AuthorTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
			Long id = (Long)session.save(element);
			return id;
	}

	@Override
	public void update(AuthorTO element) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		session.update(element);
	}
	@Override
	public void delete(List<Long> elements) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		for(Long id:elements){
			AuthorTO author = (AuthorTO) session.get(AuthorTO.class, id);
			session.delete(author);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<AuthorTO> getList() throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(AuthorTO.class);
		cr.add(Restrictions.isNull("expireDate"));
		List<AuthorTO> list =  cr.list();
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public AuthorTO getById(Long id) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		
		Criteria cr = session.createCriteria(AuthorTO.class);
		cr.add(Restrictions.eq("authorId", id));
		List<AuthorTO> list =  cr.list();
		if(list.size()!=0){
			
			return list.get(0);
		}
		else{
			
			return null;
		}
		
	}

	@Override
	public void attachAuthor(Long authorId, Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery(SQL_CREATE_NEWS_AUTHORS);
		query.setLong("news_id",newsId);
		query.setLong("author_id",authorId);
		query.executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<NewsTO> getNewsListByAuthor(Long authorId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery(SQL_GET_LIST_NEWS_BY_AUTHOR);
		query.setLong("author_id",authorId);
		List<NewsTO> list = query.list();
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery(SQL_FIND_AUTHOR_BY_NEWS_ID).addEntity(AuthorTO.class);
		query.setLong("news_id",newsId);
		List<AuthorTO> list = query.list();
		return list.get(0);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<AuthorTO> getAuthorsByNewsIdList(List<Long> newsIds)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sb = new StringBuilder();
		sb.append(SQL_GET_AUTHORS_BY_NEWS_IDS);
		for (Long newsId : newsIds) {
			sb.append(newsId).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		List<AuthorTO> list = session.createSQLQuery(sb.toString()).addEntity(AuthorTO.class).list();
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, AuthorTO> getMapAuthors(List<Long> newsIds)
			throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		Collections.sort(newsIds);
		String ids = newsIds.toString();
		ids = ids.replace("[", "(");
		ids = ids.replace("]", ")");
		String sql = SQL_FIND_AUTHOR_BY_NEWS_IDS.replace("#", ids);
		Map<Long, AuthorTO> resultMap = new HashMap<Long, AuthorTO>();
		List<AuthorTO> authorList = session.createSQLQuery(sql).addEntity(AuthorTO.class).list();
		int i=0;
		for(Long id:newsIds){
		resultMap.put(id, authorList.get(i));
			i++;
		}
		return resultMap;
	}

	@Override
	public void setExpired(Long authorId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		try{
			AuthorTO existingAuthor = (AuthorTO) session.get(AuthorTO.class, authorId);
			existingAuthor.setExpireDate(new Date());
			session.update(existingAuthor);
		}
        catch(Exception e){
			throw new DAOException("sql error in setExpired method!",e);
		}
	}
}
